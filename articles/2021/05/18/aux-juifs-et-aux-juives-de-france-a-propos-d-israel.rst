.. index::
   pair: Nouvelle Calédonie; Suite au référendum en Nouvelle Calédonie (2024-01-06)

.. _jjr_2021_05_18:

=========================================================================
2021-05-18 **Aux Juifs et aux Juives de France, à propos d’Israël**
=========================================================================

- https://juivesetjuifsrevolutionnaires.wordpress.com/2021/05/18/aux-juifs-et-aux-juives-de-france-a-propos-disrael/

Chaque regain de violence entre l’État d’Israël et les Palestiniens et
Palestiniennes, une partie de la communauté juive de France prend parti en faveur
du premier.

Cela s’explique notamment pour certains par des liens familiaux et
l’inquiétude pour des proches qui peuvent conduire à une identification avec cet
Etat.

Tous les Juifs et les Juives ne partagent pas cette position : une partie est
attachée à l’État d’Israël mais capable d’un regard critique vis-à-vis
de la droite israélienne, du gouvernement israélien, de la situation coloniale
et de l’histoire de ce pays. D’autres ne se sentent aucunement lié·es à lui.

Bien que Juifs et Juives, **nous, membres de JJR, ne considérons l’État
d’Israël ni comme une terre promise, ni comme un refuge, ni comme un État
pour lequel nous devrions rendre des comptes**.

Juifs et Juives de France, nous nous sentons toutes et tous menacé·es dans les
périodes de libération des actes et de la parole antisémite (en particulier
sur les réseaux sociaux actuellement), notamment quand des antisémites en France
utilisent comme prétexte la situation en Palestine pour vomir une haine des Juifs
et des Juives qui pré-existait largement à l’État d’Israël.

Face à cette situation, de nombreux membres de notre minorité sont inquiets, pensent à partir
ou à dissimuler leur judaïsme. Pour nous, la solution à cet antisémitisme
n’est ni l’assimilationisme ni le sionisme, mais la lutte ici et maintenant,
dans la diaspora.

Pour toutes ces raisons, parce que nous considérons qu’il nous faut prioriser
la lutte contre l’impérialisme de notre propre pays, la France et pour ne
pas répondre à l’injonction antisémite qui nous est faite de sans cesse
nous désolidariser d’Israël comme si les juives et juifs étaient comptables
des agissements du gouvernement israélien (injonction sur laquelle nous avons
déjà écrit), nous communiquons rarement sur la situation là-bas. Le texte que
nous proposons ici fait exception. Notre volonté d’éviter toute focalisation
malsaine sur la situation en Israël / Palestine n’est pas une interdiction de
prendre position.

Quelle que soit notre relation avec ce pays, nous ne pouvons que souhaiter aux
Israéliennes et aux Israéliens la même chose qu’à tous les peuples, c’est
à dire de vivre dans un cadre pacifique, démocratique et social. Cet objectif
est aujourd’hui loin d’être atteint. Nous partageons quelques constats qui
expliquent en partie cet échec :

- La démocratie n’est pas réelle quand des minorités nationales, comme
  les Palestiniennes et Palestiniens ayant la citoyenneté israélienne, ont
  des droits réduits par rapport la population majoritaire.
- La paix n’est pas possible tant que, en marge de la société israélienne,
  des millions de Palestiniennes et Palestiniens constatent que leurs aspirations
  ne sont pas prises en compte.
- Les périodes de calme ne peuvent être que temporaires puisqu’elles sont
  basées sur la répression violente et meurtrière de toute revendication palestinienne.
- L’escalade de la violence favorise la frange la plus réactionnaire des
  dirigeants israéliens et palestiniens, repoussant en retour toute résolution
  de la situation dans un sens progressiste.

Outre la situation dramatique des Palestiniennes et des Palestiniens
emprisonné·es, assassiné·es, humilié·es, dépossédé·es etc. qui en sont
de loin les principales victimes, la situation de guerre plus ou moins larvée
qui dure depuis des décennies est donc également extrêmement nuisible pour la
société israélienne.

**Pour éviter une fuite en avant guerrière et autoritaire de plus en plus cauchemardesque,
la paix dans la justice (ce qui suppose la rupture avec le statu quo colonial)
est donc nécessaire**.

Cette paix aura obligatoirement comme préalable la désescalade de la violence
et un vrai dialogue avec l’ensemble des forces palestiniennes.

Elle donnera nécessairement lieu à des concessions importantes et impliquera
des réparations pour les populations qui ont subi l’arbitraire colonial.

En attendant qu’elle arrive enfin, nous apportons tout notre soutien aux
Palestinien.nes et aux Israélien.nes qui souffrent des conséquences de la
guerre, de la colonisation, du racisme, et de la fascisation plus ou moins
rampante de leurs sociétés.

Nous le conseillons donc aux Juifs et aux Juives de France qui ont les yeux rivés
sur le Jourdain : Si vous avez à cœur la situation là-bas, profitez du recul
dont vous bénéficiez pour pousser au dialogue et à l’apaisement.

Agiter des drapeaux en revendiquant un soutien inconditionnel alors que la politique
du gouvernement israélien est indéfendable ne fait qu’aggraver une situation
déjà désastreuse.

Nous invitons également nos lecteurs et lectrices à participer à toute initiative
et rassemblement en faveur d’une paix juste, durable et équitable et des
droits des personnes palestiniennes, **du moment que ce n’est pas un prétexte
à l’expression de la haine antisémite**.

**La paix, la justice, le refus du racisme sont les seules solutions !**
