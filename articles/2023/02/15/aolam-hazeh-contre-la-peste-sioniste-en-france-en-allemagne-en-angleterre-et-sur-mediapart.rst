.. index::
   pair: UJFP ; Haolam Hazeh contre la "peste sioniste" (sic) en France, en Allemagne, en Angleterre, et sur Mediapart (2023-02-15)

.. _jjr_2023_02_15:
.. _ujfp_2023_02_15:

==============================================================================================================================
2023-02-15 **Haolam Hazeh contre la "peste sioniste" (sic) en France, en Allemagne, en Angleterre, et sur Mediapart**
==============================================================================================================================

- https://juivesetjuifsrevolutionnaires.wordpress.com/2023/02/15/haolam-hazeh-contre-la-peste-sioniste-sic-en-france-en-allemagne-en-angleterre-et-sur-mediapart/


Haolam Hazeh contre la "peste sioniste" (sic) en France, en Allemagne, en Angleterre, et sur Mediapart
--------------------------------------------------------------------------------------------------------------

Nous avons pris connaissance d’un texte publié, puis retiré du site de
l’UJFP.

Son titre : "Mediapart veut faire des sionistes de gauche nos porte-paroles ;
l’équipe d’Haolam Hazeh s’y refuse".

Haolam Hazeh est une émission Juive disponible sur une chaîne twitch proche
des thèses du PIR et dans laquelle interviennent principalement des membres
de l’UJFP.

Une rapide recherche sur internet permet de retrouver le texte en cache.

Dans celui-ci, les auteurs s’en prennent à Illana Weizman, à Jonas Pardo,
ainsi qu’à la journaliste Valentine Oberti et à Mediapart, coupables de ne
pas les avoir invités pour parler d’antisémitisme (on se demande pourquoi
…).

Les attaques contre Ilana et Jonas, qui semblent être les cibles d’une
véritable obsession, sont **extrêmement violentes et personnelles** (on évoque
notamment leur "pédigrée", comme s’ils étaient des chiens), **nous leur
apportons donc tout notre soutien**.

Les auteurs du texte y prennent également la défense des thèses d’Houria Bouteldja,
dont ils semblent mal supporter qu’Alexis Corbière (comme la quasi-totalité
de la gauche) fasse si peu de cas, et nous décrivent comme faisant partie
d’un "réseau sioniste" participant "activement et de concert avec toutes les
instances officielles de représentation juives les plus pro-sionistes et
réactionnaires (le CRIF et la LICRA en tête) à la délégitimation de la lutte
pro-palestinienne sous couvert de combattre un soi-disant antisémitisme larvé
dans les formations de gauche et d’extrême-gauche".

Le texte termine de manière lyrique par cette affirmation : "Nous refusons
que la France devienne gangrenée par la peste sioniste comme le furent l’Allemagne
et l’Angleterre."

Si ces passages se passent de commentaires, essayons quand même, au moins pour
éclaircir certains points dans l’esprit de celles et ceux qui l’ont rédigé :


L’association faite entre des Juifs·ves et une gangrène ou une peste n’est pas anodine historiquement
============================================================================================================

L’association faite entre des Juifs·ves et une gangrène ou une peste
n’est pas anodine historiquement.

Ce type de comparaison est une constante du discours antisémite sur le temps long,
depuis les accusations faites aux Juifs·ves de disséminer la peste ayant mené
aux pogroms des années 1348-1349, dont celui de Strasbourg dit massacre de
la Saint-Valentin où 2000 Juifs·ves accusés d’empoisonner les puits sont brûlés
vifs le 14 février 1349, jusqu’à leur assimilation nazie à un cancer qu’il
faudrait extirper, en passant par Martin Luther qualifiant en 1543 les Juifs·ves
de "gangrène".

Si ce type de propos est habituel chez le fasciste Alain Soral, nous devrions être
interpellé·e·s que des militant·e·s se prétendant antiracistes puissent
utiliser un tel vocabulaire pour désigner des Juifs·ves avec lesquels ils sont
en désaccord politique (en l’occurence ici de "sionistes" (sic), **dont leur
définition semble se résumer à "l’ensemble des courants ne partageant pas
le déni de l’UJFP en matière d’antisémitisme"**, quelles que soient leurs
positions politiques réelles par ailleurs).


L’objectif de JJR est la lutte contre l’antisémitisme (y compris à gauche)
===============================================================================

L’objectif de JJR est la lutte contre l’antisémitisme (y compris à
gauche donc, famille politique qui, contrairement à ce que les rédacteurs du
texte semblent penser, n’est pas magiquement immunisée contre le racisme
présent dans la société) et le développement d’une option progressiste
au sein de la minorité juive.

Il n’est pas de décrédibiliser la cause palestinienne avec laquelle, en tant
qu’anti-colonialistes nous sommes en sympathie.

Une simple lecture de nos textes permettra de s’en assurer et d’invalider une
telle théorie complotiste.

Pour information, notre position sur la situation en Israël / Palestine peut
facilement être trouvée ici :

- https://juivesetjuifsrevolutionnaires.wordpress.com/2021/05/18/aux-juifs-et-aux-juives-de-france-a-propos-disrael/.

Ce dans quoi s’inscrit en réalité le texte d’Haolam Hazeh, c’est la longue
tradition d’injonction géopolitique (https://juivesetjuifsrevolutionnaires.wordpress.com/2016/02/15/a-propos-dinjonction-geopolitique/)
qui consiste à faire d’un préalable à toute discussion sur l’antisémitisme
**une injonction à se positionner pour les Juifs et Juives de la diaspora sur
l’État d’Israel**.

**Cette injonction est raciste et a pour but principal de neutraliser toute
discussion de fond sur l’antisémitisme en France**.

Nous refusons le tri entre les "bon·ne·s" et les "mauvais·es Juifs·ves"
quand il est question d’antisémitisme, que ce tri vienne d’une partie des
courants sionistes (qui s’emploient à disqualifier les Juifs·ves progressistes
sous prétexte qu’ils ne partagent pas leur soutien à la politique de l’État
ou du gouvernement israélien) ou des courants gravitant autour de l’UJFP et
de leurs alliés.


JJR cherche à intervenir au sein de la minorité juive
===========================================================

JJR cherche à intervenir au sein de la minorité juive.

Cela passe par la diffusion de textes, par la discussion au sein des communautés,
etc.

À aucun moment par contre nous n’avons agi de concert avec le CRIF ou la LICRA.

Au contraire, nous critiquons de manière régulière les tendances droitières
au sein de la minorité juive (voir notamment :
https://juivesetjuifsrevolutionnaires.wordpress.com/2016/03/10/le-diner-du-crif-ou-linternationalisme-de-la-bourgeoisie/
et, dernier exemple en date, notre article du 26 janvier 2023 concernant une
interview du grand rabbin Haïm Korsia : https://juivesetjuifsrevolutionnaires.wordpress.com/2023/01/26/haim-korsia-michel-houellebecq-et-les-territoires-perdus/).

**Là encore, il s’agit d’une vision complotiste.**


.. _ujfp_houria_2023_02_15:

La citation d’un texte d’Houria Bouteldja (qu’ils désignent comme une "camarade et sœur de lutte")
========================================================================================================

La citation d’un texte d’Houria Bouteldja (qu’ils désignent comme une **"camarade
et sœur de lutte"**) dans lequel celle-ci se consacre avant tout à la déresponsabilisation
de Mohamed Merah du crime qu’il a commis, et à appeler à l’empathie envers
celui-ci sans un mot pour ses victimes nous semble une bien mauvaise manière
de prouver qu’elle n’aurait rien d’antisémite.

Pour rappel, entre autres faits d’armes, l’essayiste avait justifié le déferlement
de messages et les harcèlements antisémites comprenant des appels explicites
au meurtre et au viol visant April Benayoum, Miss Provence 2020, car celle-ci
avait évoqué des origines familiales israéliennes.

Nous avons pour notre part dès 2015 dénoncé les théories d’Houria Bouteldja,
reprises notamment par l’UJFP, dans cet article : https://juivesetjuifsrevolutionnaires.wordpress.com/2015/04/27/contre-lantisemitisme-la-politique-du-pire/
et à nouveau en 2019 ici : https://juivesetjuifsrevolutionnaires.wordpress.com/2019/11//6/quel-antiracisme-politique/


Pour rester dans le thème complotiste
============================================

Pour rester dans le thème complotiste, prétendre que l’exclusion de
Jeremy Corbyn serait le résultat d’une "cabale" (les rédacteurs du
texte semblent ignorer la connotation antisémite de ce terme, qui renvoie
à la kabbale et à la propension supposée des Juifs·ves à conspirer)
organisée par les sionistes nous semble pour le moins aventureux.

Il existe en effet une masse de témoignages (1000 plaintes) concernant l’antisémitisme
au sein du Labour et l’absence de réaction appropriée de la direction
du parti.

Pour plus de détails nous renvoyons notamment à l’article "Mais que se passait-il
dans le Labour de Corbyn ?" paru dans la revue K (https://k-larevue.com/mais-que-se-passait-il-dans-le-labour-de-corbyn/)
et au témoignage "Tout ce que j’aurais aimé ne jamais savoir sur
l’antisémitisme au sein du Labour", traduit par le collectif Golema
(http://golema.net/temoignages/tout-ce-que-jaurais-aime-ne-jamais-savoir-sur-lantisemitisme-au-sein-du-labour-sara-gibbs/).


Estimer que le sionisme serait une "profanation de la foi juive et des enseignements talmudiques"
====================================================================================================

Estimer que le sionisme serait une "profanation de la foi juive et des enseignements
talmudiques" nous semble également une affirmation correspondant aux théories
de groupes religieux ultra-réactionnaires, dont le plus connu est le Neturei Karta.

Cela va à l’encontre de ce qu’on peut attendre d’un groupe se réclamant de
l’antiracisme politique, d’autant plus quand celui-ci se revendique dans le
paragraphe précédent d’une filiation avec le Bund, organisation juive laïque.

Il y a pourtant bien d’autres critiques valides du sionisme sur le plan politique
(notamment son impact concret sur les Palestinien·nes, la critique du nationalisme,
la tendance initiale à l’homogénéisation des cultures juives, etc.).


.. _jjr_bund_2023_02_15:

Concernant cet héritage bundiste, dont JJR se revendique également
=====================================================================


Concernant cet héritage bundiste, dont JJR se revendique également parmi
d’autres courants du mouvement ouvrier.

L’action et les propositions du Bund de lutter contre l’antisémitisme en le
liant à la révolution socialiste (c’est la doctrine de la Doïkayt, la lutte
"ici et maintenant") et en refusant l’option nationaliste au problème de
l’antisémitisme nous semble juste.

Cependant, il nous semble malhonnête de dresser une ligne absolue séparant
ceux qui seraient les "bon·ne·s Juifs·ves" partisans du Bund antisionistes
des "mauvais·es Juifs·ves" sionistes.

L’antisionisme du Bund est basé en partie sur le constat de la présence
d’importantes masses juives en Europe de l’Est, en partie également sur une
défense de la culture yiddish, **deux données qui ont été dramatiquement modifiées
par le génocide**.

De plus, le Bund ne s’est jamais empêché de faire front contre l’antisémitisme
ou la bourgeoisie avec certains sionistes, notamment avec les marxistes du Poaleï
Zion.

Enfin, une très grande majorité des bundistes ont été exterminés durant la Shoah
et **une grande partie des survivant·es ont ensuite émigré aux États-Unis et
en Israël**.

Bref, les positionnements pris à l’époque doivent être replacés dans le contexte
particulier d’un monde yiddish malheureusement en grande partie disparu.

Cela ne signifie pas que la doctrine de la Doïkayt soit caduque, mais que sa
formulation et la stratégie pour la mettre en oeuvre doivent prendre en compte
la situation actuelle des Juifs·ves en diaspora et non être réduite à un mythe
historique mobilisé essentiellement au passé.


Enfin, le terme "sioniste" est une qualification politique
==============================================================

Enfin, le terme "sioniste" est une qualification politique, cela ne correspond
pas au simple fait d’avoir des désaccords avec l’UJFP.

L’affirmation selon laquelle JJR serait une organisation "sioniste" ne peut,
et pour cause, reposer sur aucune de nos prises de position.

Si nous avons pris la peine d’écrire une réponse, bien que le texte ait été dépublié,
c’est qu’il ne s’agit pas d’un article isolé.

Il s’inscrit dans un contexte d’attaques lancées par certains militant·e·s
du courant dit "décolonial" (parfois également autoproclamé "antiracisme politique")
contre des militant·e·s antiracistes suite à la publication du livre
d’Illana Weizman ("Des Blancs comme les autres ? Les Juifs, angle mort de l’antiracisme",
que nous avions chroniqué ici : https://juivesetjuifsrevolutionnaires.wordpress.com/2022/11/08/des-blancs-comme-les-autres-les-juifs-angle-mort-de-lantiracisme/)
dont **illes semblent convaincu·e·s qu’il leur est consacré**.

Un **tel narcissisme** est pourtant hors de propos si l’on constate que seules
six pages sur les deux-cents-vingt que compte l’ouvrage évoquent leurs théories
(de la même manière, illes semblent penser que les formations dispensées
par Jonas Pardo sont centrées sur elleux, une brève visite de la page Facebook
de celles-ci suffit pourtant pour s’assurer qu’il n’en est rien).

Dans la même veine que ce texte, on peut évoquer également deux publications
du site QG Décolonial, la première ("Les habits neuf du sionisme de gauche")
extrapolant les positions de tout un courant antiraciste à partir d’un twitt
supprimé du RAAR ; la seconde ("Ilana Weizman sait-elle ce qu’est l’antisémitisme ?")
difficilement lisible, qualifiant pêle-mêle Illana Weizman, Jonas Pardo, le RAAR, JJR, etc.
de "libéraux" ou de "réactionnaires" sans visiblement que le sens de tels
qualificatifs soit compris.

Jusque là nous n’avons trouvé nulle raison de réagir au vu du ridicule et de
la faiblesse des arguments mobilisés, mais la teneur et la violence de ce
troisième texte, même retiré, oblige à une réponse.


Exacerbation des tensions dans nos luttes
=====================================================

Il nous semble quant à nous évident que des personnes écrivant des textes de
ce type ne participent pas à la construction d’une lutte anti-raciste forte de
la diversité de ses points de vue, mais au contraire à exacerber les tensions
qui demeurent dans nos luttes.

D’autant plus que ce n’est pas la première fois que l’UJFP publie puis efface
sans l’assumer un texte extrêmement problématique, pensons ici à leur article
sur l’usage du mot "Shoah" qui relativisait les crimes nazis et dont nous avions
fait la critique ici : https://juivesetjuifsrevolutionnaires.wordpress.com/2021/05/04/critique-du-texte-de-lujfp-sur-lusage-du-terme-shoah/

Nous répétons donc ce que nous écrivions déjà en 2015 au sujet de l’UJFP
(https://juivesetjuifsrevolutionnaires.wordpress.com/2015/04/27/nos-positions-sur-lujfp/):

::

    "Nous ne pouvons espérer qu’une chose : c’est qu’elle rompe avec cet
    aveuglement et qu’elle se reprenne.

    Ceci dans l’intérêt de la minorité juive, du combat antiraciste, et enfin
    de la solidarité anticolonialiste, que les dérives antisémites comme la
    complaisance vis à vis de l’antisémitisme ne font qu’affaiblir."


Nous invitons chacun·e à en tirer les conséquences qui s’imposent.
