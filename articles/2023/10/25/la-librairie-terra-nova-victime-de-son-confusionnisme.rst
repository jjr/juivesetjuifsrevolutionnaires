.. index::
   pair: TerraNova ; La librairie Terra Nova victime de son confusionnisme (2023-10-25)
   pair: Houria Bouteldja ; La librairie Terra Nova victime de son confusionnisme (2023-10-25)

.. _terra_nova_2023_10_25:

=========================================================================
2023-10-25 **La librairie Terra Nova victime de son confusionnisme**
=========================================================================

- https://juivesetjuifsrevolutionnaires.wordpress.com/2023/10/25/la-librairie-terra-nova-victime-de-son-confusionnisme/


La librairie Terra Nova victime de son confusionnisme
==========================================================

Depuis quelques mois, et la sortie de son nouveau livre, les théories
d’Houria Bouteldja, que l’on croyait oubliées, semblent connaître un
retour en force en librairie et chez certains militant·es actifs sur les
réseaux sociaux. 

Ainsi, à l’annonce d’une présentation de son livre à la librairie toulousaine 
Terra Nova, des camarades toulousain·es qui se sentaient jusque là proche 
de ce lieu l’ont contacté pour demander une annulation de l’événement.

En dépit **d’un mail respectueux et extrêmement pédagogique** adressé à
la librairie, l’invitant de surcroît à un temps d’échange préalable,
celle-ci a décidé de maintenir la soirée sans autre forme de débat. 

**Nous ne nous attarderons pas ici sur la réponse proprement affligeante de Terra Nova**.

Nous tenions simplement à saluer le collage réalisé sur la vitrine de
la librairie (« Souviens toi Hozar Ha Torah » – en référence au **texte
abject d’Houria Bouteldja** quelque jours après les tueries antisémites de
Mohamed Merah). 

**Cette initiative rassurante démontre que la gauche n’est pas indistinctement 
aveugle au caractère réactionnaire de la pensée d’Houria Bouteldja**.


.. _tsedek_2023_10_25:
.. _ujfp_2023_10_25:

2023-10-25 **Sur Tsedek et l’UJFP et leur soutien à Bouteldja**
==================================================================

Suite à ces événements, la librairie, les éditions La Fabrique, Houria
Bouteldja ainsi que Tsedek et l’UJFP ont communiqué sur « l’agression
», les « intimidations » et la « campagne sournoise » dont ils seraient
victimes. 

**Nous nous étonnons de l’utilisation de tels qualificatifs étant
donné ce qu’ils recouvrent**. 

Il est par ailleurs curieux que celleux-la mêmes
qui soutiennent la « résistance palestinienne sous toutes ses formes »,
incluant **la torture, la mutilation et l’immolation de civils et d’enfants**
ou qui, comme La Fabrique, **se sont faits connaître en éditant des textes
romantisant la violence révolutionnaire**, se déclarent « choqué·es et
blessé·es » par un mail et un collage. 


Terra Nova ne fait que payer le prix de son confusionnisme  à but lucratif
============================================================================

En définitive, Terra Nova ne fait que payer le prix de son confusionnisme 
à but lucratif. 

Tâchons d’y voir du positif : désormais les choses sont claires, Terra Nova 
est du côté des réactionnaires.

