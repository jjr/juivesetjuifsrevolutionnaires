.. index::
   pair: Houria Bouteldja ; ombien de fois faudra-t-il répéter que Houria Bouteldja n’est pas une camarade ? (2023-10-24)

.. _bouteldja_2023_10_25:

=======================================================================================================
2023-10-24 **Combien de fois faudra-t-il répéter que Houria Bouteldja (HB) n’est pas une camarade ?**
=======================================================================================================

- https://juivesetjuifsrevolutionnaires.wordpress.com/2023/10/25/la-librairie-terra-nova-victime-de-son-confusionnisme/
- :ref:`antisem:houria_bouteldja`

Introduction
================

Depuis quelques mois, et la sortie de son nouveau livre, les théories
d’Houria Bouteldja, que l’on croyait oubliées, semblent connaître
un retour en force en librairie et chez certains militant·es actifs sur
les réseaux sociaux.

Ainsi, à l’annonce d’une présentation de son livre à la librairie toulousaine
Terra Nova, des camarades toulousain·es qui se sentaient jusque là proche
de ce lieu l’ont contacté pour demander une annulation de l’événement.

.. _tsedek_2023_10_24:
.. _ujfp_2023_10_24:

2023-10-24 Les organisations **Tsedek et UJFP ont apporté leur soutien à Houria Bouteldja**
=====================================================================================================

La conférence a eu lieu, nos échanges ont été rendus publics et les
organisations Tsedek et UJFP ont apporté leur soutien à Houria Bouteldja.

Nous profitons donc de l’occasion pour rappeler ce qui demeure problématique
dans ses théories.

Nous avions déjà écrit deux textes à son sujet:

- :ref:`l’un en 2015 <jjr_pir_2015_04_27>`
- :ref:`et l’autre en 2019 <jjr_antiracisme_2019_11_06>`

Pour des raisons de lisibilité, **nous avons préféré ne pas évoquer
les nombreuses déclarations provocatrices, homophobes ou antisémites** de
l’autrice pour nous concentrer sur la théorie générale dans laquelle
elles s’inscrivaient.


1. La théorie du « philosémitisme d’État »
===============================================

1. La théorie du « philosémitisme d’État » considère que
l’antisémitisme est une réaction face à un traitement préférentiel qui
serait fait aux Juif·ves au détriment des autres minorités, notamment en ce
qui concerne la lutte contre l’antisémitisme. En réalité, l’antisémitisme
est protéiforme, ce qui explique qu’il traverse les tendances politiques
et les groupes sociaux. Il s’adapte à différents systèmes de pensées
qui se trouvent être compatibles avec le narratif antisémite. Il est aussi
la reproduction de préjugés antijuifs ancrés depuis plusieurs siècles. Il
peut donc se manifester de multiples façons, allant de l’affirmation d’une
identité nationale fantasmée blanche et chrétienne, à la propagande en
faveur de régimes réactionnaires, militaires ou religieux du Moyen-Orient,
pour ne citer que ces deux exemples. En taisant cette complexité pour en
faire un phénomène purement réactionnel, les défenseur·es de la théorie
du « philosémitisme d’État » prônent en pratique l’idée absurde
que le meilleur moyen de faire disparaitre l’antisémitisme serait de
cesser de s’y opposer.  Rappelons ici que les outils législatifs de lutte
contre l’antisémitisme, ainsi que l’éducation faite par l’État et la
société civile autour de ce sujet ne sont pas un traitement de faveur, mais
une victoire des luttes juives pour la reconnaissance de la responsabilité de
l’État français vis à vis du régime de Vichy, et de sa participation à
la Shoah. Par ailleurs, au delà des beaux discours politiques de tous bords
contre l’antisémitisme, la réalité vécue par les Juif·ves en France ne
correspond pas à celle d’une minorité choyée ou privilégiée. A l’école,
dans la rue, au travail, nous subissons l’antisémitisme, et sa reconnaissance
par les institutions est encore le fruit de luttes.

De plus, quand bien même ce « philosémitisme d’État » se ferait
apparemment malgré elleux, Houria Bouteldja somme tout de même les Juif·ves
de s’en désolidariser. Pour cela, iels doivent s’opposer à la lutte contre
l’antisémitisme (qui serait imposée par l’État racial) et surtout au «
sionisme ». L’essayiste attribue en effet une place centrale à ce dernier
thème. Pour elle, l’antisionisme représente ce qui permettrait in fine de
séparer le groupe blanc du groupe « indigène » pour qui « l’antisionisme
est [la] terre d’asile ». Ainsi, le positionnement qu’elle assigne aux
Juif·ves, qui ne sont ni vraiment « blanc·hes » ni vraiment « indigènes
», est conditionné à l’opposition ou non à ce qu’elle nomme le «
sionisme ». Le groupe des Juif·ves est contraint de se subordonner à sa
propre conception politique, de montrer patte blanche, à défaut de quoi les
Juif·ves seraient alors « blanc·hes ». Une telle centralité accordée
en Europe à un sujet comme le conflit israélo-palestinien est éminemment
problématique et peut rappeler par certains aspects l’accusation antisémite
classique de double allégeance. La discussion autour de ce sujet n’est pas
interdite, mais elle ne peut être un critère déterminant pour définir le
positionnement d’un groupe au sein du système social racial en France. Ce
discours participe à un isolement politique des Juif·ves et de leurs luttes
actuelles notamment contre l’antisémitisme présent en France, véhiculé
en partie par ce genre de discours.


2. Le cœur de la théorie d’Houria Bouteldja est le refus de considérer les contradictions internes aux groupes sociaux raciaux.
====================================================================================================================================


2. Le cœur de la théorie d’Houria Bouteldja est le refus de considérer les
contradictions internes aux groupes sociaux raciaux. Les « indigènes », groupe
qu’elle présente comme un bloc monolithique, fait référence au régime
colonial français de l’indigénat. Tout groupe social quel qu’il soit,
faisant partie d’une minorité nationale ou non, est traversé par plusieurs
champs d’oppression : entre LGBTQI+ et hétéro-cisgenres, entre hommes et
femmes, entre bourgeois et prolétaires, et ainsi de suite. La conséquence
logique de ces oppositions est l’existence de courants réactionnaires y
compris au sein dudit groupe « indigène ».  Houria Bouteldja se fixe pour
stratégie d’ignorer ou de marginaliser ces combats en les considérant comme
imposés de l’extérieur par le groupe blanc dominant au lieu de tenter
de trouver des intersections entre les luttes sur ces différents fronts de
manière à s’opposer à toutes les oppressions. Pour elle, la lutte contre
l’antisémitisme ou les LGBTQIphobies ne répondent pas à une exigence
d’égalité et de dignité mais seraient des manifestations idéologiques
du système racial qui prendraient, dans la lignée des Lumières, la forme
du progressisme et de l’universalisme « blanc » avec pour conséquence
une marginalisation du groupe « indigène ». Son objectif de construire une
unité politique « indigène » ne peut alors se réaliser que sur une ligne
anti-moderne et donc réactionnaire.


3. Houria Bouteldja propose dans son dernier ouvrage
========================================================

3. Houria Bouteldja propose dans son dernier ouvrage la convergence des « beaufs
» et des « barbares », groupes sociaux définis avec une absence habituelle
manifeste de finesse sociologique, autour des « affects » mobilisés par
Alain Soral. Ces derniers ne sont pas neutres, ils relèvent de l’adresse
à des populations économiquement dominées craignant un déclassement
supplémentaire sur le champ racial ou de genre que la minorisation des
femmes, des LGBTQI+ et des Juif·ves leur permettrait d’éviter. Le «
pacte soralien » que Houria Bouteldja tente de réduire à des « affects
», c’est la promesse aux prolétariats masculin blanc et « indigène »
d’une supériorité par rapport aux Juif·ves, aux femmes, et aux LGBTQI+
légitimée par une idéologie complotiste dont les idées suprémacistes sont
le ciment. Séparer l’adhésion idéologique au discours soralien de son
essence complotiste, antisémite, masculiniste et nationaliste, participe à
une non-compréhension du phénomène politique et social qu’il représente,
dans une tentative de présenter ses adhérents comme des « fâchés pas
fachos ». Notons ici qu’alors que les Juif·ves doivent faire profession
de foi antisioniste avant d’être considéré·es comme des allié·es,
la question d’un positionnement politique préalable ne semble pas se
poser concernant les « beaufs ». En conséquence, l’alliance prônée
par l’essayiste, si elle se réalise un jour, regroupera naturellement les
franges les plus réactionnaires des groupes sociaux raciaux blancs et «
indigènes » faussement identifiées à la totalité de ces derniers. Elle
ne pourra se réaliser qu’au détriment des Juif·ves, LGBTQI+, femmes,
etc. Celles et ceux qui seraient opprimé·es ou souhaiteraient lutter sur
ces fronts, y compris de l’intérieur du groupe « indigène », sont donc
régulièrement intimé·es au silence par Houria Bouteldja et ses partisan·es.

Ces théories sont fausses et celles et ceux qui les portent ne peuvent en
aucun cas se revendiquer du champ de l’antiracisme ou en être de quelque
manière que ce soit les allié·es. La lutte contre le racisme ne doit pas être
menée en s’opposant à la lutte contre l’antisémitisme, le sexisme et les
LGBTQIphobies. Nos luttes antiracistes ne peuvent être menées en s’alignant
sur des positions réactionnaires ou sur des stratégies politiques aux relents
nationalistes inspirées par des mouvements fascisants.

Quelques ressources disponibles sur internet pour celles et ceux qui souhaiteraient approfondir le sujet
===============================================================================================================


- « Bouteldja, ses « sœurs » et nous ». Mélusine, octobre 2016 https://infokiosques.net/spip.php?article1356 « Bouteldja,
- « une sœur » qui vous veut du bien ». Lala Mila, août 2017. https://infokiosques.net/spip.php?article1469 « Une indigène au visage pâle »,
- Ivan Segré, mars 2016 https://lundi.am/Une-indigene-au-visage-pale

Podcast en 2 parties sur le philosémitisme d’état, de Martin Eden, avec Memphis Krickeberg.

- https://youtu.be/R3Zt0jjq00E?si=wPEGPYRqq4h7vdUA
- https://youtu.be/s15q48Q3J3U?si=JR0VotMHoXuGxmIo Ms Dreydful,

à propos du détournement de la pensée d’Audre Lorde par HB

- https://msdreydful.tumblr.com/post/97563425901/f%C3%A9ministes-ou-pas-penser-la-possibilit%C3%A9-dun
