.. index::
   pair: Soutien ; confédéralisme démocratique 
   ! Murray Bookchin

.. _jjr_kck_2023_12_24:

=========================================================================
2023-12-24 **Soutien internationaliste au confédéralisme démocratique**
=========================================================================


Soutien internationaliste au confédéralisme démocratique
==========================================================

- https://blogs.mediapart.fr/juives-et-juifs-revolutionnaires/blog/241223/soutien-internationaliste-au-confederalisme-democratique

Le 6 Janvier 2024, c'est la manifestation du mouvement social confédéraliste
démocratique.

Nous souhaitons honorer la mémoire de :ref:`Sakine Cansz (Sara), Fidan Dogan
(Rojbîn) et Leyla Saylemez (Ronahî) <kurdistan_luttes:assassinats_2013_01_09>`, 
assassinées par un agent des services secrets turcs à Paris le 9 Janvier 2013.

Le 23 décembre 2022, c'est aussi le centre culturel kurde Ahmet Kaya qui est
la cible d'une attaque terroriste qui cible entre autres :ref:`une responsable du
mouvement des femmes kurdes Emine Kara (Evîn Goyî), le chanteur Mir Perwer,
et le militant Abdurahman Kizil <kurdistan_luttes:assassinats_2022_12_23>`.

Ces féminicides du 9 Janvier 2013 sont aussi le fait d'une mentalité
masculiniste qui nous horrifie et contre laquelle nous luttons à la fois en
dehors et à l'intérieur de nos mouvements.

**Nous remercions le KCK (Union des communautés du Kurdistan) pour avoir adressé
ses condoléances au peuple Juif d’Israël et au peuple Arabe de Palestine
au lendemain de l'attaque du 7 octobre 2023**.

C'était une parole juste et nécessaire qui nous a fait chaud au cœur et
qui nous renforce dans nos combats pour l'unité des opprimé·es et en tant
que Juifs et Juives internationalistes engagé·es contre les colonisations.

**Nous adressons en retour tout notre soutien et notre solidarité à tous les
peuples qui construisent ensemble le confédéralisme démocratique et tous
les peuples en lutte contre les colonisations**.

C'est également pour nous l'occasion d'honorer la mémoire de figures qui
nous inspirent : **Murray Bookchin, théoricien ashkénaze du municipalisme
libertaire**, qui partagea ses écrits avec Abdullah Öcalan, leader du mouvement
kurde emprisonné depuis 1999 sur l'île prison d'Imrali.

Certain·es d'entre nous serons présent·es à la manifestation et d'autres
y seront présent·es avec le cœur, nous sommes avec vous camarades.


Liens
========

- https://mastodon.social/@KCK_Kurdistan_
- https://kck-info.com
- https://ypj-info.org/targeting-women/the-targeting-of-womens-liberation-pioneers-in-paris/

