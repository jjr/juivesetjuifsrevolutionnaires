.. index::
   pair: JJR ; Reponse des JJR à Bouteldja (2023-12-06)

.. _reponse_jjr_2023_12_06:

=========================================================================
2023-12-06 **Reponse des JJR à Bouteldja**
=========================================================================

- :ref:`antisem:magnaudeix_2023_12_05`

- https://x.com/JJR_JJR_JJR/status/1732075690609037804#m

Non Houria Bouteldja nous n'avons jamais demandé l'interdiction de cet 
événement, nous l'avons simplement dénoncé comme une mascarade en matière 
de lutte contre l'antisémitisme. 

- https://x.com/JJR_JJR_JJR/status/1732075784699859298#m

Une mascarade similaire à celle consistant à penser qu'il existe un complot 
sioniste allant de JJR, à la mairie de Paris et l'État en passant par Mediapart


- https://x.com/JJR_JJR_JJR/status/1732076254034088185#m

Le 6 décembre, un certains nombre d'organisations ont choisi d'organiser 
un meeting "contre l'antisémitisme et son instrumentalisation". 

L'état a choisi d'interdire ce meeting. 

- https://x.com/JJR_JJR_JJR/status/1732076411026886712#m

Nous avons déjà exprimé à plusieurs reprises nos profonds désaccord avec 
ces organisations, notamment en ce qui concerne leur traitement de 
l'antisémitisme, **et le fait qu'elles participaient par leurs positions 
à l'ostracisation des juifs et juives à gauche**. 

- https://x.com/JJR_JJR_JJR/status/1732076830696362490#m

Néanmoins nous continuons de penser que l'interdiction par l'état témoigne 
surtout d'une logique sécuritaire systématisée à laquelle on s'oppose. 

Ce n'est pas la réponse politique adéquate à ces discours et ça participe 
a la restriction des libertés publiques. 

- https://x.com/JJR_JJR_JJR/status/1732076906055422156#m

Nous pensons que la confrontation politique reste la plus à même de faire 
évoluer ce traitement à gauche. 
Dans ce sens, nous publierons prochainement un texte d'analyse faisant 
état des raisons de nos désaccords.
