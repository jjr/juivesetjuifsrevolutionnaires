.. index::
   pair: Alerte au monde féministe de gauche ; 2023-11-24
   pair: Vivian Silver ; 2023-11-24

.. _collage_2023_11_24:

=================================================================================================================================================
2023-11-24 **Réponse aux Collages Féminicides Paris et alerte au monde féministe de gauche**  (en mémoire de Vivian Silver, NDLR |vivian|)
=================================================================================================================================================

- https://juivesetjuifsrevolutionnaires.wordpress.com/2023/11/24/reponse-aux-collages-feminicides-paris-et-alerte-au-monde-feministe-de-gauche/

Communiqué d’excuses du collectif Collages Féminicides Paris (CFP)
=========================================================================

Le 13 novembre 2023, le collectif Collages Féminicides Paris (CFP) a publié
sur son compte Instagram une série de photos de collages sur la situation
en Israël-Palestine.

Dans les commentaires, plusieurs de leurs abonné·es ont attiré leur attention
sur les dénis et relativisations de l’antisémitisme de ces collages. 

Le **collectif CFP a publié sur son compte, quelques jours après, un communiqué
d’excuses, rectifiant les erreurs pointées par plusieurs personnes**. 

Merci à ORAAJ 
=================

Nous saluons cette remise en question salutaire, et remercions le :ref:`collectif ORAAJ <oraaj:oraaj>`
pour le travail pédagogique effectué.


Pour poursuivre ce travail, nous avons décidé d’approfondir le sujet
==========================================================================

Pour poursuivre ce travail, nous avons décidé d’approfondir le sujet. 

Notre post n’est en rien un call-out ou une vindicte à l’égard de ce groupe,
mais plutôt notre pierre à l’édifice en terme d’éducation contre
l’antisémitisme. 

Ces collages s’inscrivent dans un contexte de solitudes juives depuis le 7 octobre 2023, 
notamment dans des groupes militants de gauche, féministes et queer.

Si nous ne condamnons évidemment pas tout le travail de CFP, leurs collages sur
la situation Israël-Palestine nous ont heurté·e·s et interrogé·e·s. 

En tant que Juif·ves, sommes-nous en sécurité dans une manifestation féministe ?
=======================================================================================

La question peut se poser : à quelques jours de la manifestation du 25 novembre 2023
contre les violences sexistes et sexuelles, en tant que Juif·ves, sommes-nous
en sécurité dans une manifestation féministe ? 

Comme toute manifestation, qui est, en plus d’être un temps de revendications 
politiques et d’expression de solidarités, également un espace d’empouvoirement 
voire de résilience : y avons-nous aussi encore une place digne et sereine ? 

Devrions-nous cacher, renier nos judéités ou sinon, serons-nous sommé·es de 
nous justifier ou de prouver quoi que ce soit ?

Nous invitons les organisations et les voix juives et féministes de gauche
à s’emparer de ce sujet, ce samedi en manifestation et après.

**Collage "Les palestinien·nes aussi sont des sémites" : le mot "antisémitisme" dévoyé**
==========================================================================================

::

    Collage "Les palestinien·nes aussi sont des sémites" : le mot "antisémitisme" dévoyé

**Ce collage est peut-être celui qui nous a le plus choqué·e·s car il
démontre une ignorance totale du mot "antisémitisme"**.

L’antisémitisme veut dire racisme antijuif ; il ne veut pas dire "contre
le sémitisme" ou "contre les sémites". 

Ce mot a été inventé par Wilhem Marr, journaliste allemand antijuif de la fin 
du XIXe. 

Il est l’un des théoriciens ayant développé l’idée selon laquelle la soi-disant 
"race aryenne", germanique ou indo-européenne, était mise en danger par la 
"race sémite", c’est-à-dire les Juif·ves, qui sont vu·es comme étant
pour toujours "de sang mêlé", sans provenance précise. 

Nous rappelons qu’en France, ces théories se sont matérialisées par la création 
d’une Ligue Antisémitique, après le succès commercial du livre de Drumont La
France juive, mais aussi par un développement intellectuel et militant
d’extrême-droite extrêmement fourni.

**L’antisémitisme est donc aussi une idéologie, contre les Juif·ves**
=======================================================================

L’antisémitisme est donc aussi une idéologie, contre les Juif·ves. 

L’antisémitisme n’est pas uniquement un mot dont on peut décortiquer l’étymologie 
de façon déconnectée de son sens politique.

Ainsi, les Palestinien·nes ne sont pas des "sémites" parce que "les sémites" 
en tant que peuple ou race n’existent pas. 

Certes, il y a des langues sémitiques, en opposition aux langues indo-européennes. 
Cette classification linguistique ancienne a été reportée vers une classification
raciale dans le contexte du développement du racialisme, c’est à dire des
théories racistes pseudo-scientifiques du XIXe.


**Le mot antisémitisme désigne exclusivement la haine portée contre les Juif·ves**
=========================================================================================

**Mais, le mot antisémitisme désigne exclusivement la haine portée contre
les Juif·ves**, qui sont désigné·es comme des personnes intrinsèquement 
"étrangères" aux nations dans lesquelles elles résident.

C’est pourquoi ce slogan collé, "les Palestinien·nes sont aussi des sémites", 
vient à la fois souligner l’ignorance des colleureuses sur ces éléments racialistes 
nous venant tout droit de l’extrême-droite, mais aussi détourner l’attention 
de l’explosion de l’antisémitisme ces dernières semaines en France 
(plus de 1500 actes antisémites depuis le 7 octobre 2023).


En plus de délivrer le mauvais message, ce collage arrive au mauvais moment
==============================================================================

En plus de délivrer le mauvais message, ce collage arrive au mauvais moment :
le jour d’une marche nationale contre l’antisémitisme, qu’importent les
réserves ou critiques légitimes que l’on pouvait formuler à son encontre.

Ainsi, plutôt que de se tenir à côté des Juif·ves de France pour exprimer
leur soutien face à l’explosion des actes antisémites et faire front
face aux extrême-droites, des militant·e·s des Collages Féminicides Paris
ont brouillé le message en collant un slogan qui oppose, comme nous en avons
tristement l’habitude, défense du peuple palestinien sur le plan international
et défense des Juif·ves de France sur le plan national. 

**La lecture de nos précédents posts pourra démontrer que l’un n’exclut pas l’autre**.


Attribuer aux Palestinien·nes le qualificatif racial de "sémite" n’a donc aucun sens
======================================================================================

Attribuer aux Palestinien·nes le qualificatif racial de "sémite"
n’a donc aucun sens et n’est pas pertinent pour affirmer leur droit à
l’autodétermination et s’opposer aux bombardements sur des civil·e·s
à Gaza.


**Collage "Soutien au peuple juif décolonial" : un soutien conditionné ?**
=============================================================================

::

    Collage "Soutien au peuple juif décolonial" : un soutien conditionné ?

Nous posons la question : qu’est-ce que le "peuple juif décolonial" ? 

Une personne juive peut avoir des opinions "décoloniales", mais il n’existe pas 
de "peuple juif décolonial". 

D’ailleurs, la notion même de "peuple juif" est questionnée et questionnable. 

Nous parlons de minorité juive, plus couramment on parle de "communauté", ou 
"des communautés juives" : plus rarement de peuples, au pluriel.

Mais le collectif des Collages Féminicides Paris a choisi de coller "soutien
au peuple juif décolonial", **induisant donc que les autres personnes juives
dans leur ensemble n’ont pas son soutien**. 

Il est éminemment problématique de conditionner son soutien à une minorité 
discriminée en fonction de ses opinions, réelles ou supposées.

Des slogans tels que "soutien aux Juif·ves de France" ou " soutien à tous·tes  celleux qui subissent l’antisémitisme", auraient été plus à propos
======================================================================================================================================================

**Des slogans tels que "soutien aux Juif·ves de France" ou " soutien à tous·tes 
celleux qui subissent l’antisémitisme", auraient été plus à propos**.

En effet, la situation des Juifves de France est extrêmement anxiogène en ce
moment : beaucoup retirent la mezouza de leur porte, enlèvent leurs noms des
boîtes aux lettres, portent des casquettes plutôt que des kippas, demandent à
leurs enfants de ne pas dire à l’école qu’ils sont juif·ves, cherchent
à anticiper des attaques ou agressions à leur domicile alors que nous ne
sommes pas forcément "visibles" dans le sens de visibilité liée à des
tenues ou des objets religieux mais pourtant bien connu·es/identifié·es
comme personnes juifves dans nos quartiers.

Nous ne connaissons que trop cette situation de peur et de vigilance.

**Les Juifves de France doivent être soutenu·es lorsqu’iels subissent des actes racistes quelles que soient leurs convictions politiques**
================================================================================================================================================

Les Juifves de France doivent être soutenu·es lorsqu’iels subissent des actes
racistes, quelles que soient leurs convictions politiques. 

Les agresseur·euse·s ne demandent pas aux Juifves leurs opinions politiques 
avant de les agresser, ils/elles attaquent un·e Juif·ve, les Juif·ves en général. 

Il devrait en être de même pour les allié·es véritablement antiracistes : l’enjeu
est de défendre une personne qui subit une discrimination, pas de se poser en arbitre 
du conflit israélo-palestinien, et de classer les gens·tes en bon·ne·s ou mauvais·e·s 
Juifves et d’opérer de facto une "chasse aux sionistes" parmi nous. 

Ces éléments de language et cette réserve quant à la lutte contre l’antisémitisme 
n’ont que rarement été aussi poussés depuis des décennies. 

**Aucun·e Juif·ve ne mérite de se faire agresser au titre de sa judéité**
==========================================================================

**Aucun·e Juif·ve ne mérite de se faire agresser au titre de sa judéité.**

Dans son communiqué, le collectif CFP écrit ne pas avoir perçu le fait que
ce collage "soutien au peuple juif décolonial" puisse induire l’idée
d’un soutien conditionné. 

Nous n’avons pas toujours conscience des figures du "bon juif" et du "mauvais juif", 
mais ce sont des **tropes antisémites** depuis des siècles : à vous et nous de 
ne pas continuer à transmettre cela.

La même logique de soutien conditionné agit quand il s’agit d’exprimer
de l’empathie après les attaques du 7 octobre 2023 : les Israélien·ne·s ne
méritent-ielles donc aucun soutien ou aucune empathie parce que citoyen·ne·s
d’Israël ?

Un manque de soutien féministe aux victimes des attaques du 7 octobre 2023 (TW mention de violences des massacres du 7 octobre 2023)
======================================================================================================================================

::

    Un manque de soutien féministe aux victimes des attaques du 7 octobre
    (TW mention de violences des massacres du 7 octobre)

Les deux précédents collages que nous avons choisi de relever interviennent
dans un contexte mentionné précedemment. 

**Sur le compte des Collages Féminicides Paris, il est impossible de trouver la 
moindre marque de soutien aux femmes, aux enfants, et à toutes les personnes 
qui ont subi les terribles attaques du 7 octobre 2023, faisant plus de 1200 
mort·e·s.** 


Aucun collage non plus concernant les 236 personnes enlevées, dont des femmes et des bébés, et ceci plus d’un mois et demi après les faits
=============================================================================================================================================

Aucun collage non plus concernant les 236 personnes enlevées, dont des femmes 
et des bébés, et ceci plus d’un mois et demi après les faits.

Nous savons que le travail militant des colleureuses est un travail bénévole,
et que trop souvent, les militant·e·s ont des injonctions à réagir.

Toutefois, il est primordial pour nous de souligner auprès des colleureuses
et des autres groupes militants féministes que les massacres antisémites du
7 octobre 2023 visant hommes, femmes et enfants, se caractérisent aussi par des
violences sexuelles et actes de torture visant spécifiquement les femmes :

- femmes violées jusqu’à leur casser le bassin, 
- femmes enceintes éventrées,
- femmes décapitées, brûlées, 
- corps de femmes assassinées exhibés devant la foule… 

Si le qualificatif de "féminicide" pour qualifier les actes du Hamas ne fait 
pas consensus, même au sein de notre organisation, il n’y a aucun débat sur 
l’existence des violences ayant spécifiquement visé les femmes lors des massacres.


Nous déplorons de ce fait que les groupes féministes de notre camp politique soient restés si silencieux face à celles-ci
============================================================================================================================

Nous déplorons de ce fait que les groupes féministes de notre camp politique
soient restés si silencieux face à celles-ci.

**De nouveau, nous nous retrouvons au milieu des silences d’une partie de la
gauche, et de l’instrumentalisation de la droite**. 


**Mais nous refusons de nous taire**
=====================================

**Mais nous refusons de nous taire**. 

**Parler de ces violences, qui nous traumatisent en tant que femmes et minorités 
de genre juifves, est indispensable**.

Ces actes ignobles ont été filmés, souvent en direct, et mis en ligne sur
les réseaux sociaux, envoyés aux familles via les portables des victimes,
leur imposant une véritable torture psychologique.

Devant ces atrocités, nous déplorons dans les rangs féministes, une réticence
à poser les mots tortures physiques et psychologiques, viol comme arme de
guerre, violences sexuelles, victimes, enlèvements.

**Oui, il s’agit de crimes contre l’humanité. Non, il ne s’agit pas d’actes de résistance.**
=================================================================================================

**Oui, il s’agit de crimes contre l’humanité. 
Non, il ne s’agit pas d’actes de résistance**.

Nous voyons ici à l’oeuvre des processus que nous connaissons bien et les
colleureuses aussi : **la silenciation et la relativisation des violences que
subissent les femmes**.

Certains silences, dénis et justifications face aux crimes, certains manques
de soutien font mal, et surtout quand il surgit dans nos rangs, parmi les
militant·es féministes et antiracistes.

**Où sont vos "je te crois" pour les Israélien·nes ?**
=========================================================

**Où sont vos "je te crois" pour les Israélien·nes ?**

Pourquoi alors cela se produit-il ?

**D’une part, la déshumanisation des vies juives est une constante en France
ces dernières années**. 

Sans refaire toute la chronologie, les manifestations après le meurtre d’Ilan Halimi (13 février 2006, NDLR)
ont déplacé peu de non-juifves. 
Après les attentats du 7 janvier 2015, le fameux "je suis Charlie" a bien vite
éclipsé les victimes juives de l’Hypercasher.

D’autre part, depuis le 7 octobre 2023, **lorsque qu’une personne attire
l’attention sur les violences subies par les personnes israéliennes lors
des attaques, elle est immédiatement soupçonnée de nier la souffrance des
Palestinien·ne·s**.

.. _vivian_silver_2023_11_24:

2023-11-24 **Nous croyons qu’il est possible de concilier les deux, de voir les souffrances d’où qu’elles viennent** |vivian|
===================================================================================================================================

- https://www.tenoua.org/mort-dun-poeme/
- :ref:`womenwagepeace:vivian_silver`

**Nous croyons qu’il est possible de concilier les deux, de voir les souffrances
d’où qu’elles viennent**.

D’un point de vue féministe, cela semble être le B.A.ba : le soutien à
une femme ou minorité de genre victime de violences ne doit jamais être
conditionné à une inspection des circonstances de ces violences : sa tenue,
l’heure qu’il était, **et dans le cas présent, la nationalité des victimes**.

Le 14 novembre 2023, nous avons appris la mort de :ref:`Vivian Silver <womenwagepeace:vivian_silver>` |vivian|, 
tuée le 7 octobre au kibboutz Beeri. 

Cette femme juive était une militante pour la paix, et comme de nombreux·ses 
habitant·es des kibboutzim proches de Gaza, elle "accompagnait des Palestinien·nes 
dans des hôpitaux israéliens afin d’être soigné·es" et "elle luttait pour 
documenter les abus des forces de sécurité" (`voir le post de Tenoua <https://www.tenoua.org/mort-dun-poeme/>`_).

**Peut-être est-il tout simplement question de ne pas voir  le monde en noir et blanc, de manière binaire**
=============================================================================================================

- https://juivesetjuifsrevolutionnaires.wordpress.com/2023/11/08/nuances-et-discernement/

Peut-être n’est-il pas réellement question de savoir si Vivian Silver "mérite" 
ou pas un collage CFP, peut-être est-il tout simplement question de ne pas voir 
le monde en noir et blanc, de manière binaire. 

Devant la masse d’informations et d’images, la tentation de la simplification et
de l’action réactionnelle est grande. 
**Les instrumentalisations politiques, souvent populistes, sont nombreuses**.

Nous appelions dans notre texte `du 8 novembre 2023 <https://juivesetjuifsrevolutionnaires.wordpress.com/2023/11/08/nuances-et-discernement/>`_
à la nuance et aux discernements de nos allié·es. 

**Nous réitérons notre appel et essayons d’en faire preuve également**.

Dans un conflit aussi complexe, une chose reste simple, à nos portées à
tous·tes, c’est de manifester du soutien et de l’empathie aux personnes qui
ont subi un massacre, quel que soit leur "côté", ainsi qu’aux victimes
de l’antisémitisme et tous les racismes dans notre pays et partout dans le
monde depuis le début de la guerre.

Le comité féministe queer des JJR
