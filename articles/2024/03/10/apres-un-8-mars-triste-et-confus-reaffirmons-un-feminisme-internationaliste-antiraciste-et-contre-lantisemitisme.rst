.. index::
   ! Après un 8 mars 2024 triste et confus, réaffirmons un féminisme internationaliste, antiraciste et contre l’antisémitisme (2024-03-10)

.. _jjr_2024_03_10:

========================================================================================================================================================
2024-03-08 **Après un 8 mars 2024 triste et confus, réaffirmons un féminisme internationaliste, antiraciste et contre l’antisémitisme** par JJR
========================================================================================================================================================

- https://juivesetjuifsrevolutionnaires.wordpress.com/2024/03/10/apres-un-8-mars-triste-et-confusreaffirmons-un-feminisme-internationaliste-antiraciste-et-contre-lantisemitisme/
- https://blogs.mediapart.fr/juives-et-juifs-revolutionnaires/blog/110324/pour-un-feminisme-internationaliste-antiraciste-et-contre-lantisemitisme
- https://www.jstor.org/stable/23881894


Nous souhaitons rappeler que les femmes juives ont eu un rôle immense dans les luttes féministes, syndicales et socialistes
==============================================================================================================================

Après le 8 mars 2024, journée internationale de lutte pour les droits des femmes,
nous souhaitons rappeler que les femmes juives ont eu un rôle immense dans les
luttes féministes, syndicales et socialistes.

L’idée de cette journée a ainsi été proposée initialement par **Theresa Serber Malkiel** :ref:`[1] <[1]>`,
juive russe ayant immigré aux Etats-Unis, qui s’est battue pour le droit de
vote des femmes, pour le droit des travailleur·ses notamment immigré·es.

Le fait que son nom soit si peu connu, **témoigne d’une historiographie qui efface
sans cesse les personnes racisées et minorisées de genre de l’histoire
des luttes qui ont bénéficié à toustes**.

**Nous portons la mémoire de ces combats et plus que jamais nous nous indignons
quand ils sont déconsidérés, méprisés ou oubliés**.

C’est fort·es de cette histoire que nous souhaitons dénoncer aujourd’hui le
fait qu’en 2024 les personnes juives minorisées de genre soient tétanisées
face à un mouvement d’émancipation féministe en France qui peine à les
entendre et les considérer. On se souvient du 25 novembre 2023, pour la
marche contre les violences sexuelles : beaucoup d’entre nous n’avaient
pas réussi à faire entendre leurs voix et leur légitime colère concernant
les violences sexuelles qui avaient ciblé des femmes israéliennes le 7 octobre 2023.

Nous nous interrogeons, si ce soutien féministe qu’on attend à l’égard
de toutes les femmes victimes ne vient pas, est-ce parce qu’elles sont juives
ou supposées telles ?

En effet, indépendamment du caractère antisémite des violences du 7 octobre 2023,
c’est bien en l’occurrence sur la réception en France que l’on dénonce,
au mieux, la minimisation, au pire, la négation des violences sexuelles.


.. _btler_2024_03_10:

Où est le soutien inconditionnel à toutes les victimes ?
==========================================================

Où est le soutien inconditionnel à toutes les victimes, quel que soit même le
caractère raciste ou non des violences ?

**Les paroles de Judith Butler la semaine dernière demandant à voir les
preuves des viols des femmes israéliennes nous hantent**.
A-t-on déjà vu des féministes traiter avec autant de circonspection des victimes
de violences sexuelles ?

**A-t-on déjà entendu de la part de militant·es féministes un tel silence, de
telles minimisations et justifications, de tels soupçons ?**

Aussitôt le rapport de l’ONU publié sur les crimes sexuels commis par le
Hamas à au moins trois endroits de l’attaque, et le témoignage des otages
libéré⋅es sur des actes de torture et de violences sexuelles, on a pu
lire de la part de militant⋅es qu’il s’agissait en fait de mensonges,
qu’il n’y aurait pas de preuves, et que l’ONU serait manipulée par
"les sionistes". Dans ce rapport figure également le récit des entretiens en
Cisjordanie avec des détenu·es palestinien·nes, hommes et femmes, faisant
état de violences sexuelles sous la forme de fouilles corporelles et de
menaces de viol de la part de forces de l’ordre israéliennes.

Vous faut-il également voir des preuves ou décidez-vous de croire une seule
catégorie de victimes ? Nier ces violences, toutes les violences, est indigne
de quiconque se revendique de la gauche et d’un féminisme.

Il ne devrait y avoir "aucun débat sur l’existence des violences du 7
octobre 2023 ayant spécifiquement visé les femmes lors des massacres", comme nous
l’écrivions dans un texte le 24 novembre 2023.

Il ne devrait pas non plus y avoir de "débat" sur une nécessaire solidarité
avec les femmes palestiniennes dont les conditions de vie ou de survie empirent
de jour en jour.
Nous sommes horrifié⋅es de voir des soldats israéliens se photographier en exhibant
sur les réseaux sociaux les sous-vêtements de femmes palestiniennes comme
des trophées.

Nous ne devrions romantiser ni soutenir aucun mouvement ni aucun Etat ni aucun
groupe politique qui aspire à contrôler le corps des femmes, à les priver de
leurs libertés, à punir les individus ayant des identités de genre et de sexualité
considérées comme hors de la norme.


Alors soyons clair·es : cela n’inclut ni le Hamas, ni le gouvernement Netanyahu et sa coalition de néofascistes,ni le gouvernement français actuel
========================================================================================================================================================

La lutte révolutionnaire, la lutte anticoloniale et la lutte antiraciste ne
peuvent avoir de sens que si elles soutiennent le droit fondamental des
personnes minorisées de genre à disposer de leurs corps, à avoir accès a
la contraception, à l’IVG, à avoir le droit à une sexualité consentie
et épanouie, à avoir le droit de travailler en dehors du foyer ou non,
d’avoir des enfants ou non, de participer aux luttes sociales et politiques,
de voter, d’être les acteur·ices de leur vie et non les seconds rôles des
récits masculins hégémoniques, alors soyons clair·es : **cela n’inclut
ni le Hamas, ni le gouvernement Netanyahu et sa coalition de néofascistes,
ni le gouvernement français actuel et sa volonté nataliste de lutte contre
l’immigration**, et certainement pas les Etats-Unis et l’explosion de leurs
lois liberticides envers les femmes et les personnes trans, particulièrement
les attaques contre l’IVG.


Peut-on se représenter ce que vivent les militant·es juifves qui ont toujours  participé à ces luttes ?
==========================================================================================================

Le 7 mars 2024, dans plusieurs villes de France étaient organisées des marches
féministes radicales dans lesquelles nos militant·es ont vu et entendu des
slogans antisémites.

Comment peut-on encore les laisser passer ?
Peut-on se représenter ce que vivent les militant·es juifves qui ont toujours
participé à ces luttes ?

Le 8 mars 2024, à Bordeaux, les femmes du collectif **Nous vivrons** dénonçant
les violences sexuelles ayant eu lieu le 7 octobre 2023 en Israël ont
été empêchées de manifester.

L’inter-orga a considéré que la simple dénonciation de ces violences sexuelles
ne pouvait être qu’une provocation "sioniste" provenant de "l’extrême droite"
et de la Ligue de défense juive (LDJ), organisation d’extrême-droite.

Résultat, le service d’ordre a physiquement empêché les femmes de **Nous vivrons**
de rejoindre le cortège.

À Paris, le même collectif a passé la manifestation entière sous les slogans
"fascistes, sionistes, terroristes" ou encore "sioniste, casse-toi,
la Palestine n’est pas à toi".

Le collectif, composé de plus d’une centaine de femmes, était accompagné
d’un service d’ordre d’hommes, certains masqués de noir avec des gants coqués.
Nous dénonçons la présence de ce service d’ordre exclusivement masculin.

Il convient également de dire qu’il ne s’agissait pas des fascistes de la LDJ
comme bon nombre de personnes et de responsables politiques de gauche se sont
pourtant empressé·es d’affirmer, mais du Service de protection de la communauté
juive (SPCJ), qui joue un rôle de service d’ordre et de protection communautaire
devant les synagogues et écoles juives.


Le collectif Nous vivrons n’est pas de gauche et notre conception du féminisme est très éloignée de la leur
==============================================================================================================

Le collectif Nous vivrons n’est pas de gauche et notre conception du féminisme
est très éloignée de la leur.
Mais ce n’est pas cependant un groupe d’extrême-droite (c’est sur ce mensonge
qu’a reposé son exclusion à Bordeaux). Il a pour objet de dénoncer les
violences sexuelles subies par les femmes israéliennes, faits absolument niés,
minimisés et relativisés dans une partie du mouvement social et féministe,
et que nous n’avons cessé de déplorer depuis notre appel adressé au
monde féministe de gauche.

Aussi, malgré les grandes divergences qui nous opposent à ce collectif et notre
condamnation de leur usage d’un service d’ordre exclusivement masculin, **nous
condamnons également et avec la plus grande fermeté le traitement qui lui a été réservé**.

En aucun cas des femmes, qu’elles soient Israéliennes ou d’autres
nationalités, ne peuvent être jugées comme fascistes ou terroristes du simple
fait de leur lieu de résidence, de leur nationalité, de leur religion, de leur
dénonciation des violences sexuelles comme arme de guerre.

L’assimilation de la dénonciation des violences à l’encontre des femmes
israéliennes à un soutien au massacre en cours à Gaza, est mensongère.

**Il est primordial de s’engager contre les deux**.


Ces incidents sont symptomatiques de l’abandon des femmes israéliennes victimes de violences sexuelles par une partie conséquente de la gauche
=================================================================================================================================================

Ces incidents sont symptomatiques de l’abandon des femmes israéliennes
victimes de violences sexuelles par une partie conséquente de la
gauche.

**L’attaque subie par le collectif, notamment par des membres d’Urgence
Palestine, est une honte**.

De nombreuses femmes juives de ce cortège se sont fait attaquer sans comprendre
pourquoi.
Les mots d’ordre de **Nous vivrons** avant la manifestation étaient ceux-ci :
"Nous devons accueillir cette place officielle qui nous est faite par un
respect scrupuleux de l’organisation qui nous est imposée. (…)

Zéro provocation.

Zéro réponse aux provocations.

Zéro propos hostiles aux organisatrices qui nous ont laissé une place.

Pas de ‘Israel Vivra/ vaincra ni de slogans en dehors de l’objet de notre
venue à savoir : **les femmes israéliennes victimes et les otages**"

Après l’attaque de leur cortège dans deux villes au minimum, les réactions
d’une partie du mouvement social, d’Antoine Léaument de La France
Insoumise, à Raphael Arnault de la Jeune Garde Antifasciste, ont continué
d’entraîner de la confusion et ont choqué nombre de Juifves de France.

Il y a bien des critiques légitimes a faire des silences, des positionnements
et actions du collectif **Nous vivrons** sur beaucoup de sujets, notamment leur
amalgame entre critique du sionisme et antisémitisme, mais la confusion
entre l’extrême droite et le collectif **Nous vivrons** est dangereuse.

En plus d’être factuellement fausse, elle alimente une haine déjà présente dans
le mouvement social envers les Israélien⋅nes et les Juifves.
**Il y a des critiques légitimes qui peuvent être faites aux sionismes, notamment
leurs conséquences concrètes sur les Palestinien·nes, mais le mot "sioniste"
n’est pas synonyme de "fasciste"**.

**Nous vivrons** n’est pas le "pendant sioniste" de Némésis, groupe
"féminin identitaire" que nous appelons à dégager de nos espaces de lutte
sans ambiguïté.

Le fait que de nombreuses femmes juives, y compris très
éloignées de leur positions, se soient reconnues dans la dénonciation par
**Nous vivrons** de l’insuffisance des réactions sur les violences sexuelles du
7 octobre ou des mécanismes de déni a l’œuvre, devrait a minima amener à
une remise en cause.

Nous le disons très clairement : votre confusion fait le lit de l’antisémitisme
et provoque des agressions réelles.

Prenez vos responsabilités, nous n’en pouvons plus.
=======================================================

**Prenez vos responsabilités, nous n’en pouvons plus**.

Nous saluons l’acceptation de sa présence par l’inter-orga de la marche
parisienne et appelons à nous réunir sur ce sujet avec les organisations
composant le mouvement social afin de lever les ambiguïtés entre une lutte
juste contre le fascisme et l’opposition au soutien aux femmes israéliennes
et juives victimes de violences sexuelles.

Nous sommes des militant·es antiracistes et avons nos places en tant que
personnes juives dans les cortèges antiracistes féministes.

Nous avons le droit de nous sentir en sécurité dans nos organisations politiques et
dans les manifestations qui nous concernent.

Nous refusons l’injonction qui nous est faite de montrer patte blanche sur
Israël-Palestine pour pouvoir y participer.

Nous refusons que des gauchistes blanc·hes s’octroient le droit
de traiter des femmes, des personnes queer et trans juives de "collabo"
ou diabolisent Israël comme le "seul Etat colonial" au lieu de lutter
aussi activement contre la FrançAfrique ou contre la politique française à
Mayotte qu’ils ne le font contre Israël.

Nous dénonçons ainsi la ligne adoptée par une partie des mouvements
féministes et queer qui, pour détourner l’attention de leur propre
colonialisme (celui de la France), font du colonialisme israélien l’enjeu
central des luttes pour l’émancipation, en n’hésitant pas à faire la
chasse aux "sionistes" réel·les ou imaginaires, et ce jusque dans nos
milieux militants.

Surtout lorsque l’on sait que les antisémites utilisent ce terme pour désigner
simplement les Juif·ves depuis sa popularisation par Soral et Dieudonné.

Nous refusons que des personnes racisé·es nous traitent de "blanc·hes"
quand beaucoup de nos parents et grand-parents viennent des mêmes pays que
les leurs.

La blanchisation des Juifves dans certains discours "antiracistes"
participe d’une même volonté de diaboliser les Juifves et de refuser de
prendre en compte la réalité prégnante et indéniable de l’antisémitisme
dans la société française et dans le monde en général.

Nous le disions à la veille de la manifestation féministe du 25 novembre 2023,
et le répétons aujourd’hui, au lendemain du 8 mars 2024, journée internationale
de lutte pour les droits des femmes : **ces violences nous traumatisent et nous
excluent en tant que personnes minorisées de genre, et Juifves**.


Les positions en faveur de la paix, contre la colonisation et contre le fascisme sont claires au sein de JJR
====================================================================================================================

Pourtant, les positions en faveur de la paix, contre la colonisation et contre
le fascisme sont claires au sein de JJR et ancrées dans le temps.

Ce que nous revendiquons, c’est une position féministe internationaliste qui,
sans nier les différences de rapports de force entre États, dénonce sans
chantage ni conditionnement toutes les violences sexistes, sexuelles et de
genre, qu’elles soient vécues par des Palestinien·nes, des Iranien·nes,
des Israélien·nes, des Ukrainien⋅nes, et de toutes les nationalités.

Les premières victimes des guerres sont les femmes et les enfants.

Nous luttons pour une solidarité entre toutes les personnes minorisées de genre, nous
luttons pour un monde où tous les viols et oppressions sexistes seraient
condamnées sans réserve, pas d’un monde où leur gravité doit être
"contextualisée", immédiatement remise en cause, niée, ou servir des
agendas politiques.

Nous croyons aux mouvements pour la paix et la justice, souvent portés par
des femmes qui refusent de voir leurs vies, leurs maisons et leurs familles
détruites pour nourrir les aspirations nationalistes ou ethnocentristes dont
les racines sont aussi patriarcales et capitalistes.


Notes
========

.. _[1]:

[1] Clara Zetkin, féministe socialiste allemande et Theresa Serber Malkiel
--------------------------------------------------------------------------------

Souvent créditée comme initiatrice de la journée internationale des femmes,
revendiquait pourtant elle-même l’héritage du modèle de la Journée nationale
des femmes conceptualisé par Theresa Serber Malkiel. https://www.jstor.org/stable/23881894
