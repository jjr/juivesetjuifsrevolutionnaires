.. index::
   ! Massacre antisémite de Toulouse : 5 ans après, on n’oublie pas ! (2024-03-19)

.. _jjr_2024_03_19:

=============================================================================================
2024-03-19 **Massacre antisémite de Toulouse : 5 ans après, on n’oublie pas !** par JJR
=============================================================================================

- https://juivesetjuifsrevolutionnaires.wordpress.com/2017/05/08/massacre-antisemite-de-toulouse-5-ans-apres-on-noublie-pas/
- :ref:`antisem:ozar_hatorah_2012`

Le 19 mars 2012, Mohamed Merah perpétue un massacre antisémite à l’école
Ozar Hatorah de Toulouse.

Il assassine à bout portant Jonathan Sandler, 30 ans, enseignant et rabbin,
ses deux enfants, Gabriel, 3 ans et Aryeh, 6 ans, Myriam Monsonégo, 8 ans.

Il blesse grièvement Aaron Bijaoui, agé de 15 ans et demi.

Pour la minorité nationale juive en France, ce massacre, 5 ans après l’assassinat
d’Ilan Halimi, est une nouvelle manifestation de la dimension génocidaire de
l’antisémitisme : c’est la première fois depuis la seconde guerre mondiale
que des enfants sont tués parce que juifs et juives, même si ce n’est pas la
première fois qu’une école juive est ciblée (on se souviendra notamment de
l’attentat perpétré contre l’école juive Nah’alat Moché de Villeurbanne,
le 7 septembre 1995, qui a fait 14 blessés).

**Ce massacre a joué un rôle important dans le processus qui a donné naissance
à notre collectif**, parce que comme lors de l’assassinat d’Ilan Halimi,
**les réactions qui ont majoritairement émergé dans notre camp idéologique
nous sont apparu comme n’étant absolument pas à la hauteur des enjeux**.

Ce massacre s’inscrit dans la montée en puissance des discours antisémites,
amplifiés par les médias sociaux, dont Dieudonné et Soral sont les
personnalités les plus visibles.

**Ce massacre n’a suscité que très peu de réactions populaires**, pas plus que
la virulence des discours antisémites : nombreux sont celles et ceux qui ont
minimisé, banalisé le massacre en en faisant un fait divers, comme ils et
elles ont minimisé la portées et l’existence des discours antisémites.

**Aucune manifestation de masse n’a alors eu lieu, contrairement à ce qui s’est
produit 3 ans plus tard**.

Cette absence de réaction est l’illustration de la construction du système de
domination de la "majorité nationale", et de la manière dont celle-ci
se dissocie des minorités nationales qui a amené y compris des secteurs
«progressistes ou révolutionnaires à ne pas voir dans les victimes de Toulouse
des "semblables" pour lesquels il était légitime de se mobiliser…

Les vieux procédés de défense raciste ont été mobilisés à plein pour nier,
minimiser l’antisémitisme : ceux qui consistent à isoler les actes des
discours qui les inspirent, à renverser les responsabilités (en renvoyant
la responsabilité de l’antisémitisme aux "sionistes" comme d’autres
renvoient la responsabilités de l’islamophobie aux "islamistes"), ou à en
minimiser la portée (en parlant d’autres victimes de meurtre ou de massacre,
de géopolitique, pour occulter la dimension raciste du crime).

Les discours officiels, quant à eux, se sont empressés d’éviter toute mise
en cause de toute responsabilité historique du nationalisme et du pouvoir
colonial français dans cette explosion de violence antisémite.

Pourtant, Mohamed Merah est le pur produit de l’antisémitisme colonial : celui
diffusé en Algérie par les colons français et Drumont, pour essayer de dévier
la révolte anticoloniale des algériens musulmans vers les juifs et juives en
les désignant comme "boucs émissaires".

**Mohamed Merah a été formé idéologiquement par cet antisémitisme colonial français**,
dont les avatars modernes sont tout autant Soral, Dieudonné, que les courants
réactionnaires qui s’inscrivent dans la lignée des théories d’un Sayyid Qutb
inspiré notamment par Alexis Carrel.

Cette rhétorique sur le prétendu "privilège juif" et le "complot juif
mondial", diffusée initialement par les suprémacistes blancs a pour principal
objectif de protéger, ainsi que le statut quo raciste et colonial, de toute
explosion de colère de la part des populations colonisées ou des minorités
issues de la colonisation.
Elle représente aussi pour les takfiristes une explication du monde conforme
à la théorie du "choc des civilisations" qu’ils partagent sur le fond avec
les suprémacistes blancs.

5 ans après, nous rendons hommage aux victimes.

Nous entendons aussi rappeler la nécessité pour les progressistes de prendre
au sérieux la lutte contre l’antisémitisme et à toutes et tous ceux qui seraient
tenter de voir dans l’antisémitisme un "vestige du passé" de constater qu’il
s’agit d’un fléau, qui, s’il n’a rien de nouveau dans sa nature, reste tout à fait actuel.

Nous réaffirmons qu’il nous appartient d’organiser notre autodéfense.

