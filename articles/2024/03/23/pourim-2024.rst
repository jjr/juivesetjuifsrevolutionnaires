.. index::
   ! Pourim 2024

.. _pourim_2024:

=========================================================================
2024-03-23 **Pourim 2024** Bonne fếte de Pourim 2024, Purim Sameah 5784
=========================================================================

- https://juivesetjuifsrevolutionnaires.wordpress.com/2024/03/23/pourim-2024/
- :ref:`pourim_2023_03_06`
- :ref:`judaisme:pourim`


Nous lèverons une armée d'Esther
==================================

.. figure:: images/nous_leverons_une_armee_d_esther.webp


Pour chaque nouvel Haman
============================

.. figure:: images/pour_chaque_nouvel_haman.webp


Nous lèverons une armée d'Esther
==================================

.. figure:: images/nous_leverons_une_armee_d_esther.webp

Ce mythe, tiré de l’histoire juive, nous rappelle qu’aujourd’hui encore,
l’antisémitisme existe, qu’il est là, qu’il opère, que nous ne devons ni nous
cacher ni nous mettre à genoux et que nous pouvons retrouver dans nos textes
la trace vivace de l’indispensable lutte.
