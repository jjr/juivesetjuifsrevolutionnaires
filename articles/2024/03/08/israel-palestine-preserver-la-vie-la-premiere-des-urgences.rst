

=========================================================================
2024-03-08
=========================================================================

- https://juivesetjuifsrevolutionnaires.wordpress.com/2024/03/08/israel-palestine-preserver-la-vie-la-premiere-des-urgences/

Depuis les tueries visant des civils et les violences sexuelles de masse
commises par le Hamas le 7 octobre 2023, nous assistons, impuissant·es, aux
crimes de guerre commis par Tsahal à l’encontre des Palestinien·nes et en
particulier celleux de Gaza. Poursuivant l’objectif insensé de détruire le
Hamas par l’action militaire et peut-être, à terme, d’occuper à nouveau
Gaza en déplaçant le maximum de Palestinien·nes, l’armée israélienne
perpètre un massacre de masse sur une population civile désormais acculée
à Rafah dans des conditions sanitaires absolument révoltantes.

Dès le 13 octobre 2023, nous dénoncions les "mesures criminelles et
inhumaines" du gouvernement Netanyahu et appelions à un cessez-le-feu
immédiat et à la reprise d’un processus de paix fondé sur la justice pour
toutes et tous en Israël/Palestine. C’est avec tristesse et colère, mais
néanmoins une détermination intacte, que nous réaffirmons cet impératif
aujourd’hui, alors que le drame dont nous sommes les témoins atteint une
ampleur vertigineuse. Aux victimes directes de l’armée et des bombardements
risquent bientôt de s’ajouter les mort·es de la famine, l’aide alimentaire
que l’armée israélienne laisse entrer à Gaza étant bien inférieure aux
besoins de la population.

La nécessité de préserver la vie est une urgence absolue. Elle impose la
constitution de mobilisations aussi larges que possible de l’ensemble des
partisan·es de la paix autour de revendications qui seraient à minima les
suivantes :

- Cessez-le-feu immédiat avec la fin des attaques contre des populations
  civiles, des bombardements, du blocus et des déplacements de population

- Libération de tous·tes les otages (israélien·nes et d’autres
  nationalités) détenu·es par le Hamas et les groupes qui ont pris part
  aux attaques du 7 octobre

- Libération des Palestinien·nes placé·es en détention arbitraire
  en Israël

- Acheminement de toute l’aide humanitaire nécessaire à la survie
  des populations gazaouies et à la reconstruction de la bande de Gaza

- Arrêt de la colonisation en Cisjordanie et retrait des territoires
  occupés

- Mise en place d’un processus de paix sur la base du droit international
  et des légitimes aspirations des Palestinien·nes et des Israélien·nes

Nous appelons donc nos lecteurs et lectrices à participer à toutes les
initiatives qui porteraient ces revendications.

Nous apportons notre soutien aux initiatives locales portées par des
militant·es israélien·nes et palestinien·nes pour exiger du gouvernement
israélien, du Hamas et de toutes les parties prenantes, l’aboutissement de
ces revendications, notamment à des groupes comme B’Tselem, Women Wage Peace,
ou Standing Together.
