.. index::
   ! Sur la loi immigration

.. _jjr_immigration_2024_01_05:

=========================================================================
2024-01-05 **Sur la loi immigration**
=========================================================================

- https://blogs.mediapart.fr/juives-et-juifs-revolutionnaires/blog/030124/sur-la-loi-immigration


.. tags:: Immigration, Solidarité

Mardi 19 décembre 2023, l'assemblée nationale a adopté la "Loi Immigration",
l’un des pires textes voté en matière de racisme et de xénophobie depuis le
début de la Ve République. 

C'est une honte et un grave signal d'alerte. 

Les arguments présentés par les personnes en faveur de cette loi nauséabonde
sont aisément contredits par les faits.

Pour elles, l'immigration devrait être conditionnée à la "création
de valeur" en France. Cette approche utilitariste de l'immigration est non
seulement détestable mais également fausse car elle part du postulat que
les sans-papiers ne produisent aucune "valeur". Une telle affirmation est
répugnante. Si l'on devait rentrer dans cette logique marchande, on constaterait
alors qu’en réalité les sans-papiers sont les personnes qui font marcher
l'économie de ce pays, l'immense majorité dans les métiers dits "en tension",
victimes systématiquement de conditions de travail inhumaines et de salaires
indignes. Ce sont ces personnes qui remplissent les rôles que beaucoup de
français·es n'accepteraient jamais, par privilège. Du reste, imputer aux
victimes de l'exploitation néo-coloniale française une forme de culpabilité
quant au fait qu'iels représenteraient un soi-disant "coût" trop important
pour un des pays les plus riches du monde, est tout simplement dégueulasse.

Un autre argument est la prise en charge des sans-papiers par la Sécurité
Sociale qui représenterait un "trou béant" dans les dépenses de
l'État. Rappelons certains chiffres en premier lieu. 8 sans-papiers sur
10 ne bénéficient pas de l'AME (Aide Médicale d'Etat). Le recours aux
aides sociales en France est un véritable parcours du combattant pour les
exilé·es. L'administration, désormais quasi entièrement numérisée,
est une institution inhumaine qui rend les démarches des sans-papiers d'une
difficulté difficilement imaginable, et laisse les associations d'aide aux
exilé·es surmenées. La barrière de la langue et la complexification voulue
par l'État des formulaires multiples de demandes d’aides forment un système
cynique dont le but est de pousser les personnes en besoin à abandonner leurs
démarches, par épuisement et par sentiment d'impasse.

Rappelons aussi que l'AME représente en réalité 0.4% des dépenses de la
Sécurité Sociale. L'argument du "coût" donc, en plus d'être contraire
aux droits humains les plus fondamentaux, est une manipulation mensongère et
meurtrière.Le droit au soin et à la dignité humaine sont inaliénables et
ne peuvent entrer dans des logiques de coût budgétaire.

La suppression de l'AME, mais aussi le durcissement conséquent des conditions
d'accès au "titre de séjour étrangers malades", ne représentent pas seulement
des dérives symboliques racistes, xénophobes et inhumaines du pouvoir en
place. Elles représentent surtout l'abandon de personnes aujourd'hui en France
ayant besoin de soins, étant malades, handicapées, qui du simple fait de leur
existence ont le droit de vivre en bonne santé et d'être prises en charge
par notre système social fondé sur l'égalité et la justice pour toustes.

Ce texte organise également le démantèlement du regroupement familial. Il
ne devrait pas être nécessaire de rappeler que toute personne a le droit
de vivre aux côtés de ses proches. Être séparé de sa famille ne devrait
jamais être acceptable. La rhétorique de la "submersion migratoire" est une
fable raciste qui voit dans le fait de rassembler des familles séparées
par la guerre, la pauvreté, la violence, un danger pour une dite "nation"
fantasmée, ou encore pour l'économie du pays. Non, des familles réunies
après de terribles périples empreints de souffrances, et bien souvent de
deuils, ne peuvent être conçues comme un danger.

La France et d'autres pays européens sous-traitent financièrement la
répression meurtrière des migrant·es à des pays tiers. Rappelons que l'UE
collabore avec les régimes algérien, libyen, et tunisien, pour ne citer
qu'eux, afin d'empêcher par des moyens brutaux et assassins la traversée en
Méditerranée des exilé·es. La vie de ces personnes est rendue impossible
dans ces pays par la répression et le harcèlement féroce de leurs forces
de police. Ces mêmes États traquent les embarcations de fortune partants
de leurs côtes en direction de l'Europe pour les arrêter et interner les
exilé·es dans des prisons au milieu du désert, sans eau, sans nourriture, et
soumis⋅es à des pratiques de torture, de violences sexuelles et de travail
forcé, d'après les rapports d'enquête de l'ONU en 2022 et de plusieurs
ONG depuis plusieurs années. L'Europe est grandement responsable depuis des
décennies de la mort de toutes ces personnes qui cherchaient simplement à
vivre. Les lois qui durcissent leur accueil ne font qu'accroître la misère.

L'exil est une nécessité, qu'il soit d'ordre économique, politique ou
climatique. Cela suppose la traversée de zones de conflits armés, de
pays racistes et brutaux qui les traquent, et de la mer Méditerranée qui
fait office de cimetière pour les personnes migrantes. Les instabilités
politiques et les réalités socioéconomiques ou climatiques des pays que
fuient les exilé·es sont bien souvent les conséquences des colonialismes et
néo-colonialismes français et européens. Le Mali, le Burkina Faso, la Côte
d'Ivoire, la Guinée, le Sénégal et bien d'autres pays faisaient partie de
l'empire colonial français, exploiteur des personnes et des ressources. La
France continue d'exploiter ce continent et travaille main dans la main avec
des dictateurs sanglants qui répriment leurs populations et les plongent dans
une misère sans nom. Ce pays a une responsabilité historique envers toutes
ces personnes et n'a absolument aucun droit de leur nier, encore et toujours,
le droit à une vie digne. Au delà de cette responsabilité, c'est le devoir
de l'humanité de préserver la vie et de permettre à toustes la paix.

Nous nous devons de rajouter que, si notre solidarité inconditionnelle
est d'abord un produit de notre humanité la plus élémentaire, ce à
quoi nous assistons fait également écho avec nos histoires familiales
juives, de descendant·es d'exilé·es, d'apatrides, d'étranger·es,
"d'indésirables". Lorsque nous entendons le vocabulaire utilisé par les
parlementaires racistes à l'Assemblée Nationale pour décrire les sans-papiers,
nous ne pouvons nous empêcher de voir nos mémoires ressurgir brutalement. Ces
mémoires renforcent notre détermination à nous placer aux côtés de toustes
les apatrides et les exilé·es.

Nos parents et nos grand-parents étaient pour la plupart des réfugié·es,
fuyant les persécutions, la guerre et/ou la pauvreté. Lorsque nous entendons
parler de "quotas d'immigration" nous ne pouvons nous empêcher d'avoir en
tête le nombre bien trop conséquent de réfugié·es juifves pendant la
première moitié du XXe siècle qui se virent refuser l'asile en France, aux
États-Unis, en Angleterre et dans la plupart des pays d'Europe, avec pour
justification des rhétoriques proches de celles employées aujourd'hui par
ce gouvernement et ses alliés politiques.

Déjà à cette époque, tous ces pays peignaient nos aïeux·les comme
des "parasites", des "dangers" pour le travail des "vrais nationaux", des
"menaces"... Les quotas d'immigration entre la fin du XIXe siècle et la Shoah
furent responsables de la mort par abandon de dizaines, voir de centaines de
milliers de juifves d'Europe de l'Est qui cherchaient à fuir une Europe qui
les pourchassait, les discriminait et les assassinait.

Pour les familles juives de pays arabes, elles ont en Algérie été déchues
de la nationalité française en 1940 sous Vichy et exclues de l'école
française, puis ont été contraintes de quitter leur pays. En Égypte,
le pouvoir les expulse du pays après 1956 et leur retire la nationalité,
les rendant apatrides. Quant aux familles du Maroc et de Tunisie, elles sont
souvent arrivées en France en même temps que les grandes vagues d’immigration
de population du Maghreb car la France avait besoin de main d’œuvre et ne
s’est pas privée d’aller les chercher.

En tant que Juifves, originaires d’Europe, d'Afrique du Nord ou du
Moyen-Orient, descendant·es de réfugié·es, nous ne pouvons que nous
identifier aux populations sans papiers et à leur stigmatisation par cette
loi. 

Nous leur apportons tout notre soutien et notre solidarité.

Aujourd'hui, alors que tant de personnes sans-papiers ici en France ne vivent pas
mais survivent, il est impératif de ne pas accepter ce projet de société. Un
projet de société au sein duquel il existerait une hiérarchie de valeur entre
les vies humaines. Aucune vie ne vaut plus qu'une autre. Et aucune personne
n'est illégitime à vivre où que ce soit. Ces principes, ce sont les principes
du droit humain, à l'égalité, à la dignité, à la justice, à la paix,
et à la vie. Ces principes restent et resteront toujours inaliénables.

À toutes les personnes sans-papiers, à toutes les personnes immigrées
et issues de l'immigration, à toutes les personnes apatrides, exilé·es,
réfugié·es, nous vous disons ceci :

Nous vous soutenons, et nous nous mobiliserons pour vous soutenir et vous
prêter main forte dans cette lutte.

Nous appelons tout le monde à se saisir de cet enjeu et à faire de même.
