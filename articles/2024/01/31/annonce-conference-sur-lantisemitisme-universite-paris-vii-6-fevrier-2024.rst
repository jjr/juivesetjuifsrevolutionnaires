

.. _jjr_2024_01_31_conference:

================================================================================================================
2024-01-31 **Annonce de la Conférence sur l’antisémitisme – Université Paris VII pour le 6 février 2024**
================================================================================================================

- https://juivesetjuifsrevolutionnaires.wordpress.com/2024/01/31/cqfd-lantisemitisme-cest-du-racisme/
- :ref:`jjr_2024_02_06_conference`

Annonce sur mastodon
=========================

- https://kolektiva.social/@jjr/111849748153452236
