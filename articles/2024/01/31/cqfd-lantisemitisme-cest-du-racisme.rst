

.. _jjr_2024_01_31_cqfd:

=========================================================================
2024-01-31 **CQFD – L’antisémitisme, c’est du racisme**
=========================================================================

- https://juivesetjuifsrevolutionnaires.wordpress.com/2024/01/31/cqfd-lantisemitisme-cest-du-racisme/
- https://cqfd-journal.org/L-antisemitisme-c-est-du-racisme

CQFD – L’antisémitisme, c’est du racisme
============================================

Nous avons été interviewé pour le numéro de janvier du mensuel CQFD.
N’hésitez pas à lire et à partager.

Lien `vers l'article de CQFD 'L-antisemitisme-c-est-du-racisme' <https://cqfd-journal.org/L-antisemitisme-c-est-du-racisme>`_

Sur mastodon
================

- https://kolektiva.social/@jjr/111850254370274429
