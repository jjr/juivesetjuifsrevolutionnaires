

.. _jjr_2024_01_15:

=========================================================================
2024-01-15 **Pour les otages et les gazaoui.e.s, l’enfer continue**
=========================================================================


- https://juivesetjuifsrevolutionnaires.wordpress.com/2024/01/15/pour-les-otages-et-les-gazaoui-e-s-lenfer-continue/


Cela fait maintenant plus de cent jours depuis l’attaque du 7 octobre 2023
et la prise d’otages de plus de deux cent civils par le Hamas.
Aujourd’hui, cent trente six d’entre elleux sont encore retenu.es dans
la bande de Gaza, au milieu des combats et des bombardements incessants.

Plus de cent jours que la vie de ces civils est mise en péril chaque minute,
en violation totale de la Convention de Genève et du droit international. Plus
de cent jours que ni le Hamas ni le gouvernement israélien ne semblent avoir
l’intention de préserver la vie de ces personnes innocentes.

Rappelons l’étau infernal dans lequel se trouvent ces otages, que leur
détention est absolument inacceptable et leur libération devrait être
réclamée et actée inconditionnellement. L’utilisation de civils quel
qu’iels soient comme monnaie d’échange entre des puissances politiques
n’a jamais été acceptable et ne le sera jamais.

**L’enfer qui s’abat sur la bande de Gaza depuis ces cent jours,  n’a jamais
visé à sauver les otages, mais à poursuivre un agenda politique violent
et meurtrier au profit des colons et des extrémistes religieux au pouvoir
en Israël.**

Le sort des otages devrait être la priorité du gouvernement israélien,
ce qui n’est visiblement pas le cas, tout comme préserver la vie des
Palestiniens ne semble pas être la priorité du Hamas.

Cette riposte militaire aveugle aux proportions injustifiables cause, chaque
jour qui passe, de plus en plus de victimes civiles gazaoui·es. Cette situation
est inacceptable, elle est également la preuve du désintérêt du gouvernement
Netanyahou pour le sort des otages.

Nous le répétons encore et toujours : soutien inconditionnel aux civils
qui, otages du Hamas ou gazaoui·es, sont toujours les premières victimes de
la guerre.
