

.. _jjr_2025_01_25:

===============================================================================================
2024-01-25 **Les chiffres sur la montée de l’antisémitisme confirment ce que nous vivons**
===============================================================================================


- https://juivesetjuifsrevolutionnaires.wordpress.com/2024/01/25/les-chiffres-sur-la-montee-de-lantisemitisme-confirment-ce-que-nous-vivons/

Les chiffres sur la montée de l’antisémitisme en France sortis ce matin
dans le journal « Le Parisien » sont glaçants.

Le chiffrage provenant des données du ministère de l’intérieur, il y
a fort à parier que nombreuses seront les voix qui s’élèveront pour
contester la portée de cette hausse.

Ce n’est qu’une manière de ne pas vouloir regarder les choses en face.

Car si les pourcentages peuvent paraître abstraits, ce qu’ils traduisent
c’est la réalité des actes et agressions antisémites depuis le 7 octobre 2023.

Nous expérimentons toutes et tous, juifves de France, à quel point cette
augmentation est palpable, par les témoignages de nos proches ou parce que
nous la vivons nous-mêmes, en ligne, à l’université, à l’école,
dans la rue, au travail…

**Nous apportons tout notre soutien aux personnes de notre minorité.**

**Notre colère est immense et nous serons toujours là pour lutter contre et
dénoncer l’antisémitisme, d’où qu’il vienne. Quoi qu’il en coûte**.

**À l’heure actuelle, c’est un acte de résistance que de se placer avec
les juifves du monde entier face au racisme qu’ils subissent**.

Ceux, groupes politiques, partis, collectifs, individus, qui minimisent ou
relativisent ces données et les vécus des Juifves sont a minima complices
de la crise que nous traversons. **Nous ne l’oublierons pas**.

Hazak. **Nous vivrons.**
