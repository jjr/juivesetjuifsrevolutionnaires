

.. _jjr_2024_04_07:

=========================================================================
2024-04-07 **Il y a trente ans : le génocide des Tutsis**
=========================================================================

- https://juivesetjuifsrevolutionnaires.wordpress.com/2024/04/07/il-y-a-trente-ans-le-genocide-des-tutsis/
- https://fr.wikipedia.org/wiki/G%C3%A9nocide_des_Tutsis_au_Rwanda

Le 7 avril est le jour international de commémoration du génocide des Tutsis du
Rwanda. C’est en effet il y a trente ans, jour pour jour, que les suprémacistes
hutus commencèrent l’extermination depuis longtemps planifiée de la population
tutsi. Entre huit cent mille et un million de personnes furent assassinées. Les
tueries ne prirent fin trois mois plus tard qu’avec la mise en déroute des
assassins par la rébellion du Front Patriotique Rwandais (FPR), dirigée par
Paul Kagame, qui est depuis à la tête du pays.

**La France joua un rôle clé dans le massacre**
=================================================

Les autorités françaises, sous les directives notamment de François Mitterrand
et d’Hubert Védrine, équipèrent et formèrent la gendarmerie et l’armée rwandaise
et leur envoyèrent du matériel, des conseillers stratégiques et des instructeurs
militaires (qui participèrent ponctuellement aux combats contre le FPR).

La France fut l’un des seuls pays à reconnaître le Gouvernement Intérimaire
Rwandais (GIR) génocidaire, dont le ministre des Affaires Étrangères fût reçu
à l’Élysée le 27 avril 1994, alors que les tueries avaient commencé vingt
jours auparavant.

Ce soutien continua donc pendant le génocide et culmina avec l’opération
Turquoise, durant laquelle deux mille cinq cents soldats français furent envoyés
officiellement pour "mettre fin aux massacres", en réalité pour permettre
aux assassins de se replier en toute sécurité. Certains d’entre eux ont fui en
France et trente ans après, la justice française tarde toujours à les juger. Le
20 décembre 2023, Sosthène Munyemana a été condamné à 23 ans de réclusion
criminelle, mais des dizaines d’entre eux sont toujours libres et vivent dans
l’impunité, comme Agathe Habyarimana, qui fut l’une des organisatrices du
génocide et dont le procès traîne depuis près de quinze ans sans qu’elle
n’ait jamais été mise en examen.

Un grand nombre de tueurs se réfugièrent, accompagnés de nombreux civils, dans
la région du Kivu, au Zaïre voisin, d’où ils tentèrent de reconstituer leurs
troupes. Les Forces Démocratiques de Libération du Rwanda (FDLR) qui combattent
aujourd’hui aux côtés de l’armée congolaise, sont directement issues de
ces groupes hutus ayant fui la victoire du FPR. Ils s’opposent notamment à la
rébellion du M23, soutenue par le Rwanda de Paul Kagame et qui affirme lutter
pour défendre les populations tutsies ou assimilées vivant en République
Démocratique du Congo (RDC). Le conflit qui meurtrit actuellement l’Est de la
RDC est donc en partie une conséquence de l’intervention française en 1994.

Si la France affirme aujourd’hui commémorer le génocide des Tutsis, **les discours
négationnistes ont toujours dans notre pays pignon sur rue**.

L’un de ses avatars le plus fréquent serait qu’il y aurait eu au Rwanda non
pas "un génocide", mais "des génocides", "des massacres", etc. ce qui permet
de renvoyer dos à dos génocideurs et génocidés (c’est la position que tiennent
Hubert Védrine ou Dominique de Villepin).

Un autre serait de faire comme si le conflit actuel était la seule conséquence
de l’expansionnisme d’un Rwanda agressif et assoiffé des ressources naturelles
que recèle le sol de la RDC **(ce qui semble être notamment le discours de
Jean-Luc Mélenchon)**.

Ces deux types de discours ont pour conséquence une dilution de la responsabilité
des génocidaires et de leurs acolytes français.

**Aujourd’hui, rendons hommage à toutes les victimes directes et indirectes du
suprémacisme hutu et de son allié, le colonialisme français**.
