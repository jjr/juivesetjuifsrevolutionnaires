.. index::
   ! L’instrumentalisation de l’instrumentalisation ou le piège de l’injonction à ne pas dénoncer l’antisémitisme

.. _jjr_2024_04_25:

=====================================================================================================================================================
2024-04-25 **L’instrumentalisation de l’instrumentalisation ou le piège de l’injonction à ne pas dénoncer l’antisémitisme** par JJR
=====================================================================================================================================================

- https://juivesetjuifsrevolutionnaires.wordpress.com/2024/04/25/linstrumentalisation-de-linstrumentalisation-ou-le-piege-de-linjonction-a-ne-pas-denoncer-lantisemitisme/


.. _instr_droite_2024_04_25:

Il y a l’instrumentalisation de l’antisémitisme par la droite et l’extrême-droite
======================================================================================

Il y a l’instrumentalisation de l’antisémitisme par la droite et l’extrême-droite,
que nous dénonçons depuis toujours, consistant à utiliser une prétendue lutte
contre l’antisémitisme à des fins racistes et particulièrement islamophobes
ou antipalestiniennes.
Dans le contexte actuel, cela se traduit par une criminalisation des actions
en soutien aux Palestinien·nes et un déni des crimes commis par l’État d’Israël,
au nom de la lutte contre l’antisémitisme.


.. _instr_gauche_2024_04_25:

Puis il y a ce qu’on peut appeler "l’instrumentalisation de l’instrumentalisation", en œuvre dans les milieux de gauche et d’extrême-gauche, dans nos propres rangs
=======================================================================================================================================================================

Puis il y a ce qu’on peut appeler "l’instrumentalisation de l’instrumentalisation",
en œuvre dans les milieux de gauche et d’extrême-gauche, dans nos propres rangs.

**Cela consiste à nier, minimiser ou banaliser toute dénonciation d’antisémitisme
sous le prétexte que celui-ci est instrumentalisé par la droite, et,
dans une  totale confusion, estimer que toute dénonciation de l’antisémitisme
en France est dirigée contre les Palestinien·nes**.

L’antisémitisme n’existerait plus en soi (mis à part chez quelques militants
d’ultra-droite ou à l’extrême droite) et ne serait donc qu’instrumentalisé
à des fins racistes et pour silencier le soutien aux Palestinien·nes.

**Cette rhétorique est dangereuse et elle a des conséquences concrètes**.

**Ainsi, pour devenir "légitime" à être entendu·e sur la cause palestinienne,
il faut (surtout si on est Juif·ve) minimiser ou relativiser l’antisémitisme
en France** : prétendre qu’il n’existerait plus ou qu’il se cantonnerait
à l’extrême-droite ou à quelques préjugés, ou ne serait que "la faute d’Israël".

C’est la rhétorique qu’on retrouve dans le livre Racismes de France paru en 2021,
qui prétendait pourtant à l’exhaustivité, et dans lequel on ne retrouve qu’un
indigent chapitre comparant l’antisémitisme aux préjugés sur les auvergnats.

**L’analyse selon laquelle les lieux de productions de l’antisémitisme (ne)
seraient (que) l’extrême-droite et Israël (et non d’abord la société au sens
large et l’État français à travers l’histoire) n’a rien de matérialiste**.

.. _instr_gauche_2_2024_04_25:

"c’est pas nous, c’est l’extrême droite"
================================================

C’est une analyse qui correspond en fait plutôt d’une part à un rapport
moral à la lutte contre l’antisémitisme ("c’est pas nous, c’est l’extrême droite")
qui serait donc résiduel, ou d’autre part à la théorie réactionnaire du choc
des civilisations,  – où le racisme qui vise une minorité ne serait que
réactionnel aux actions du pays qui lui est associé, à tort ou à raison.

L’antisémitisme serait ainsi devenu un faux-sujet.

Le mot même d’antisémitisme semble ne plus pouvoir être prononcé sauf pour dénoncer
son instrumentalisation par la droite et **pour affirmer continuellement que
l’antisionisme n’est pas de l’antisémitisme**.


Une oppression millénaire (qui se traduit aujourd’hui par un racisme systémique  évident) est alors réduite à un simple outil au service de stratégies politiques
====================================================================================================================================================================

Une oppression millénaire (qui se traduit aujourd’hui par un racisme systémique
évident) est alors réduite à un simple outil au service de stratégies politiques.

Le 3 mars 2024 avait lieu la conférence "Contre l’antisémitisme et son instrumentalisation,
pour la paix révolutionnaire en Palestine" co-organisée par Tsedek!, l’UJFP,
le NPA, l’AFA-PB, Révolution permanente et Paroles d’Honneur.

Sur les cinq heures de diffusion, **cinq minutes tout au plus (dont trois grâce au NPA)
ont été consacrées à la lutte contre l’antisémitisme, pourtant présentée en premier
dans l’intitulé**.

Universitaires féministes, militant·es pro-palestiniens, représentant·es des
principales formations d’extrême gauche et jusqu’à la France insoumise, **il est
normal pour tout ce monde de ne même plus faire attention au thème annoncé de la réunion**.

Toute tentative d’alerter sur des actes ou des propos antisémites, quels qu’ils
soient, par des groupes de gauche, produit immédiatement une disqualification,
une accusation d’être des "traîtres à la lutte pour la justice", d’être des
"sionistes de gauche".

Le terme "sioniste" ne désigne plus des positions politiques revendiquées mais devient une insulte
=====================================================================================================

Le terme "sioniste" ne désigne plus des positions politiques revendiquées mais
devient une insulte proférée envers toute personne (particulièrement juive !)
qui persiste à dénoncer l’antisémitisme, ce qui ferait le jeu de
l’instrumentalisation dénoncée.

Ce clivage conduit à perdre de vue qui sont les personnes ou les groupes
politiques derrière les étiquettes "sionistes" et "antisionistes" : **des alliances
se forment avec des antisionistes réactionnaires voire d’extrême-droite** tandis
que des groupes juifs de gauche  ou d’extrême gauche, impliqués dans le
mouvement social et ayant une base théorique socialiste, communiste ou libertaire,
sont silenciés, invectivés, harcelés et exclus, parce qu’il sont désignés
comme "sionistes" dès lors qu’ils soulèvent des problématiques d’antisémitisme.

Certains de nos camarades en ont fait les frais en manifestation, ou dans des
cadres unitaires. Nous ne nous attarderons pas à lister ces comportements et
insultes sur les réseaux sociaux car ils sont bien trop nombreux.

On retrouve là tous les arguments de la sionologie à l’œuvre sous Staline et
jusque dans les années 1980, qui a été une des formes macabres de l’antisémitisme
moderne.

Nous refusons de rentrer dans ce piège.
==========================================

Nous refusons de rentrer dans ce piège.

Nous continuerons à dénoncer toutes les oppressions racistes, antisémites et
réactionnaires, nous continuerons à dénoncer les instrumentalisations
de l’antisémitisme à des fins islamophobes, répressives ou antipalestiniennes
par la droite et l’extrême-droite, **et nous refuserons de répondre à l’injonction
géopolitique qui est faite à la minorité juive en France**.

Enfin, et par ailleurs, nous pensons toujours que continuer à lutter à part
entière contre l’antisémitisme comme contre tous les racismes reste la meilleure
façon de lutter contre "l’instrumentalisation de l’antisémitisme".

En effet, peut être que si la gauche n’était pas aussi poreuse à l’explosion
de l’antisémitisme ces dernières années et plus encore ces derniers mois; et
qu’elle prenait la question au sérieux **plutôt que d’être dans la dénégation
et la recherche permanente de tokens et d’alibis : il n’y aurait pas autant
de choses à "instrumentaliser"** .

Nous appelons donc à une honnêteté intellectuelle et à une véritable
convergence des luttes plutôt qu’à des tentatives de division car nous le
savons : nier une oppression ne rend pas plus lisible une autre oppression,
**abandonner la lutte d’une oppression ne rend pas plus efficace la lutte contre
d’autres oppressions, bien au contraire**.
