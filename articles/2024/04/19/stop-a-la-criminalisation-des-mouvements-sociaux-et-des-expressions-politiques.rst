.. index::
   ! Stop à la criminalisation des mouvements sociaux et des expressions politiques (2024-04-19)

.. _jjr_2024_04_19:

==================================================================================================
2024-04-19 **Stop à la criminalisation des mouvements sociaux et des expressions politiques**
==================================================================================================

- https://juivesetjuifsrevolutionnaires.wordpress.com/2024/04/19/stop-a-la-criminalisation-des-mouvements-sociaux-et-des-expressions-politiques/

Les interdictions de meetings, de manifestations, les dissolutions d’associations
et les condamnations de syndicats et syndicalistes sont inquiétantes pour nos
libertés publiques.

Il s’agit de choix politiques de la part du gouvernement : alors que le mouvement
social est attaqué, l’extrême droite, elle, prospère et se réunit toujours
plus nombreuse dans nos rues en bénéficiant de sa complaisance et de celle des médias.

Depuis les tueries perpétrées par le Hamas le 7 octobre 2023 et les crimes
commis par l’armée israélienne sur la population gazaouie en représailles,
une partie de la gauche française s’est mobilisée en faveur de la cause
palestinienne.
Cette mobilisation a pris diverses formes.
**Elle a pu être prétexte à l’expression d’antisémitisme, notamment du fait de
la cécité d’une partie du mouvement social vis-à-vis de celui-ci**.

Pour autant, la mobilisation pour un cessez le feu, en faveur de la paix et pour
la défense des droits du peuple palestinien est légitime et nécessaire.
A l’heure où des dizaines de milliers de Gazaoui·es meurent sous les bombes
de Tsahal, il n’est pas possible de rester indifférent.

**JJR réaffirme son opposition a la criminalisation du mouvement social et
du mouvement de solidarité avec les Palestiniens et Palestiniennes**.

Le libre débat démocratique est nécessaire, il peut passer par la critique
sévère qui est souvent nécessaire, mais il suppose la possibilité d’une confrontation
publique des idées : interdire les réunions d’un parti politique de gauche,
détourner les dispositions antiterroristes pour criminaliser des syndicalistes,
interdire une manifestation contre le racisme et les violences policières ne
sera jamais acceptable.

Bien pire, **cela risque de nourrir l’idée complotiste que le pouvoir politique
en France serait aux mains "des sionistes" et de nourrir la confusion qui
se répand dans notre champ politique**.

**La lutte pour que l’antisémitisme soit pris en compte au sein du mouvement
social doit être menée de l’intérieur de celui-ci et non sous pression policière.
C’est le sens du combat que mène JJR depuis bientôt dix ans**.

Ainsi, bien que très critiques des positions de certaines des personnes et
organisations attaquées, en particulier sur leur traitement de l’antisémitisme
que nous estimons insuffisant, **nous dénonçons leur criminalisation et la
politique hypocrite du gouvernement qui laisse libre cours à l’extrême-droite
en France et soutient la politique du gouvernement d’extrême droite israélien
à Gaza**.
