.. index::
   pair: JJR ; L’extrême-droite est et restera notre ennemie, combattons-la ! (2024-05-06)

.. _jjr_2024_05_06:

==================================================================================================
2024-05-04 **L’extrême-droite est et restera notre ennemie, combattons-la !** par JJR |jjr|
==================================================================================================

- https://juivesetjuifsrevolutionnaires.wordpress.com/2024/05/06/lextreme-droite-est-et-restera-notre-ennemie-combattons-la/


L’extrême-droite est et restera notre ennemie, combattons-la !
================================================================

C’est avec une grande exaspération que nous avons découvert il y a quelques
jours la tribune de Serge Klarsfeld (https://www.tribunejuive.info/2024/04/17/serge-klarsfeld-dans-un-monde-majoritairement-antisemite-les-juifs-ne-peuvent-compter-que-sur-eux-memes-pour-se-defendre/)
qui affirme (entre autres aberrations) qu’une des grandes victoires de la
lutte contre l’antisémitisme est "l’abandon de l’ADN de l’antisémitisme par
des partis issus de la droite extrême […] devenus partisans d’Israël et protecteurs
des Juifs contre l’islam radical."

Surnommé "le chasseur de nazis", Serge Klarsfeld est connu pour son travail
sur la mémoire de la Shoah et son militantisme pour la reconnaissance de la
responsabilité française dans la déportation des Juifves.
Il est le cofondateur avec son épouse Beate, de l’association des Fils et filles
de déportés juifs de France.

Ce n’est pas la première fois que cette personnalité affiche son rapprochement
avec le Rassemblement National. Il a accepté en octobre 2022 la médaille de
Perpignan du maire RN de la ville, Louis Alliot.
Nous le déplorions à ce moment dans un texte (https://juivesetjuifsrevolutionnaires.wordpress.com/2022/10/20/le-plan-suicidaire-des-klarsfeld/).

Il s’est également réjoui publiquement de la participation du RN à la marche
contre l’antisémitisme le 12 novembre 2023.

Alors qu’en 2021, ce militant de la mémoire écrivait une tribune contre le
racisme anti-Musulmans et contre la réhabilitation de Pétain par Éric Zemmour,
comment ne pas voir que Zemmour et le RN sont les deux faces d’une même pièce ?

Cela fait plusieurs années qu’une partie de l’extrême-droite française tente
de se défaire de son image antisémite et plus particulièrement depuis
le 7 octobre 2023 et l’explosion des actes antijuifs.


Nous sommes alarmé·es de cette tendance à ne plus voir l’extrême-droite comme une ennemie
---------------------------------------------------------------------------------------------

Nous sommes alarmé·es de cette tendance à ne plus voir l’extrême-droite
comme une ennemie.
Nous pouvons l’observer chez une partie de la minorité juive, comme dans une
partie de la population française générale.

Certaines personnes juives se tournent vers le RN ou Reconquête, notamment
parce qu’elles voient en ces partis des soutiens potentiels face au supposé
"nouvel antisémitisme".

Ce concept employé par tout le spectre de la droite ne vise qu’à justifier une
islamophobie d’État de plus en plus assumée.
Cette théorie raciste du "nouvel antisémitisme" se focalise sur l’origine
ethnique ou culturelle supposée des auteurs d’actes antisémites.

Elle empêche de penser le fonctionnement de l’antisémitisme qui imprègne la
société française et se traduit en injures, harcèlement et agressions.
A se focaliser sur les prénoms ou les origines des agresseurs (sur lesquels
aucune donnée sérieuse n’existe), on met de côté les ressorts du fonctionnement
de l’antisémitisme.

Or, celui-ci s’appuie notamment sur :

- des stéréotypes anciens hérités de l’antijudaïsme chrétien,
- une forme de racialisation qui date de l’Espagne du XVe siècle,
- l’antisémitisme moderne qui personnifie tous les maux du capitalisme et
   tous les changements sociaux dans la figure du Juif,
- l’antisionisme soviétique dit "sionologie", doctrine qui a connu des
  adeptes en France à l’extrême-droite, comme le négationniste François
  Duprat, ancien numéro 2 du FN.

Ces différentes facettes de l’antisémitisme se nourrissent les unes des autres,
et s’articulent variablement selon les contextes historiques et politiques.

Dans l’histoire contemporaine, l’extrême-droite en a été la principale productrice
idéologique.

Dans la période récente, c’est Alain Soral, aidé de Dieudonné, qui a réussi
à amalgamer ces différentes facettes, en se revendiquant du "national-socialisme"
et en véhiculant son négationnisme de la Shoah, par son obsession complotiste
d’un "lobby judéo-maçonnique" et du "sionisme mondial", en reprenant dans
l’antijudaïsme chrétien le thème de la traîtrise juive, et en ciblant exclusivement
le "capital nomade", celui du riche juif errant face au travailleur français
enraciné, thème de l’antisémitisme moderne.

Marine Le Pen ne tarissait pas d’éloges sur Soral, membre du FN jusqu’en
2009, alors même qu’il avait déjà été condamné pour incitation à la haine
antisémite en 2007 et 2008. Et même après que Soral a continué en solo, les
liens entre leurs entourages respectifs ont perduré.
Voir notre texte "Quand le FN était soralien" (https://juivesetjuifsrevolutionnaires.wordpress.com/2022/12/10/quand-le-fn-etait-soralien/)

Aujourd’hui, la stratégie politique de l’extrême-droite est évidente:

- répudier en vitrine ses éléments antisémites les plus radicaux,
- se placer en défenseuse des Juifves,
- instrumentaliser la lutte contre l’antisémitisme pour justifier son racisme
  et son islamophobie.

La désertion par une partie de la gauche des luttes contre l’antisémitisme
-------------------------------------------------------------------------------

Malheureusement, la désertion par une partie de la gauche des luttes contre
l’antisémitisme a participé à ce qu’advienne cette imposture.

**Le manque de réaction et de soutien face à la flambée d’antisémitisme qui a
suivi le 7 octobre 2023 peut être ajouté à la longue liste des abandons
ces 20 dernières années**.

L’entreprise de normalisation de l’extrême-droite en cours n’a qu’un seul
objectif : son accession au pouvoir.

L’antisémitisme (comme toute autre forme de racisme) de l’extrême-droite française
est profond et bien ancré.

Nous exhortons toutes les personnes juives à ne pas se laisser séduire par
ce "nouveau philosémitisme" de façade de l’extrême-droite.

Le Rassemblement National a fêté ses 50 ans il y a peu. 50 ans d’un parti
créé par des anciens Waffen-SS et anciens membres de l’OAS sur le lit
d’Ordre Nouveau.
Depuis 50 ans ce parti n’a jamais renié ses racines antisémites et peine à
masquer son antisémitisme présent : sa collusion avec des antisémites notoires
dont des membres du GUD a été rappelée par plusieurs enquêtes ces dernières années
et par la sortie d’un livre en novembre 2023 (https://arenes.fr/livre/les-rapaces/ )
relatant l’antisémitisme et le racisme de la municipalité vitrine du RN à Fréjus.

L’antisémitisme structure sa vision du monde, et ce quand bien même il aurait aujourd’hui l’habileté de le masquer
----------------------------------------------------------------------------------------------------------------------

Rappelons que la théorie raciste du "grand remplacement", qui nourrit l’idéologie
du RN et Reconquête, postule originellement que ce sont les juif·ves qui sont
à la manœuvre de ce complot contre les populations blanches européennes.

Ainsi, Marine Le Pen reprend les ressorts complotistes de cette théorie quand
elle parle du "mondialisme financier" et "culturel" qui imposerait une immigration
massive.

De même, lorsqu’elle parle de Macron, elle appuie systématiquement sur son passé
à la banque Rothschild.

Ces thèmes n’ont pas changé depuis la fin du XIXe siècle.

Leur vision du monde structurellement complotiste et suprémaciste ne peut que
mener vers l’antisémitisme.

L’électorat du RN ne s’y trompe pas, c’est lui qui compte les scores les plus
élevés de tout le champ politique quant aux préjugés antisémites.

À l’approche des élections européennes, il faut aussi marteler que le RN est
dans le groupe d’extrême-droite Identité et démocratie au parlement européen
avec l’AfD allemande, dont les liens serrés avec les néonazis ne sont plus
à prouver.

Les alliances politiques de Marine Le Pen trahissent en effet la vacuité de sa
stratégie de dédiabolisation, parmi lesquelles celle avec le premier ministre
hongrois Viktor Orbán.
Ce dernier, au-delà d’avoir entrepris une vaste entreprise de négation de
l’histoire de la Shoah – notamment en réhabilitant le "Pétain Hongrois" (Miklós Horthy)
responsable des déportations de masse des Juifs de Hongrie -, déploie une
politique antisémite autour de la figure de George Soros, philanthrope américain
d’origine juive hongroise.

Érigé en bouc émissaire, Soros constitue pour lui – ainsi que pour les extrêmes
droites européennes et américaines – une figure récurrente du Juif riche
utilisant son pouvoir financier pour influencer le cours du monde.

En 2018, le gouvernement hongrois promulguait la loi "Stop Soros" qui pénalisait
les ONG aidant les migrants, loi portée par une rhétorique qui fantasmait
un prétendu "plan Soros" visant à "islamiser l’Europe" en finançant l’immigration
extra-européenne.

Selon eux, Soros manipulerait également l’opposition au gouvernement en place,
alors même qu’il n’est pas engagé politiquement en Hongrie.

Tout cela ne gêne manifestement pas le RN.

En décembre dernier, Jordan Bardella, tête de liste RN aux élections européennes,
était à Florence auprès des extrêmes droites en vue des futures élections,
où était réaffirmée la théorie raciste du "grand remplacement", George Soros
étant à nouveau ciblé.

Rappelons enfin que les Juifves en France (1ère communauté juive en Europe)
ne comptent qu’environ 500 000 personnes sur 68 millions, et que leur prêter
des pouvoirs surdimensionnés (poids dans les élections et influences cachées)
est un trope antisémite ancien.

Plus qu’hier et moins que demain, ne baissons jamais la garde face à
l’extrême-droite qui reste ce qu’elle a toujours été : antisémite et
raciste !

Notes
=========

(1) Serge Klarsfeld. “Dans un monde majoritairement antisémite, les Juifs ne peuvent compter que sur eux-mêmes pour se défendre”
------------------------------------------------------------------------------------------------------------------------------------

- https://www.tribunejuive.info/2024/04/17/serge-klarsfeld-dans-un-monde-majoritairement-antisemite-les-juifs-ne-peuvent-compter-que-sur-eux-memes-pour-se-defendre/

(2) Le plan suicidaire des Klarsfeld
----------------------------------------

- https://juivesetjuifsrevolutionnaires.wordpress.com/2022/10/20/le-plan-suicidaire-des-klarsfeld/

(3) Quand le FN était soralien
------------------------------------

- https://juivesetjuifsrevolutionnaires.wordpress.com/2022/12/10/quand-le-fn-etait-soralien/
