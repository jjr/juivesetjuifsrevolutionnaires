

.. _jjr_202_05_02:

=====================================================================================================
2024-05-02 **23 mai 2024 : Réunion publique : Pour une gauche qui lutte contre l’antisémitisme**
=====================================================================================================

- https://juivesetjuifsrevolutionnaires.wordpress.com/2024/05/02/23-mai-2024-reunion-publique-pour-une-gauche-qui-lutte-contre-lantisemitisme/
- :ref:`raar_2024:public_raar_2024_05_23`

Face à l’augmentation massive des actes antisémites partout dans le monde
depuis le 7 octobre 2023, et alors que les crimes de guerre de l’armée israélienne
se multiplient, menaçant l’existence des Palestinien·ne·s de Gaza,
nos collectifs ( Golem , JJR , RAAR ) souhaitent engager un débat dans et
avec l’ensemble du camp progressiste.

Nous avons besoin de parler et d’échanger afin d’échapper aux pièges qui
empoisonnent les débats actuels.

Dans nos espaces politiques, nous constatons une indifférence toujours plus
grande face à l’explosion des actes anti-juifs , comme si l’appropriation
malhonnête de la lutte contre l’antisémitisme par les forces réactionnaires
disqualifiait cette question pourtant bien réelle.

Nous sommes aussi inquiet·e·s de la stigmatisation systématique de groupes
juifs exprimant une condamnation des crimes du Hamas , qui se voient dénigrés
comme “sionistes”, terme proclamé comme une injure, voire comme “fascistes”.

À cela s’ajoute une montée généralisée du complotisme et du confusionnisme ,
qui ne restent malheureusement pas cantonnés à la fachosphère et déboussolent
aussi le camp progressiste.

Concernant la droite et l’extrême-droite, nous condamnons la manière dont
elles utilisent la lutte contre l’antisémitisme à des fins islamophobes ou
pour attaquer les mouvements progressistes.

Tout cela a été rendu possible en raison de l’abandon par une partie de la
gauche de la lutte contre l’antisémitisme.

Cette situation nécessite des prises de positions claires dans lesquelles
on n’oublie rien et ne sacrifie rien de nos exigences progressistes.

Il s’agit d’une véritable “ligne de crête” :

- ne rien lâcher, ni sur la lutte contre l’antisémitisme, ni sur le combat
  contre tous les racismes ;
- lutter contre la tendance à nier les exactions du 7 octobre 2023, d’un côté,
  manifester l’horreur que nous inspire ce que le peuple palestinien
  endure en ce moment et depuis trop longtemps, de l’autre ;
- garder intacte la mémoire des victimes du Hamas d’un côté, dénoncer la
  banalisation du carnage quotidien que subissent les Palestinien·ne·s, de l’autre ;
- réclamer la libération des otages, d’un côté, réclamer un cessez-le feu
  immédiat et total à Gaza, de l’autre .

Nous appelons tou·te·s les camarades, ami·e·s et organisations progressistes
à venir discuter de ces questions avec nous à Paris à la Bourse du Travail
(Salle Hénaff), le 23 mai 2024, à 19h.

Venez en débattre avec nous !

Rejoignez-nous sur cette ligne de crête !

Ne laissons pas le terrain de la lutte contre l’antisémitisme à la droite
et l’extrême-droite ! Venez nombreux·ses !

Inscription à l’adresse suivante: lagauchecontrelantisemitisme@gmail.com
