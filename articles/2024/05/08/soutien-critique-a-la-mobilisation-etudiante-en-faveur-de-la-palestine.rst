.. index::
   pair: JJR ; Soutien critique à la mobilisation étudiante en faveur de la Palestine (2024-05-08)

.. _jjr_2024_05_08:

===================================================================================================================================================
2024-05-08 **Soutien critique à la mobilisation étudiante en faveur de la Palestine** par JJR ||jjr|
===================================================================================================================================================

- https://juivesetjuifsrevolutionnaires.wordpress.com/2024/05/08/soutien-critique-a-la-mobilisation-etudiante-en-faveur-de-la-palestine/


Soutien critique à la mobilisation étudiante en faveur de la Palestine
========================================================================

Depuis plusieurs semaines, de nombreuses universités se mobilisent en soutien
au peuple palestinien, notamment en France et aux États-Unis.
Étant donné la situation dramatique à Gaza et le soutien apporté par les pays
occidentaux au gouvernement israélien, de telles actions sont légitimes.
Nous apportons tout notre soutien aux étudiant·es face à la répression policière : les universités
sont un lieu historique des mobilisations sociales, y compris internationalistes,
et doivent le rester.

Cependant, ces mobilisations ont pu donner lieu à d’importantes tensions,
voir dans certains cas à des comportements et propos antisémites.
Face à cette situation, certain·es étudiant·es juif·ves déjà traumatisé·es
par les évènements du 7 octobre 2023, ayant de la famille en Israël, et/ou ayant
été pris·es à partie ou craignant de l’être cessent d’aller en cours,
envisagent des ré-orientations ou choisissent de cacher leur judéité.

**Cela n’est pas acceptable**.


Le soutien à la cause palestinienne ne doit pas être prétexte à une expression antisémite
-------------------------------------------------------------------------------------------

Nous appelons donc celles et ceux qui se mobilisent à la vigilance sur ce
point.
Le soutien à la cause palestinienne ne doit pas être prétexte à une expression
antisémite.

La multiplication de discours fantasmant une domination "sioniste" en France
est à cet égard préoccupante (le chroniqueur du Média Kamil Abderrahman a fait
plusieurs tweets en ce sens, comme le 3 janvier ou il écrit "On vit dans un
pays totalement soumis au lobby sioniste et aux injonctions du CRIF" ).

Si le terme "juif" n’est pas utilisé, il s’agit bien ici d’antisémitisme dans
la plus pure lignée des Protocoles des Sages de Sion.

De même, la négation des crimes du Hamas, le relai ou le soutien apporté
à cette organisation ne sont pas acceptables (le 30 avril, le collectif Urgence
Palestine a ainsi relayé un communiqué des "Organisations étudiantes de la
bande de Gaza en solidarité avec l’intifada étudiante aux États-Unis"
**dont l’organisation islamiste est le premier signataire**).

Enfin, le soutien envers le peuple palestinien ne doit pas être prétexte à
la diffusion **d’un discours campiste favorable ou relai de la propagande des
régimes criminels en Iran, en Russie ou en Syrie** (la participation de figures
du milieu militant antisioniste à des évènements communs avec Michel Collon
ou Jacques Baud, que nous avons déjà dénoncée, est ici inquiétante).

Une proposition pour échapper à ces écueils serait d’ajouter aux revendications
étudiantes la libération des otages du Hamas et le soutien à celles et ceux
qui se battent contre les dictatures d’Assad, de Poutine et de Khamenei.

**Dans la mesure ou ces points sont pris en compte, nous invitons nos lecteurs
et lectrices à participer au mouvement en cours et à faire pression sur nos
gouvernements pour que cesse le soutien au gouvernement israélien et à ses
actes criminels à Gaza et en Cisjordanie**.
