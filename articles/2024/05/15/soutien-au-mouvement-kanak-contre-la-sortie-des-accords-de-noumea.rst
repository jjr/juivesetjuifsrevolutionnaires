.. index::
   ! Soutien au mouvement kanak ! Contre la sortie des accords de Nouméa ! (2024-05-15)

.. _jjr_2024_05_15:

===================================================================================================================================================
2024-05-15 **Soutien au mouvement kanak ! Contre la sortie des accords de Nouméa !**
===================================================================================================================================================

- https://juivesetjuifsrevolutionnaires.wordpress.com/2024/05/15/soutien-au-mouvement-kanak-contre-la-sortie-des-accords-de-noumea/


Soutien au mouvement kanak ! Contre la sortie des accords de Nouméa !
========================================================================

Un important mouvement social secoue actuellement le Kanaky / Nouvelle Calédonie.

Il fait suite à un projet de loi visant à l’élargissement du corps électoral,
gelé depuis les accords de Nouméa de 1998.

Alors que ces derniers avaient pour objectif une décolonisation progressive
du territoire, la politique du gouvernement français vise aujourd’hui la mise
en minorité du peuple autochtone kanak, c’est-à-dire la recolonisation.

Cette orientation était déjà visible lors du troisième référendum sur
l’indépendance du 12 décembre 2021, qui avait été largement boycotté
(https://juivesetjuifsrevolutionnaires.wordpress.com/2022/01/06/suite-au-referendum-en-nouvelle-caledonie/).

**La loi de 2010 visant à la correction des inégalités économiques et professionnelles
entre Kanaks et non-Kanaks, pourrait également être menacée**.

**Quatre personnes sont déjà décédées dans les affrontements.
Nous apportons toutes nos condoléances à leurs familles et à leurs proches**.

La paix doit revenir, elle ne le pourra que quand les justes revendications
de la population autochtone kanak seront prises en compte.

Nous appelons donc à la mobilisation pour faire reculer le gouvernement.


Liens
=======

- :ref:`raar_2024:msg_raar_2024_05_20`
- :ref:`jjr_2022_01_06`
