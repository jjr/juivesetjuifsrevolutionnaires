.. index::
   pair: Ilan Halimi ; N’oublions pas Ilan Halimi (2024-02-11)


.. _jjr_2024_02_11:

=========================================================================
2024-02-11 **N’oublions pas Ilan Halimi**
=========================================================================

- https://juivesetjuifsrevolutionnaires.wordpress.com/2024/02/11/noublions-pas-ilan-halimi/

Cher Ilan,

Il y a dix-huit ans, le 23 janvier 2006, tu as été capturé par un groupe
d’une vingtaine de personnes dirigé par Youssouf Fofana et se faisant
appeler le "gang des barbares".

Ta famille, la famille Halimi, est contactée par les ravisseurs : ils exigent
450 000€ en échange de ta vie.

Ta famille ne pouvant payer un tel montant, les tortionnaires contactent un
rabbin au hasard dans l’annuaire et lui intiment l’ordre de "réunir
la somme dans sa communauté". Si la famille d’un juif (forcément riche)
ne peut pas payer, sa communauté paiera. Voilà le raisonnement antisémite
à l’œuvre.

Pendant vingt-quatre jours, Ilan Halimi, tu as vécu un terrible calvaire,
humilié et torturé par tes ravisseurs. Le 13 février 2006, tu es retrouvé
agonisant sur les rails du RER C. Ton corps porte des marques de torture et
tu succombes à ces trop nombreuses blessures.

Tu aurais pu être notre cousin, notre frère, et en un sens, tu l’es. Le
supplice de ta mort à profondément choqué notre communauté. Ce meurtre
antisémite nous a tous·tes touché·es dans notre chair. C’est, comme
chaque année, le coeur lourd que nous te rendons hommage aujourd’hui.

Face à ce crime, deux attitudes politiques lourdes de conséquences se sont
développées et ont conduit à un recul des luttes antiracistes :

D’un coté une minimisation, un déni ou même une justification
de la violence antisémite renvoyée à une "importation du conflit
israélo-palestinien" en France.

Cet antisémitisme est expliqué par un "effet boomerang" du sionisme, sans
voir qu’il ne s’agit jamais que d’un prétexte à la haine raciale allant
jusqu’à des assassinats racistes.

Ces parallèles font écho à l’actualité, à l’heure où l’antisémitisme explose
et ou le mot "sioniste" remplace "juif" lors des attaques antisémites.

De l’autre, une instrumentalisation de la nécessaire dénonciation
de l’antisémitisme, pour diffuser un discours raciste faisant des
"arabo-musulmans" les vecteurs d’un "nouvel antisémitisme" et
minimiser ainsi la profondeur de l’ancrage historique de l’antisémitisme
dans la société française.

Si des racismes intracommunautaires existent évidemment, l’idéologie
qui anime les tueurs d’Ilan Halimi prend sa source dans les stéréotypes
antisémites les plus anciens. Celui associant les Juifves et l’argent date du
Moyen Âge européen et chrétien. Le fait que ces stéréotypes se diffusent
dans tous les secteurs de la société française, y compris ses minorités,
irriguées notamment par des porte-parole tels que Soral ou Dieudonné, est
la preuve qu’ils sont partie intégrante de l’idéologie dominante et non
d’une tendance antisémite qui serait spécifique à ces minorités.

Ilan Halimi, nous ne t’oublions pas : ton supplice nous hante et nous ne
pardonnons pas.
