

.. _jjr_2024_02_04:

=========================================================================
2024-02-04 **Agression antisémite à l’université de Strasbourg**
=========================================================================

- https://juivesetjuifsrevolutionnaires.wordpress.com/2024/02/04/agression-antisemite-a-luniversite-de-strasbourg/

Nous apprenons avec effroi et colère l’agression antisémite dont ont été
victimes trois étudiant·es juifves à Strasbourg dans la nuit de dimanche à
lundi alors qu’ielles collaient des affiches pour demander la libération des
otages retenus par le Hamas ainsi que la phrase "NON À L’ANTISÉMITISME".

Cette agression intervient dans un contexte particulièrement inquiétant de
libération de la violence antisémite, qui n’épargne pas les universités.

Nous souhaitons apporter notre soutien aux étudiant·es agressé·es ainsi
qu’à l’ensemble des étudiant·es juifves qui sont harcelé·es, se
trouvent obligé·es de dissimuler leur judéité ou d’abandonner leurs
études par peur d’être pris·es à partie ou agressé·es.

Nous appelons l’ensemble des organisations de notre camp politique,
syndicats étudiants, engagés dans la lutte antiraciste à se positionner
fermement contre l’antisémitisme, et à apporter publiquement leur soutien
aux étudiant·es juifves.
