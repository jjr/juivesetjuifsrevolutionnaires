.. index::
   pair: Que sais-je ? ; L'antisémitisme (avec JJR, Juifves révolutionnaires)
   ! Que sais-je ? L'antisémitisme (avec JJR, Juifves révolutionnaires)
   pair: Déclaration de Jérusalem sur l’antisémitisme; DJA

.. _jjr_2024_02_25:

=======================================================================================================
2024-02-25 **Que sais-je ? L'antisémitisme (avec JJR, Juifves révolutionnaires)** |martin|
=======================================================================================================

- https://youtu.be/tYWRnqNL_dk?t=23
- https://www.youtube.com/@MartinEdenNews/videos
- :ref:`antisem:martin_eden`
- :ref:`antisem:sender`
- :ref:`antisem:lea_jjr`

#antisémitisme #shoah #ProcheOrient #soral #Mélenchon
#soral #Dieudonne #Guiraud #quesaisje?

.. figure:: images/affiche.webp



Sites des JJR
================

- :ref:`jjr`

Podcasts de la chaîne Martin Eden sur la même thématique
============================================================

- https://www.youtube.com/playlist?list=PLkVAm1rOEsU3bbgH759I0XadwvIBEieIv (5 mai 2023  Mon parcours #soral #dieudonné #psyhodelik #rochedy #faurisson #conversano)
- https://www.youtube.com/playlist?list=PLkVAm1rOEsU0JwRs6wnDkkoajgkv0zuGz (Critique du "philosémitisme d'état" d'Houria Bouteldja, 13 août 2023
  #homophobie #danyetraz #houria #houria #wissam #PIR #danyetraz #antisémitisme #homophobie )
- https://www.youtube.com/playlist?list=PLkVAm1rOEsU0erTKabtfJWbDKxoyCi1h- (Histoire de l'antisémitisme à gauche (avec Michel Dreyfus), 9 mars 2023)
- https://www.youtube.com/watch?v=ne2UGJZMYpA (La gauche et l'antisémitisme dans les années 2010 (avec Memphis Krickeberg), 20 nov. 2023  )
- https://www.youtube.com/watch?v=kFJhHTAxDWo (10 sept. 2023  Alain Soral : une synthèse critique (avec Frédéric Balmont) #soral #dieudonné #faurisson #antisémitisme #complot
- https://www.youtube.com/watch?v=0BuZhW3Xu3g (20 nov. 2023  #dieudonné #antisémitisme #PIR #houriabouteldja #PIR #wissam #antisémitisme #soral #dieudonné #antimoderne)

Sources cités dans l'entretien
===================================

- https://leplus.nouvelobs.com/contribution/317700-le-pen-et-les-neo-nazis-autrichiens-le-bal-de-la-honte-et-la-tentation-du-fascisme.html
- https://www.bfmtv.com/politique/front-national/prison-ferme-librairie-negationniste-qui-est-frederic-boccaletti-nouveau-depute-rn-du-var_AV-202206210034.html
- https://melenchon.fr/2018/09/29/carnet-de-voyage/ ( "En général quand une campagne électorale voit un homme de gauche être traité d’antisémite c’est qu’il n’est pas loin du pouvoir.")
- https://contre-attaque.net/2022/07/18/macron-est-il-petainiste/
- https://www.lefigaro.fr/vox/histoire/maurice-barres-le-heraut-de-l-union-sacree-20230813
- https://www.lemonde.fr/politique/article/2020/02/12/devant-les-deputes-lrm-macron-invoque-maurras-pour-parler-du-regalien_6029292_823448.html
- https://www.lefigaro.fr/politique/le-scan/2018/11/07/25001-20181107ARTFIG00121-macron-petain-a-ete-un-grand-soldat-pendant-la-premiere-guerre-mondiale.php
- https://www.nouvelobs.com/politique/20210323.OBS41777/gerald-darmanin-accuse-d-antisemitisme-dans-son-livre-sur-le-separatisme-islamiste.html
- https://www.nouvelobs.com/politique/20211029.OBS50455/accuse-d-antisemitisme-apres-avoir-assimile-zemmour-et-traditions-juives-melenchon-s-explique.html
- https://www.conspiracywatch.info/gerard-filoche-relaie-un-photomontage-complotiste-antisemite-lefface-puis-denonce-une-cabale.html
- https://www.conspiracywatch.info/le-point-sur-la-tres-douteuse-video-anti-valls-publiee-par-jean-luc-melenchon.html

Pour aller plus loin
========================

- :ref:`antisem:arte_video_antisem_1_4`
- :ref:`antisem:arte_video_antisem_2_4`
- :ref:`antisem:arte_video_antisem_3_4`
- :ref:`antisem:arte_video_antisem_4_4`
- https://sortirducapitalisme.fr/emissions/la-destruction-des-juifs-d-europe-1933-1945/
- https://sortirducapitalisme.fr/emissions/aux-racines-de-l-extermination-des-juifs-d-europe/
- https://sortirducapitalisme.fr/emissions/repenser-l-antisemitisme-pour-mieux-le-combattre/
- https://sortirducapitalisme.fr/emissions/aux-racines-historiques-de-l-ideologie-nazie/


**Déclaration de Jérusalem sur l’antisémitisme** (DJA)
==========================================================

- :ref:`antisem:declaration_jerusalem`


|martin| Martin Eden introduction
====================================

Bonjour à tous et à toutes,

Bienvenue sur Martin Eden podcast |martin|
et aujourd'hui un un format un peu que sais-je un peu important sur un
sujet très léger qui est **l'antisémitisme** et pour parler de cette question,
pas du tout polémique, et ben j'ai invité des gens pas du tout polémiques
que sont :ref:`juives et juifs révolutionnaires <jjr>` (JJR, |jjr|)

bonjour à vous !

Bonjour

Alors est-ce que vous voulez vous préser enfin présenter un peu sommairement
qui vous êtes, qui vous représentez, et cetera et cetera voilà je pense
parce que c'est une ça sera un bon commencement.

Présentation de 2 membres des JJR Léa et Sender |jjr|
===================================================================

Léa se présente
-----------------------

- https://youtu.be/tYWRnqNL_dk?t=32

Alors moi je m'appelle :ref:`Léa <antisem:lea_jjr>`, je suis membre des Juives et Juifs
Révolutionnaire donc nous sommes un collectif non mixte de juifves
qui avons ressenti le besoin qu'il y ait une voix de juifves d'extrême gauche
pour faire court.

Martin Eden
 d'accord

Sender se présente
---------------------

Moi je suis :ref:`Sender <antisem:sender>` et je fais partie du même collectif
et je voilà

**Alors question c'est quoi l'antisémitisme ?**
================================================

- https://youtu.be/tYWRnqNL_dk?t=60

Martin Eden
 Alors question : c'est quoi l'antisémitisme ?


.. _lea_antisemitisme_2024_02_25:

2024-02-25 Léa : **définition de l'antisémitisme au sens large**
---------------------------------------------------------------------

Alors ben justement on va essayer d'y répondre donc **qu'est-ce que l'antisémitisme
au sens large ?**

Donc c'est un racisme qui a des traits communs avec tous les autres racismes
donc **c'est l'essentialisation en fait d'un groupe de personnes** à une
identité fantasmée donc après l'antisémitisme c'est un racisme qui a ses spécificités
comme chaque racisme a ses spécificités.

Donc souvent les racismes en fait c'est un fantasme d'infériorité d'un groupe
de personnes et on va dire que la particularité de l'antisémitisme c'est
que **ce fantasme il est entre guillemets "inversé"** donc ce serait
**un fantasme en fait de puissance**.

L'antisémitisme aussi ce sont pas seulement des préjugés, dont certains
sont millénaires, c'est aussi une explication du monde, une vision de la société
dans laquelle il y aurait une élite qui comploterait dans l'ombre,
contre la masse, donc dans certains récits c'est une élite financière,
un milliardaire, un banquier juif ; dans d'autres c'est une élite médiatique
donc un patron de presse Juif par exemple.

Ca peut être aussi une élite intellectuelle qui est en cause et c'est des récits
en fait qu'on retrouve dans tout l'échiquier politique et c'est en fait
ces explications manichénnes du monde qui sont à la base malheureusement de
la pyramide de violences antisémites qui vont on va dire des brimades aux
agressions et jusqu'aux meurtres.

Martin Eden
 d'accord
 voilà pour non vas-y pardon non mais juste justement vas-y vas-y je rebondirai
 après vas-y lance-toi


.. _sander_antisem_2024_02_25:

Sender **sur les différentes formes de l'antisémitisme**
------------------------------------------------------------

- https://youtu.be/tYWRnqNL_dk?t=167

Et ben non, c'est que du coup oui si il faut aussi moi je vais
peut-être appuyer un peu sur l'aspect historique de la question de
l'antisémitisme au niveau du fait que **il puisse se manifester à travers
des formes différentes**.

Donc qu'est-ce qu'on appelle l'antisémitisme ? en fait comme c'était
dit auparavant c'est quelque chose de général et de global qui englobe et
donc **ces différentes formes d'antisémitisme, il faut quand même savoir les
reconnaître** et il faut savoir un petit peu d'où elles viennent et pourquoi
est-ce qu'il en y a de différentes d'ailleurs c'est une bonne question aussi.

On pourra on peut parler en fait on entend souvent dire voilà il y aurait
il y a **l'antijudaïsme chrétien du Moyen-Âge** en Europe qui évolue au fur
et à mesure du temps mais voilà il y a une période pendant l'Inquisition
et puis après ça on revient à une autre forme de haine des Juifs
qui est **l'antisémitisme moderne au 19e** donc s'il y a une chose
quand même à comprendre par rapport à à ces différentes manifestations
d'antisémitisme c'est qu'elles sont liées par quelque chose, c'est qu'il y
a un point commun entre ces exemples et **ce point commun c'est ce
qu'on appelle l'antisémitisme**

C'est **l'essence même de l'idéologie qui consiste à mettre les Juifs au
centre du monde** dans une espèce de rhétorique complotiste de comme disait
Léa de **fantasme inversé**, d'accoler une puissance énorme aux Juifs complètement
déconnectée de la réalité matérielle ; juste des situations des Juifs
et de la diaspora juive au fur et à mesure de l'histoire.

si c'est important peut-être de préciser ça c'est pour dire que en fait
ce qui va changer principalement dans
l'antisémitisme **c'est des questions de lexique**, des **questions d'imaginaires qui
sont mobilisés** et en fait ça va, pardon j'ai perdu un mot sur le bout
de la langue, ça va induire en quelque sorte la différence qui en ressort
aussi dans leur application matérielle

pourquoi est-ce que **l'antisémitisme nazi** ne ressemble pas aux croisades
pourtant c'est deux formes d'antisémitisme mais dont les contextes en fait
sociohistoriques sont tellement différents
ils ont produit des résultats malheureusement différents voilà
moi c'était juste pour appuyer enfin je sais pas c'est un point que je voulais
appuyer qu'on parle d'antisémitisme c'est bien de faire comprendre que quand on
dit l'antisémitisme:

- ça prend en compte en fait l'antisémitisme moderne
- ça prend en compte l'antijudaïsme chrétien
- ça prend en compte l'antijudaïme nonchrétien
- ça prend en compte l'antisémitisme nazi qui lui-même est un une spécificité
  enfin un antisémitisme spécifique parmi l'antisémitisme moderne ;
- pas tous les antisémites modernes sont nazis

donc il y a quand même une une conception, une une analyse on va dire à avoir
sur ce sujet-là qui qui va plus loin que ce qu'on a aujourd'hui et voilà

un peu brutal comme

non c'est

Martin Eden : l'antisémitisme de manière synthétique c'est: **"le juif a trop"**
---------------------------------------------------------------------------------------

- https://youtu.be/tYWRnqNL_dk?t=350

Non c'est pas tellement brutal c'est qu'à un moment donné c'est pour
reprendre un petit peu je enfin ça rejoint ce que vous dites ça rejoint
beaucoup la définition de Delphine Horvilleur sur la question antisémite c'est
justement l'antisémitisme de manière synthétique c'est **"le juif a trop."**

Voilà c'est ça je pense que c'est une bonne formule c'est comme je le
répète souvent par exemple aux États-Unis pendant la ségrégation raciale
on a bizarrement on n'a pas théorisé comme quoi il a la catégorie de
l'humanité qu'on a vu dans le champ de coton on avait pas bizarrement on n'a
pas théorisé le fait qu'il contrôlait le monde par exemple.

Effectivement donc donc oui c'est on reproche aux Juifs d'avoir trop
trop de pouvoir, trop d'influence aussi trop contestataire aussi on on l'a déjà vu enfin
c'est être à la fois la être à la fois dans les dans les deux extrémités
de la révolution parce que le Juif c'est aussi le le banquier mais c'est
aussi le révolutionnaire Marx aussi enfin voilà moment donné c'est ça aussi
un petit peu le mais même si je crois que cette thématique est repris dans
une blague juive aussi que j'avais prise avec Michel Dreyfus

mais mais voilà je
c'est ce qui en fait un truc assez pervers aussi c'est toute c'est
c'est tout un imaginaire et puis le truc dure depuis à peu près de un peu
plus de 2000 ans si je dis pas de bêtises donc il a eu le temps d'avoir de s'être
développé quoi

Léa
-----

- https://youtu.be/tYWRnqNL_dk?t=428

C'est ça et il s'est mué en fait dans l'histoire comme
disait Sender historiquement c'était un antijudaïsme donc c'était vraiment
religieux par rapport à à la religion et puis ensuite c'est devenu plus un
antisémitisme racial donc on a vu les juifs comme un peuple
voilà

Martin
 Il y a eu aussi la variante scientifique aussi dans le 19e siècle avec
 justement Darwin

Léa
 Tout à fait

Sender
-------

c'est l'antisémitisme, c'est l'antisémitisme moderne
en fait à la base c'est plutôt ça c'est vraiment en fait voilà si on veut
caricaturer un petit peu en France en Angleterre par exemple c'est des très bons
exemples c'est une époque où d'ailleurs beaucoup des gens qui arrivent en fait
dans les ligues antisémites, les premières ligues antisémites fin du
19e siècle ; c'est les gens qui en fait par exemple comme Drumont viennent de la
France catholique antisémite aussi mais anti-juive et compagnie enfin des il y a
des liens pour eux entre les deux parce que c'est les mouvements nationalistes
l'Action française et compagnie c'est le nationalisme met d'accord les gens

et à un moment donné en fait quand il y a le truc scientifique voilà à
la suite de ; n entend beaucoup en plus parler par exemple `d'Ernest Renan <https://fr.wikipedia.org/wiki/Ernest_Renan#Id%C3%A9es_et_th%C3%A8ses>`_


.. note:: L'antisémitisme de Renan a des origines intellectuelles complexes,
 tout en procédant d'une manière à la fois systématique, hiérarchique
 et "fixiste".

 C'est à la suite de ces propos que l’orientaliste juif autrichien
 Moritz Steinschneider écrit un texte dans lequel il critique Renan
 pour ses "préjugés antisémites", forgeant ainsi l’adjectif "antisémite".


mais voilà Renan comme Gobineau c'était des gens qui ont théorisé en fait les
races aryennes, sémites à partir de concepts foireux puisque ça vient pas là
ça vient de la linguistique; ça existe pas les races sémites merci au revoir

- https://youtu.be/tYWRnqNL_dk?t=527

et en fait y a un moment donné où en fait ça a été voilà réalisé pile
dans la période c'est pour ça je dis il y a toujours un contexte dans lequel
l'antisémitisme se manifeste et qui en fait teinte sur à quoi ressemble
l'antisémitisme, quels sont les arguments avancés et quand l'époque c'est
le darwinisme social, l'eugénisme, l'anthropométrie et compagnie

et bien on racialise, on a racialisé le juif mais en même temps c'est là où on dit
en fait que le Juif soit catégorisé comme un sémite au sang juif de la race
de je sais pas quoi ou alors un adepte de la religion en réalité on leur
reproche des choses qui sont pas fondamentalement éloignées

elles ont leurs
spécificités, il y a des manières différentes d'application comme on disait
l'antijudaïsme n'avait pas les mêmes manifestations physiques peut-être
que l'antisémitisme mais par contre il a une forme d'essence qui est la
même qui lie les deux

c'est pour ça que c'est pas deux phénomènes qui sont complètement séparés


Martin Eden
----------------

- https://youtu.be/tYWRnqNL_dk?t=580

effectivement et tu as anticipé le fait ah enfin le le
combien de fois je l'ai entendu ce truc là pour c'est enfin s' extraire d'un
débat ah oui il y a ; non mais les Juifs moi aussi je suis sémite donc
ta gueule c'est insupportable c'est ces tours
de passe-passe linguistique et aussi je voudrais un peu rajouter quelque chose par
rapport au plan parce que tu tu avais parlé de processus de racisation et
je crois aussi c'est un angle sacrément mort qu'on a dans nos milieux

Est-ce que les Juifs sont des blancs comme les autres ?
------------------------------------------------------------

est-ce que les Juifs sont des blancs comme les autres ?

Léa
-------

non non les Juifs ne sont pas des blancs


Martin
 vous avez 2 heures


- https://youtu.be/tYWRnqNL_dk?t=635

non mais notre réponse notre réponse elle est claire enfin bon déjà les Juifs
qu'est-ce que ça veut dire quand on est il y en a qui viennent d'Afrique du Nord
du Moyen-Orient ; il y en a qui vient d'Europe enfin voilà c'est

on a des Juifs partout dans le monde en fait donc déjà enfin il y a des Juifs
qui sont noirs donc déjà de dire qu-ce que les Juifs sont des blancs ben non
enfin voilà il il y en a plein qui sont pas du tout blancs je veux dire
même d'aspect et ensuite enin les Juifs ont tellement été traités comme
une une minorité ils ont tellement été marginalisés, persécuté sallant
jusqu'au génocide que non on peut pas dire on peut pas dire qu'ils sont blancs

impossible

Sender
---------

- https://youtu.be/tYWRnqNL_dk?t=662

ouais enfin je suis assez d'accord avec ce ce je suis assez d'accord avec
ce que Léa vient de dire là je rajouterai même qu'en fait enfin je vois
pas l'intérêt non plus je comprends pas pourquoi les juifs auraient besoin
d'être blancs ou pas blancs en fait

enfin c'est là c'est pour ça qu'on parle d'antisémitisme en fait spécifiquement le
juif il est ; enfin moi j'ai envie de dire peut-être quelque chose qui qui
je sais pas en fait pourquoi est-ce qu'il a besoin d'être blanc ou pas blanc
alors qu'en fait il vit des des discriminations parce qu'il est juif il rentre
peut-être pas dans ce cadre là il a peut-être besoin de qu'on comprenne les
discriminations dont il fait l'objet dans leur spécificité


et tu vois pour
faire un un un micro point par rapport à ça lors qu'on parle souvent de la
Shoah il y a quelque chose à comprendre aussi dans on va dire `l'antisémitisme
nazi <https://fr.wikipedia.org/wiki/Origines_de_l%27antis%C3%A9mitisme_nazi>`_
une variante dans l'antisémitisme moderne c'est que l'antisémitisme nazi
c'est un peu une version paroxistique de l'antisémitisme moderne dans le sens
où il amène l'antisémitisme moderne à un tel niveau qui n'est en fait

Drumont n'est pas nazi et n'était pas nazi bon il est mort avant mais
mais surtout que son idéologie n'est pas le nazisme

pareil pour Maurras
faut comprendre aussi ce que c'est que le nazisme le nazisme c'est des gens
qui étaient persuadés que les Juifs étaient responsables de la chute de
l'Empire romain, des la chute des Grecs enfin ils ont développé
un antagoniste à l'humanité qu'ils ont personnifié en les Juifs
qui a dépassé un peu l'entendement de ce qu'on pouvait voir à l'époque
qui était déjà un peu con et en fait la Shoah est une volonté totale et
radicale d'exterminer l'entièreté des gens


ça me fait de la peine
un peu de citer mais c'est un bon exemple pour prendre un exemple bien nazi
et bien français de chez nous `Brasillach <https://fr.wikipedia.org/wiki/Robert_Brasillach>`_ c'est quand même quelqu'un qui
dit pendant les campagnes allemandes à l'est "et surtout n'oubliez pas les
enfants" donc c'est il y a une dimension en fait qui est celle de c'est
c'est non seulement les Juifs ont trop mais en fait les Juifs sont un ça peut
aller jusqu'à **les Juifs sont un danger existentiel**

ils ne peuvent pas exister si nous on veut exister

parce qu'on doit se libérer de leur domination parce
qu'ils propagent des maladies incurables parce que, parce que, parce que,

mais et ça c'est une spécificité à comprendre qui personnellement selon moi je
vois pas en quoi discuter de si les Juifs sont blancs ou pas blancs ça aide
à lutter contre ce phénomène là ; manque de dire non effectivement les
Juifs ne sont pas les les blancs dans le sens des dominants en fait

oui c'est ça oui pas des

ouais


Martin Eden
-----------------

c'est ça en fait la question était un peu parce
que je connais assez bien la rhétorique qui mène enfin à ce que comment dire
à faire passer le groupe social juif comme étant des dominants c'est
effectivement c'est un peu le jeu enfin c'est le tour de passe-passe rhétorique
le fait de leur prêter les mêmes comment dire le même la même agressivité
le même impérialisme que l'État d'Israël donc je enfin je pense que vous
connaissez assez bien la rhétorique aussi

je pense modestement

et donc c'est effectivement ce faire passer une un groupe social qui est dominé qui est
opprimé parce que les actes antisémites spécifiquement en France ils ont
explosé et non ce n'est pas uniquement dû au 7 octobre c'était là déjà
c'était une dynamique qui est déjà là avant bien avant on va dire et Soral
a bien participé à cette pratique donc c'était par rapport à ça c'est
effectivement c'est trop théorique que dire qu'effectivement que oui voilà
comme dirait De Gaulle le Juif, voilà le peuple d'élite dominateur et sûr de lui-même
quoi enfin voilà c'est enfin je crois c'était pas dans cet ordre là mais
voilà c'est finalement collé aux Juifs l'idée même du Juif enfin toute la
diaspora du monde entier serait comme par magie aurait les traits de la enfin
de voilà de voilà de enfin voilà de enfin de la politique colonisatrice
d'Israël en fait

**alors qu'en soit c'est une dénégation pure et simple des individualités**

c'est comme dire je sais pas moi que tous les musulmans
sont responsables de Daesh tu vois enfin à un moment donné en terme d'abstraction
et de malhonnêteté théorique c'est pour moi c'est carrément équivalent
quoi

Sender
------------

- https://youtu.be/tYWRnqNL_dk?t=905

ouais c'est pas pas totalement éloigné après aussi moi j'ai enfin ça peut
peut-être nous amener à à un prochain point qui est justement en fait il
y a pas que ; enfin il y a on va parler vite fait de la droite, de l'extrême
droite qui vont fantasmer une espèce de nouvelle forme d'antisémitisme en fait
y a on va parler de ça ; mais quand tu disais sur la continuité c'est par
que ça aussi dans le sens où en fait il y a quand même pas mal de gens
aujourd'hui qui sont des antisémites comme on pouvait en voir à l'époque

pas changés de grand-chose quoi qui ont pas forcément trouvé des nouvelles
rhétoriques enfin qui sont pas en fait sur le cette espèce de terrain que
les gens vont appeler "nouvel antisémitisme" qui est probable enfin qui est en
fait tout simplement un terme foireux pardon je c'est ...

.. _lea_2024_02_25:

Léa : le "nouvel antisémitisme" est juste un fantasme de l'extrême droite
------------------------------------------------------------------------------

- https://youtu.be/tYWRnqNL_dk?t=966

c'est ça en fait en fait oui ça ça nous amène à un point qu'on voulait
développer qui est celui du "nouvel antisémitisme" entre guillemets donc qui
est pour nous en fait **juste un fantasme de l'extrême droite**

donc pour revenir un peu sur ce que v'est; c'est une espèce de fausse idée selon
laquelle ben la communauté arabo mususulmane serait vecteur d'un "nouvel
antisémitisme" donc en vrai c'est voilà c'est l'extrême droite qui
a pour but de diffuser un discours raciste et de minimiser la profondeur de
l'ancrage historique de l'antisémitisme dans la société française
c'est un argument qui a été beaucoup évoqué au moment de l'affaire :ref:`Ilan Halimi <antisem:ilan_halimi_2006>`

Le meurtre d'Ilan Halimi
---------------------------

Donc pour rappel c'est :ref:`Ilan Halimi <antisem:ilan_halimi_2006>` c'est
un jeune Juif qui a été kidnappé en 2006 torturé puis tué en fait, mort
des suites de ses blessures parce que Juif, et en réalité l'idéologie qui
anime les tueurs d':ref:`Ilan Halimi <antisem:ilan_halimi_2006>`
ben elle prend sa source dans les stéréotypes antisémites les plus anciens

donc on voit que; on voit qu'en fait parce que pour ce pour ce jeune en fait
le bourreaux avaient demandé une rançon ... à sa famille donc
c'est en fait ils ont cru parce qu'il était Juif qu'il était riche donc
l'association lu fait de l'argent c'est un stéréotype qui date du Moyen-Âge
en Europe en fait ; donc après le fait que ce stéréotype il se diffuse dans
tous les secteurs de la société française et y compris dans les minorités

C'est la preuve juste qu'ils sont parties intégrantes en fait de l'idéologie
dominante et non d'une tendance antisémite qui serait spécifique à ces
minorités donc on voit que on voit que l'antisémitisme en fait il a ni
couleur ni religion ni parti politique ; on le retrouve hélas dans toutes les
classes socio-culturelles de la société française mais avec toujours les mêmes
:term:`antisem:poncifs`.

donc le Juif est l'argent, le Juif est le pouvoir, voilà le Juif complote

Martin Eden
    les Juifs qui passent les Juifs qui passent sur cette chaîne d'ailleurs
    aussi

Sender
----------

tout le monde, c'est un gâteau qu'on partage, on est beaucoup mais oui c'est
et en fait moi aussi rajouter à ça par rapport aux gens qui parlent de
"nouvel antisémitisme" c'est que enfin pour qu'il y ait un nouvel antisémitisme enfin
faudrait qu'il y en ait un ancien ; je vois pas très bien en quoi l'antisémitisme
d'aujourd'hui ; une partie en tout cas serait ancienne puisqu'elle est tout à
fait contemporaine

l'idée comme quoi l'antisémitisme se serait arrêté après 45 à coups de baguette magique
c'est un peu tiré par les cheveux en fait ; quand même faut rappeler quelque
chose c'est à dire que en fait la France
elle a collaboré pendant la guerre on va faire micro point mais en fait la
France a collaboré pendant la guerre ; l'épuration en France c'était pas ;
on n'a pas éradiqué le concept d'antisémitisme il y a des gens qui ont
été jugés il y a des choses qui ont été faites et cetera la libération
l'épuration tout ça

mais il y a des fonctionnaires qui sont restés en place
il y a des gens qui sont restés dans des instances en fait donc il y a
un moment donné en fait ; déjà parler du principe que l'antisémitisme aurait
disparu c'est faux et en fait même les antisémites eux-mêmes ils ont pas
disparu et ils ont à vivre dans ce pays ; parce qu'ils étaient pas
nombreux c'est ça le problème aussi et en fait après 45 est-ce qu'on
peut dire que il y a plus d'antisémitisme ?

non on peut dire que il y a un tabou
peut-être tel de la Shoah qu'on voit s'effriter un peu aujourd'hui de plus
en plus malheureusement, qui fait que c'est assez compliqué en fait d'être
ouvertement antisémite comme on pouvait l'être au bon vieux temps

et du coup il y a il y a plein de phénomènes qui se sont passés des
librairies de sous-main moi je pense notamment par exemple si on dit la gauche
machin là je pense à la Vieille Taupe mais je pense aussi y a mais je pense
aussi même avant la Vieille Taupe en fait je pense à ; regarder l'Action française ça
a pas été ça ils ont continué leurs affaires ; Rivarol ça existe quoi

ben le FN on en parle tellement le FN qui à la base c'est Occident, Ordre
nouveau, des groupuscules qui sont littéralement néonazis avec des nazis
dedans qui font une espèce d'amicale d'anciens nazis qui du coup s'appelle
le FN ; Jean-Marie Le Pen éditait des disques de chants nazis du 3è Reich
enfin tout ça c'était tout à fait les bons vents de l'extrême
droite française peut-être juste que une rupture un peu Marine Le Pen
qui a pas forcément changé le fond de fond de cave de son idéologie et de
et de ses militants

c'est juste que elle essaie de pas en faire trop
de bruit parce que c'est calcul politique ça  je veux dire depuis 45
l'extrême droite a toujours été autant antisémite point ;

Le "nouvel antisémitisme" juste une manière de stigmatiser les gens
-------------------------------------------------------------------------

donc parler de "nouvel antisémitisme" c'est exactement pardon je reprends ce
que tu disais Léa mais **juste une manière de stigmatiser les gens** en fait
mais ça:

- déjà de un c'est raciste ;
- deux ça ne lutte pas réellement contre l'antisémitisme

Martin Eden
---------------

- https://youtu.be/tYWRnqNL_dk?t=1272

c'est vrai que comme dirait Bernanos, Hitler a déshonoré l'antisémitisme

Sender
    c'est ah ouais yes

c'est même si bernanos c'est enfin sous le soleil de Satan
voilà bon je vais pas faire une description du bonhomme mais mais sur ça
je suis assez d'accord effectivement il y a eu une espèce de tabou autour
de ça après 45 mais comment  ah mais ça ça c'est

Sender
----------

civitas le mec cash qui dit non mais la Révolution française c'est
la faute des juifs depuis la Révolution française il y a trop de migrants
c'est à cause des Juifs c'est ça c'est vieux comme le monde c'est

Martin Eden
--------------

ouis ouais ou
c'est surtout le point aussi quand je dis qu'on est raciste d'un point de vue
global on est rarement raciste que sur un groupe social en général c'est
un package c'est ça aussi que les gens ont peut-être un peu du mal et
c'est ce que implique un petit peu le nouvel enfin ce que sous-entendrait
le "nouvel antisémitisme" qui ait comme vous l'avez dit il y aurait un ancien

effectivement vos explications sont sont très bonnes mais c'est surtout en
fait d'un point de vue concret le enfin voilà en plus le nouvel antisémitisme
je crois que ça a été théorisé par `Pierre André Taguief <https://fr.wikipedia.org/wiki/Pierre-Andr%C3%A9_Taguieff>`_ si je dis pas
de bêtises ; bon ok Taguief m'a aidé en termes de voilà parce qu'il a
écrit beaucoup de livres sur la  théorie du complot et cetera je crache
pas dessus il m'a été, il m'a aidé pendant une période


Ce "nouvel antisémitisme" non c'est une technique pour taper sur les musulmans tout simplement
----------------------------------------------------------------------------------------------------

mais sur ce point du "nouvel antisémitisme" non c'est une technique pour
taper sur les musulmans tout simplement exactement

..

sous-entendre que le nouvel antisémitisme serait incarné par les musulmans
non non je pense que enfin comme dirait `Orelsan <https://fr.wikipedia.org/wiki/Orelsan>`_
tu n'es pas français si tu pas un oncle raciste et qu'en général quand
tu es raciste c'est rarement sur un groupe social spécifique, en général c'est
vraiment c'est je vais pas parler d'intersectionnalité du racisme mais on va
dire c'est vraiment si tu es un salaud avec avec un avec les ronois (noirs, NDLR) pourquoi tu
le serais par pourquoi tu serais gentil envers les rebeu (beurs, NDLR)  tu vois on pourrait
partir de là ça serait un peu bizarre tu vois en général quand tu es
raciste t'es raciste envers tout le monde c'est que ça soit les pauvres d'ailleurs
les minimas sociaux les rebeus, les renois et cetera et cetera
je vais pas faire la liste ici

mais effectivement ce le "nouvel antisémitisme" ça serait
une espèce de oui une espèce de truc un peu opportuniste un peu, clairement
raciste pour dire regardez les musulmans c'est eux les vrais antisémites même
d'ailleurs le

Sender
    ça blanchit le RN


Léa
-----

- https://youtu.be/tYWRnqNL_dk?t=1426

oui on on l'a vu aussi récemment enfin dans l'actualité récente ; en fait il y a eu une manifestation
contre l'antisémitisme en novembre (2023) avec donc tous les partis politiques
sauf ceux d'extrême gauche et notamment avec les partis d'extrême droite
donc ça a été vraiment un scandale il y a RN et reconquête donc qui ont
essayé de s'approprier la lutte contre l'antisémitisme et pour nous ça a
été insupportable en fait en tant que Juifs ; c'est vraiment un crachat à
la figure de notre histoire quand on sait que l'extrême droite actuelle
c'est l'héritière des nazis et des collabos

donc donc voilà c'est quand même un parti qui a été fondé par d'ancien
nazis ; dirigé pendant des décennies par un antisémite notoire c'est Jean-Marie Le Pen qui a compté
quand même Alain Soral parmi ses cadres dirigeants

**c'est un parti qui a jamais été l'allié des Juifs et qui le saura jamais**

donc nous on se leurre pas quant à l'agenda politique raciste de l'extrême droite ; il se veut les
défenseurs des Juifs mais le but comme comme tu viens de le dire **c'est
vraiment de marginaliser la minorité musulmane mais en instrumentalisant nos
souffrances et ça c'est particulièrement insupportable en fait**

Sender
---------

en continuant aussi d'être parfaitement antisémite dans leur coin parce
que c'est pas

Léa
    exactement

on parle aussi souvent de la normalisation de Marine Le Pen elle a
dit que l'antisémitisme c'était pas bien **trois fois dans sa vie vous vous
rendez compte**

et à un moment donné déjà en fait il y a pas si longtemps
alors là pour le coup j'ai peut-être plus la date mais c'était en 2021
quelque chose comme ça, Marine Le Pen qui va dans un bal autrichien nazi
enfin elle a cotoyé des nazis quand elle était enfant mais de toute façon
en fait c'est peut-être pas pour ça que je disais ça c'était pas pour Marine
Le Pen en réalité moi je m'en tape de Marine Le Pen qu'elle soit antisémite ou
pas c'est pas important il y a trop d'autres trucs pour qu'elle soit acceptable

mais par contre son héritage politique et sa base électorale ne l'est peut-être
pas entièrement mais c'est pas une idéologie qui est combattue
à l'interne du parti je pense quand par exemple c'est cinepox?? pour commencer
un petit peu **la longue liste des raisons pour laquelle le RN est parfaitement
antisémite** je veux dire je suis désolé c'est un parti qui a sur ses sièges
à l'Assemblée nationale un ancien libraire négationniste ; qui vendait Mein Kampf sous
le manteau

- https://youtu.be/tYWRnqNL_dk?t=1553

le mec est député exemple


Martin Eden
    c'est lequel député

c'est bocatelli Frédéric bocatelli (`Frédéric Boccaletti <https://fr.wikipedia.org/wiki/Fr%C3%A9d%C3%A9ric_Boccaletti>`_, NDLR) je sais plus quoi là il y
avait un article sur Street Press qui était sorti qui était du Var
je dirais, peut-être que je diffame le Var je sais pas je sais plus
quelque part dans

Martin Eden
    ça sera coupé au montage

ouais merci désolé les gens du Var mais tu vois c'est pas c'est pas le seul
on parlait de Soral on peut parler

bon ben pas désolé le Var,  Châtillon et là on parle du GUD donc
ancien président du GUD, littéralement néonazi, très proche de Marine Le Pen
finance des trucs de Marine Le Pen, très proche de Jordan Bardella qui était
sorti avec je crois sa fille enfin bref c'est le petit milieu d'extrême
droite, je sais plus comment ça marche exactement là-bas mais en tout cas
Châtillon, Louceteau? qui est littéralement aussi en néonazi qui fait partie

de et puis dès qu'on voit des enquêtes qui sortent sur des groupuscules FAF
néonazis, compagnie là, pululent en France là il y a toujours un zozo qui est
assez con pour qu'on trouve un lien avec le RN

c'est vraiment avoir des aiguilles dans les yeux pour ne pas voir ce qu'on a
un peu en face des yeux c'est-à-dire l'extrême droite française dans toute
sa splendeur, c'est tout

il y a pas 10000 extrêmes droites françaises moi je serais vraiment pardon
non j'y crois pas à ce truc même pas j'y crois pas c'est faux
l'extrême droite elle est antisémite c'est peut-être pas sa priorité
maintenant électoralement parlant pour se faire bien voir, achever
son processus de normalisation et cetera et cetera on entend ça à la télé
toute la journée

enfin j'ai pas la télé mais c'est ce que j'imagine qu'on entend

merci merci

ouais ça c'est n'importe quoi c'est n'importe quoi et on l'a vu avec
Bardella qui dit que son Jean Marie Le Pen
n'est pas antisémite; ça c'est en fait tu as aucune raison de dire ça sauf
si c'est pour blanchir ton parti et c'est exactement faire mais là pour le coup
ils ont été un tout petit peu trop loin et les gens ils ont fait ouais quand
même pas

attendez mais je suis sûr qu'il y a des gens du parti qui étaient
d'accord et on a vu d'ailleurs des gens du FN défendre ce truc et dire non
non je sais pas c'était la ligne du parti donc bon

pardon c'était petit

Léa : nous on luttera en tout cas de toutes nos forces contre l'extrême droite
----------------------------------------------------------------------------------

- https://youtu.be/tYWRnqNL_dk?t=1693

ça c'est vraiment l'extrême droite, c'est l'ennemi des Juifs et de toutes les
minorités en fait donc enfin voilà quel quel que soit le contexte quelle
que soit la situation des Juifs en France et cetera c'est

nous on luttera en tout cas de toutes nos forces contre l'extrême droite et pour rappeler
son passé mais aussi son présent comme tu le dis bien Sender pour
rappeler que ce sera jamais notre allié;

Martin Eden : il y a une vraie hiérarchie des affects racistes au sein de l'extrême droite
-----------------------------------------------------------------------------------------------

- https://youtu.be/tYWRnqNL_dk?t=1720

C'est vrai qu'aussi même au sein de l'extrême droite, il y a vraiment, quand
je disais qu'en général quand tu es raciste tu es rarement raciste que
sur un groupe social effectivement
mais il y a il y a une vraie hiérarchie des affects racistes au sein de l'extrême droite
genre qui on doit détester en premier est-ce que c'est les Musulmans ou les Juifs ?
c'est un peu le ... on va dire de manière ; non mais effectivement mais enfin oui
c'est clairement ça c'est bon on en rigole mais c'est horrible en vrai
de vrai

Léa
    Ouais

mais c'est un peu, c'est un peu la discorde Soral-Zemmour,
Zemmour pour lui c'est les musulmans et Soral c'est plutôt les Juifs et même on pourrait
parler aussi d'un conversion?? d'un Soral aussi quelque part effectivement
donc ; les deux sur le fond sont d'accord sur "il y a trop d'immigrés" enfin
les trois sont d'accord d'ailleurs ; mais les trois ne sont pas d'accord sur la
priorité de quel étranger on doit dégager en premier et quand je dis dégager
c'est peut-être aussi plus si affinité

voilà aussi

Sender
----------
ouais donc parce que mais
c'est intéressant ce que tu dis pardon si je peux permettre de rebondir truc
qui me traverse l'esprit et c'est intéressant ce que tu dis parce que j'ai
l'impression qu'en fait, une des spécificités de l'antisémitisme peut
être vu aujourd'hui selon moi à l'intérieur des mécaniques islamophobes
notamment ce truc du ciment politique, ce truc de en fait d'être un groupe
minoritaire qui arrive à mettre d'accord des gens qui de base n'ont pas vraiment
de quoi se mettre d'accord mais qui établissent une espèce de priorité ; de
de danger;

L'antisémitisme, tu parlais de Soral bon ben vraiment lui c'est
au cœur de sa théorie c'est dire on va mettre d'accord les Blancs
démondialisés des campagnes un peu mascus enfin carrément mascus un
peu fachos avec les jeunesses des banlieux comme Soral les imaginait
et en fait le point d'accroche de ça va être à la fois
un discours **hyper masculiniste et misogyne** mais à la fois en fait aussi de
l'antisémitisme de l'ennemi commun c'est ça

et en fait on le voit aussi à des moments donnés, des transfuges de gauche qui partent à l'extrême
droite par islamophobie aussi tu vois même les ; moi je pense aux guignols
du printemps républicain il y a un peu cette enfin pardon je pensais
juste à ça il y a un peu cette ce même truc aussi et c'est aussi la
preuve qu'il faut se parler il faut travailler ensemble il faut lutter
ensemble, il faut pas être séparé sur des ; c'est pas parce qu'on dit que
l'antisémitisme est un racisme spécifique dont il faut prendre en compte
les spécificités que ça veut dire : "nous on lutte avec personne laissez-nous
tranquille" c'est pas du ..

enfin justement il y a des,  au contraire en fait il y a des choses qu'on peut
comprendre ensemble et voilà c'était le tunnel pour répondre à ce enfin
pour juste dire ce truc du ciment politique en fait que ça a aussi un
usage politique voilà que ça a aussi un usage électoral et un usage politique
l'antisémitisme parce qu'on sait à peu près comment ça fonctionne

- https://youtu.be/tYWRnqNL_dk?t=1882

c'est du populisme, un complotiste et .. on peut parler des
:term:`dog whistle <antisem:dog whistle>` enfin il y a plein de choses
nouvelles maintenant dont il faut se saisir

désolé de la digression


Léa : malheureusement **la gauche et l'extrême gauche ne sont pas non plus  imperméables à l'antisémitisme**
-----------------------------------------------------------------------------------------------------------------

- https://youtu.be/tYWRnqNL_dk?t=1904

et malheureusement  la gauche et l'extrême gauche ils sont pas non plus
imperméables à l'antisémitisme ; l'antisémitisme c'est pas l'apanage
d'un seul parti politique et là voilà on a rappelé un
petit peu pourquoi l'extrême droite c'était clairement une tranche politique
antisémite


.. _jjr_melenchon_2024_02:

2024-02 **Le cas Mélenchon**
-------------------------------

- :ref:`antisem:melenchon`
- https://youtu.be/tYWRnqNL_dk?t=1928

Mais  prenons si on prend l'exemple de l'extrême gauche
de la France insoumise et de son leader :ref:`Jean-Luc Mélenchon <antisem:melenchon>`
lui aussi il est entre déni et relativisation et voilà **il a eu des
paroles qui ont été aussi inacceptables** ;  du coup on peut évoquer quelques exemples
peut-être ?

Martin Eden
    oui ben de toute façon on est là pour rouler sur la gueule de :ref:`Mélenchon <antisem:melenchon>`
    là tu as faire un magnifique, tu as fait une magnifique transition vers


parce que c'est parce que c'est ça en fait, il a parlé complot et tout ça
et ça m'a fait penser à lui parce que ce qui est assez intéressant dans
dans les **paroles hyper dangereuses** de :ref:`Jean-Luc Mélenchon <antisem:melenchon>` c'est qu'il reprend
en fait tous les :term:`poncifs <antisem:poncifs>` antisémites dans ses
différentes prises de parole qu'il a pu avoir


Mélenchon et l'antijudaïsme chrétien
------------------------------------------

- https://youtu.be/tYWRnqNL_dk?t=1989

Il passe de l'antijudaïsme chrétien ; donc quand il parle  du
fait qu'il sait pas qui a mis Jésus sur la croix mais **il sait que c'est ses
compatriotes** voilà donc il reprend le bon antisémite du peuple déicide

Mélenchon et l'élite complotiste
------------------------------------------

quand il parle du complot, enfin Jérémy Corbyn donc le chef du du parti d'extrême
gauche en Angleterre qui a perdu les élections parce qu'il aurait subi donc
une accusation, une grossière accusation d'antisémitisme et que à travers
le grand rabbin d'Angleterre et l'influence du Likoud il n'a pas été élu

donc encore une fois **une élite complotiste avec carrément le rabbin au milieu**

donc et puis et puis aussi voilà il reprend tout le :term:`poncif <antisem:poncif>` antisémite
du complot; du complot de la puissance quand il parle aussi du CRIF que lui on le
verra jamais faire des génuflexions devant les castes arrogantes des communautés du CRIF.


Ca relève presque de l'obsession, Mélenchon associe systématiquement le Juif au pouvoir
------------------------------------------------------------------------------------------

- https://youtu.be/tYWRnqNL_dk?t=2050

Voilà donc c'est assez intéressant aussi de voir que le leader du parti
qui aujourd'hui est le parti majoritaire d'extrême gauche en France **peut
faire preuve donc de paroles comme je disais hyper dangereuses**, mais en fait
ça relève presque de l'obsession **c'est-à-dire qu'il associe systématiquement
le Juif au pouvoir**, le Juif à la finance, aux puissants et il reprend tous les
poncifs en fait

Martin Eden
    c'est vrai ça fait penser aussi à la phrase que :ref:`Mélenchon <antisem:melenchon>`
    faudra que je retrouve la source que je mettrai dans la description mais sur le blog de
    :ref:`Mélenchon <antisem:melenchon>` effectivement je crois que ça date
    je sais plus de quelle année précise faudrait que je retrouve la source
    j'avais tweeté ça il avait dit mais il avait dit effectivement que dès
    qu'un parti politique de gauche s'approche du pouvoir il est bizarrement
    accusé d'antisémitisme

Sender
    ouais c'est pas le rayon paralysant

Martin Eden
    là si si c'est ça et je crois j'avais tweeté pour t'acuser d'antisémitisme
    il faut déjà être au second tour Jean-Luc

    c'est voilà

Sender
----------

là Jean-Luc il est dans une il a une forme monstrueuse ; il est en roue
libre total et en fait tiens pour ajouter juste le plus récent des exemples
parce que ça m'avait quand même rendu dingue ; conférence sur youtube
il y a même pas une semaine là donc sur la chaîne de lui ou de la FI je sais
plus enfin bref ; conférence sur le proche-orient  ok très bien et à un moment
donné où il part sur un truc et dit voilà bon il y a la guerre qui se passe
là-bas mais il y a aussi la guerre qui se passe ici ... la guerre des
classes médiatiques et des protagonistes, et d'une série de protagonistes de
cette nature qui en gros tirent les ficelle et regardent les choses bouger en dessous
d'eux en **mimant avec ses mains les marionnettes**

et en fait, il y a cà fait une semaine il y a pas de problème quoi, il se
fait reprendre dessus par Twitter mais bon ce qui se passe sur Twitter
est-ce que quelque chose on a vraiment quelque chose enfin est-ce que
quelqu'un en a vraiment quelque chose à foutre de ce qui se passe sur Twitter ?

dans la vraie vie il passé toujours tranquille dans non mais oui je comprends mais

Martin Eden
    Je pense c'est mon avis mais après effectivement tu as raison mais là effectivement
    tu as raison

mais là pour le coup tu vois il y a eu, il y a eu quelques personnes qui
ont mais voilà on n'a pas d'audience et cetera il y a pas eu de sursaut
du parti pour dire excuse-moi Jean-Luc tu peux pas faire ça

Mélenchon a une impunité totale au sein de LFI
------------------------------------------------------

- https://youtu.be/tYWRnqNL_dk?t=2197

Et en fait c'est ça aussi quand les exemples qu'a rappelé Léa ;
le problème aussi c'est non seulement il y a la tête de
la France Insoumise (FI) **qui fait n'importe quoi et qui dit des trucs ahurissants,
il y a une impunité totale**

Léa
    ouais il est en déni en fait


- déni
- y a du :term:`antisem:gaslighting`,
- y a des explications foireuses
- y a le fameux "mais avez-vous lu son livre ?",  il a écrit une phrase
  où il dit que c'est pas bien ;
- il y a le fameux "c'est la faute à la LDJ" qui nous a viré pendant
  la manif de :ref:`Mireille Knoll <antisem:mireille_knoll>`

mais la LDJ on est tous et toutes d'accord ici pour dire que c'est des
gros fafs ; mais cette histoire d'antisémitisme elle pas commencé à ce
moment-là;  il est pas fini à ce moment là non plus

comme quand on parle du CRIF ; en fait il y a un moment donné le CRIF
faut arrêter de lui accorder un pouvoir complètement ahurissant qu'il n'a pas

L'association du Juif et du pouvoir elle est récurrente dans la tête de Mélenchon
--------------------------------------------------------------------------------------------

Je veux dire c'est ... c'est comme disait Léa ; c'est l'association du
Juif et du pouvoir **elle est récurrente dans la tête de** :ref:`Mélenchon <antisem:melenchon>`
enfin ça transpire dans ton parti ; si c'est normal de dire ça; si tout le monde
défend ce point de vue là

- https://youtu.be/tYWRnqNL_dk?t=2267

moi j'avais réagi?? pareil ; une autre émission je finis juste là-dessus
sur YouTube sur la chaîne de l'émission populaire c'est ça leur émission
à LFI ? .. je crois l'émission populaire bref donc il y avait
Daniel Schneiderman il y avait Manuel Bompard il y avait d'autres députés LFI
c'était un plateau full LFI et il y avait juste Daniel Schneiderman qui était
soutien LFI et en gros ils parlent du tweet de sur `Ruth Elkrief <https://fr.wikipedia.org/wiki/Ruth_Elkrief>`_ là bon

en plus on se retrouve à devoir entre guillemets, je met des gros guillemets "défendre
Ruth Elkrief" mais je vais dire le tweet de il est nul; il est quand même il sait
ce qu'il dit :ref:`Mélenchon <antisem:melenchon>` il est responsable des mots qu'il emploie quand
il parle de ces choses là et en fait surprise l'antiracisme on peut être
une personne qui ; pas sympa qui fait subir des dominations aux autres et être
victime de racisme ça existe et c'est deux choses qui sont différentes

les victimes de racisme ne doivent pas uniquement avoir un CV
à présenter pour qu'on soutienne une victime de racisme parce qu'en fait
on s'attaque au racisme; on s'attaque à l'antisémitisme

on s'attaque pas à la personne à :ref:`Jean-Luc Mélenchon <antisem:melenchon>`
en particulier parce qu'on aime pas c'est c'est contre un phénomène en fait
qu'on lutte et donc du coup tout ce plateau c'était de dire c'est pas
notre faute c'est pas notre, faute

il y a même une des députés qui s'est tourné vers Schneiderman et qui a dit
non mais vous vous rendez compte comment c'est dur pour nous de se faire
accuser d'antisémitisme ?

sont hors sol les gens
même lui il était mal à l'aise il je sais pas quoi répondre à ça
et donc il y a un déni total ou

Léa : les gens de LFI sont dans un déni total
---------------------------------------------------

- https://youtu.be/tYWRnqNL_dk?t=2361

C'est des gens **qui parlent de l'instrumentalisation de l'antisémitismeisme
plus que de l'antisémitisme en lui-même** et oui ils sont dans ils sont
dans un déni total

on l'a vu aussi avec `Alexis Corbière <https://fr.wikipedia.org/wiki/Alexis_Corbi%C3%A8re>`_
sur le plateau de mediapart avec :ref:`Jonas Pardo <antisem:jonas_pardo>` et llana Weizman où en fait ils
ont exposé mais voilà encore plein d'exemples des sorties antisémites de
:ref:`Mélenchon <antisem:melenchon>` et `Alexis Corbière <https://fr.wikipedia.org/wiki/Alexis_Corbi%C3%A8re>`_
va trouver une réponse à tout quoi

ah oui mais ce qu'il a dit sur Jésus et la croix c'est parce que c'est un enfant de
cœur il a fait le catéchisme

c'est pas une raison Je veux juste dire on parle on parle d'un
leader politique en fait et aucune remise en question, aucune écoute
de la parole des concernés parce que enfin je veux dire il y a plein de
Juifs qui ont essayé de leur parler, de rentrer dans des médiations avec
eux avec la France insoumise certains députés et cetera

ça n'a jamais abouti à quoi que ce soit c'est-à-dire qu'ils sont toujours
dans le déni de l'antisémitisme au sein de leur parti

Sender
------------

- https://youtu.be/tYWRnqNL_dk?t=2427

Il y a vraiment une faille Corbyn quoi en fait chez :ref:`:ref:`Mélenchon <antisem:melenchon>`
c'est pour ça qu'il parle de `Corbyn <https://en.wikipedia.org/wiki/Jeremy_Corbyn>`_ tout le temps
aussi il  y a cette même trajectoire qui est prise, **de on va rien faire**

Martin Eden
------------------

Bah c'est comment dire sur, j'ai vu ce débat trois quatre fois de Corbière
face à :ref:`Pardo <antisem:jonas_pardo>` et :ref:`Illana Weizman <antisem:illana_weizman>`,
c'est le malaise sur 20 en fait

c'est, c'est énorme malaise quoi, c'est pendant 2 heures tu voyais :ref:`Jonas Pardo <antisem:jonas_pardo>`
et :ref:`Illana Weizman <antisem:illana_weizman>` sont pas forcément
c'est c'est pas des toto tu vois c'est enfin voilà c'est avec tout le
respect que je leur dois c'est pas des purs anarchistes de ouf tu vois
ils mettent en avant des faits et Corbière il botte en touche à chaque fois
il botte en touche **mais c'est insupportable**

Léa
------

- https://youtu.be/tYWRnqNL_dk?t=2464

systématique avec les avec ; que ce soit les militants ou les élus de la
France Insoumise enfin c'est pareil ils ont pas voulu participer à la
manifestation du 12 novembre (2023) contre l'antisémitisme alors qu'on est en train
de vivre dans un contexte particulièrement tendu pour la communauté juive en
France sous prétexte que le RN viendrait instrumentaliser la lutte
contre l'antisémitisme mais en fait il est où votre soutien là-dedans ?

c'est-à-dire est-ce qu'on peut parler  du problème de base qui est
quand même l'antisémitisme ? c'est

Martin Eden
---------------

- https://youtu.be/tYWRnqNL_dk?t=2500

même au-delà de pour j'entends des?? les remarques dans les commentaires

oui on va pas marcher avec des on va pas marcher avec des racistes dans
une manifestation

Léa
----

oui oui non mais ça s'entend mais en fait on en oublie la cause principale
qui est la solitude de la communauté juive face à une montée extrême des actes
antisémites en France

Martin Eden

oui c'est surtout c'est une euphémisation ... après effectivement
c'est une enfin c'est une rhétorique qui euphémise constamment parce qu'on
on pourrait me dire "oui" enfin je enfin comment dirais-je c'est on pourrait
dire aussi "oui mais" comment dirais-je comment
les théories qui mobilisent le racisme structurellement ce sont enfin
c'est plutôt la rhétorique antimusulmane qui incarne le racisme concret,
systémique et cetera
et c'est pas les théories antijuives enfin ; c'est et sauf que là j'ai envie de dire non
pas tellement non plus tu vois parce que l'un et l'autre sont pas ; vivent pas
dans un univers séparé en fait

Léa
    exactement

Sender
----------

déjà ça et en fait, au-delà de ça moi j'entends pareil souvent ce
truc de dire tu vois on parle de racisme d'État ; dire  l'antisémitisme n'est pas un racisme d'État
en réalité en France bon je sais pas moi je suis pas sociologue j'ai pas mené
des grosses études si ça se trouve il y a des pourcentages très infimes de tu
vois des difficultés à trouver du boulot et cetera mais a priori c'est pas
parce que tu es Juif en France que on te refuse des apparts et qu'on refuse
du boulot et que tu vis dans des situations hyper précaires parce que tu es
assigné à

c'est beaucoup plus compliqué quand même vis-à-vis de l'État et
des institutions pour une personne musulmane, arabe, noire qui n'est pas
dire blanche de peau parce que tu as aussi des personnes juivfes qui sont pas
blanches de peau bref

mais par contre l'antisémitisme c'est pas parce qu'il n'a il n'est pas
actuellement, parce qu'il a été par le passé mais il n'est pas actuellement
porté par l'Etat qui met en place des lois discriminatoires envers les
personnes juives **c'est pas pour ça que l'antisémitisme n'existe plus**

- https://youtu.be/tYWRnqNL_dk?t=2616

l'antisémitisme il a pas attendu les États pour exister ;
d'ailleurs il a même pas attendu d'être au pouvoir pour exister ;

je vous rappelle quand même l'affaire Dreyfus c'est un des ; ça devrait
être un des points d'orgue de la gauche quand même dans l'histoire parce
que c'est des bonnes leçons à apprendre

je pense qu'on parlait justement de on soutient pas le CRIF même quand
il subissent de l'antisémitisme parce que c'est le CRIF ? .. l'affaire
Dreyfus c'était un militaire bourgeois et la gauche elle a compris à ce
moment-là que même si c'était un militaire bourgeois ah ses Juifs tous
riches et ben il fallait quand même le défendre **parce que l'antisémitisme
est un combat universel tout comme le racisme est un combat universel**


c'est pas parce qu'il était militaire et bourgeois c'est parce qu'il
était accusé à tort en tant que Juif c'est ça qui était important
bref parenthèse Dreyfus fermée ; tu as ce ...  et merde je me suis perdu
pardon

mais la parenthèse Dreyfus elle était

Léa
    oui pour faire le lien avec la gauche quoi

c'est pour faire le lien avec la gauche et cette lecture du racisme d'État
elle est pas elle peut pas être complète pour l'antisémitisme puisque
l'antisémitisme se manifeste aussi en dehors des Etats

il est aussi produit par des collectifs ou des individus qui ne sont pas
des organes étatiques qui ne sont pas des institutions mais qui véhiculent
tout de même de l'antisémitisme et qui peut se matérialiser dans plein
de différentes façons dans le quotidien des Juifs tous les jours quoi

Martin Eden
-----------------

et surtout aussi je voudrais revenir sur un exemple d'antisémitisme vraiment
mobilisateur qui n'est pas étatique mais qui est quand même assez dangereux:

- les qanon
- la prise du Capitol

à un moment donné il y avait un des connards qui avait
marqué Auschwitz 6 millions de morts à mon avis c'était bon enfin
est-ce qu'on
enfin la prise du Capitol c'est pas c'est pas anecdotique en fait j'aurais
même tenté de dire que toutes les théories du complot sont fondamentalement
antisémites aussi et on peut acter que peut-être que les théories du complot
sont assez présentes on va dire dans la société donc après on pourrait dire
aussi que la théorie du Grand remplacement est effective et cetera

oui effectivement mais malgré tout effectivement l'antisémitisme pour l'instant
n'est pas avant tout une œuvre Etatique enfin quelque chose d'Etatique mais
mais effectivement il est quand même répandu de, il est quand  sévèrement
répandu à travers théorie du complot

quoi la fameuse pancarde "qui ?" en fait

Sender
    oui oui

Léa
-----

et puis c'est peut-être pas aujourd'hui en France en tout cas par des
institutions étatiques que ça se traduit mais par exemple à l'école ou
à l'université c'est des endroits où l'antisémitisme est très très
présent et ça c'est des institutions de l'État en fait

donc c'est peut-être pas sous forme de violences policières ou quoi comme ça
peut comme ça peut être pour d'autres minorités racisées mais c'est
quand même au sein d'institutions de l'État


Martin Eden : la rhétorique vraiment d'extrême droite est infusée dans la rhétorique même de l'État
---------------------------------------------------------------------------------------------------------

- https://youtu.be/tYWRnqNL_dk?t=2793

mais bon on pourrait aussi questionner enfin on pourrait questionner
aussi voilà comment ça se fait que l'imaginaire antisémite aussi a
enfin l'imaginaire complotiste et antisémite a autant le vent en poupe ;
que fait l'Etat et cetera et cetera

au mieux il y a une complaisance et enfin voilà quoi quand j'entends par
exemple Macron dire qu'il faut "réarmer démographiquement" la nation
effectivement là on peut se dire effectivement que l'État a quand même
enfin la rhétorique vraiment d'extrême droite est infusée dans la rhétorique
même de l'État

donc à un moment donné bon j'ai envie de dire aussi ok oui effectivement il y a pas
d'antisémitisme vraiment étatique pour l'instant bon ça a été le cas
assez longtemps quand même bon on va pas non plus enfin j'ai pas non plus
polémiquer 5000 ans là-dessus

mais il y a quand même quelque chose, il y a quelque chose sinon les actes
antisémites n'auraient pas explosé en fait et c'est pas que Alain Soral
aussi même si je l'ai un peu mis en avant ici ça ce n'est pas que lui

Sender
--------------

- https://youtu.be/tYWRnqNL_dk?t=2849

mais tu vois le .. notre cher président qu'on a là mais c'est un parfait
exemple en fait c'est que il passe pas de législation qui va discriminer
matériellement, par le biais de l'État, les minorités juives

par contre ça l'empêche pas de réhabiliter Maurras, ça l'empêche pas de
parler du bon Pétain, ça l'empêche pas de reprendre en fait les
poncifs antisémites parce que en fait Darmanin, Napoléon et cetera
on en a parlé de ça mais je veux dire c'est tellement ; c'est là où on parle
de choses qui est structurelle, qui est culturelle, qui existe dans la toile de
fond de la société française parce que ça fait quand même excessivement
longtemps que dans cette région du monde les Juifs ont des problèmes

- https://youtu.be/tYWRnqNL_dk?t=2892

et ça se matérialise différemment selon les époques et c'est
là et c'est ça qui est important en fait de comprendre c'est l'antisémitisme
c'est pas que "oh là là il y a 1000 % d'explosion", il y a des risques d'attentat
il y a des morts et cetera ça c'est la version hardcore horrible ... de ça

mais en fait sinon c'est plein d'autres choses donc c'est juste pour
finir aussi sur cette histoire de ; l'État il est peut-être pas producteur
en tant que tel de d'antisémitisme à travers son bras on va dire légal
mais il participe à des atmosphères, il participe à une atmosphère de
banalisation et de se dire ah effectivement des penseurs comme Barrès,
comme Maurras, comme Pétain sont des gens respectables

- https://youtu.be/tYWRnqNL_dk?t=2945

donc quand ils écrivent des trucs antisémites c'est à prendre voilà
c'est quand même partie de l'histoire française Barrès tout ça faut
sa par de soi??

Martin Eden
--------------

c'est-à-dire aussi moi je men souviens aussi il y avait pas mal il y a eu
pas mal de en ce moment de d'articles sur `Maurice Barrès <https://fr.wikipedia.org/wiki/Maurice_Barr%C3%A8s>`_ ce
grand nationaliste français enfin je crois que c'était dans le Monde dans le
Figaro je sais plus lequel

Sender
    Figaro c'était le Figaro

mais là tous les gens qui tombent sur l'antisémitisme, les déclarations
antisémites de :ref:`Mélenchon <antisem:melenchon>` là bizarrement ils
avaient tous aqua poney ? bizarrement

ou c'est non mais sans déconner tu vois je suis

Sender
    enfin mais c'est ça c'est à mort Marx vive `Barrès <https://fr.wikipedia.org/wiki/Maurice_Barr%C3%A8s>`_
    mais c'est n'importe

voir même aussi sur France Culture quand ils s'étaient extasiés qu'on avait
retrouvé des des papiers de Céline avec `Finkielkraut <https://fr.wikipedia.org/wiki/Alain_Finkielkraut>`_
j'ai envie de dire mais enfin mais ranger vos tables les mecs quand même
arrêtez de bander aussi dur je vous sens même à travers le truc

mais c'est enfin mais comment enfin mais c'est enfin on a le droit d'aimer Céline,
c'est apparemment faudrait que je lise pour vraiment me faire un avis mais oui
on a le droit d'aimer Céline, c'est pas c'est pas le problème mais quand tu sens
limite l'orgasme à travers un micro je sais pas dose un peu quand même le gars
"bagatelle pour un massacre" je sais pas ça te dit quelque chose ?

Sender
-------

ou si tu veux juste pour ouais le micro-point sur Céline en fait ; le
problème c'est pas d'aimer ou pas Céline et
c'est comme pour toutes les représentation antisémites, enfin tous les gens
qui ont pu être antisémites dans l'histoire de France;  l'idée c'est pas de
les "cancel postmortem" pour toujours et de détruire leur ;

c'est pas les gens qui luttent contre l'antisémitisme qui brûlent les livres
**c'est les antisémites qui brûlent les livres ; d'accord ? faut pas oublier ça
aussi**

et en fait non c'est l'idée de dire peut-être qu'on ne pourrait
ne pas en faire une figure nationale, grand écrivain français, et c'est
en fait là on parle pas de n'importe qui on parle d'un nazi

mais là on est ; attends on est peut-être dans les extrêmes parce que quand
je parlais de truc, de l'atmosphère qui est laissée en fait par ce genre
de banalisation et ce genre de normalisation

je voulais introduire, transitionner vers une partie de Léa parce que c'est ça
cette transition

Léa : parler de la traduction de l'antisémitisme dans le quotidien des Juifs aujourd'hui
---------------------------------------------------------------------------------------------

non mais c'est vrai qu'on qu'on a un temps limité et qu'on voulait vraiment
ben parler de la traduction de l'antisémitisme dans le quotidien des Juifs
aujourd'hui parce que c'est vrai qu'aujourd'hui en France donc on en parle
beaucoup là ça a fait des gros titres récemment donc les actes antisémites
ont explosé

donc l'antisémitisme il se traduit au quotidien:

- par des paroles,
- des comportements  à caractère discriminatoire,
- des harcèlements,
- des agressions:

  - à l'école,
  - à l'université
  - au travail
  - dans la rue
  - sur les réseaux sociaux

voilà

on rappelle quand même que en France sur les 15 dernières années il y a
eu 14 personnes qui ont été tuées parce que Juives

aujourd'hui donc depuis le le 7 octobre 2023 on est dans un contexte
qui est extrêmement tendu ; ben évidemment lié au à la guerre en Israël
et en Palestine et donc on voulait donner quelques exemples concrets
aussi de ce que vivent la communauté juive en France actuellement

donc il y a une présence militaire quand même devant les synagogues aux horaires
des prières donc savoir que aujourd'hui pour pouvoir prier en sécurité
en tant que Juif, il faut se cacher derrière des policiers ou des militaires et ???

pendant des semaines après le massacre du 7 octobre en Israël il y a eu

- des parents qui ont pas mis leurs enfants à l'école confessionnelle
  parce qu'ils avaient trop peur,
- les restaurants casher sont désertés,
- les Juifs enlèvent leurs `mezouzahs <https://fr.wikipedia.org/wiki/Mezouzah>`_
   donc c'est le petit le petit boîtier qui contient une prière devant les
   portes des maisons
- les Juifs changent leurs noms dans les applications comme uber sur les
  interphones
- et cetera

**bref il faut savoir que là on est en train de vivre une réalité où les
gens se cachent, ils ont peur ils se font discrets.**

Nous aussi on a voilà vécu un énorme choc et un énorme traumatisme après
l'attentat :ref:`d'Ozar Hatorah <antisem:ozar_hatorah_2012>`
ou celui :ref:`de l'Hyper casher <antisem:hypercasher_2015_01_09>` et
c'est resté en fait dans toutes les têtes de la communauté
juive donc **les gens sont vraiment terrorisés à l'idée que ça se repasse**

et voilà et on voulait vraiment rappeler ce contexte parce que
c'est important en fait de dire la réalité de ce qu'on vit aujourd'hui en
tant que Juif en France ; donc évidemment que l'État aussi a sa part de
responsabilité dans un contexte aussi tendu



Sender
--------

- https://youtu.be/tYWRnqNL_dk?t=3218

oui et puis quand on parle si je me permets juste de compléter ce que dit ...
c'est quand on parle notamment par exemple de présence militaire dans les
synagogues de des choses qui sont des ; qui font partie de l'appareil d'État et
qui sont mobilisés pour protéger on va dire physiquement l'intégrité
des personnes juives qui vont à la prière, qui vont à l'école et cetera

moi j'entends beaucoup dire enfin en fait moi j'entends beaucoup dire j'ai
l'impression que ça se dit et j'ai entendu dire ce truc de ben regarder au
moins ils sont, vous êtes protégés comme si c'était une forme de privilège

moi j'estime, moi par exemple je suis pas forcément la personne qui va le
plus souvent de sur Terre à la synagogue ou à l'école juive mais je
pense pas que ça soit très agréable en réalité d'être rappelé dès que
tu sors et rentres dans un établissement qu'en fait il y a potentiellement
des menaces d'attentat ; c'est ça au final

c'est pas les Juifs ne vont ne vont pas se dire ah qu'est-ce que je suis
heureux aujourd'hui que quand depuis qu'il y a l'armée devant la synagogue
tellement plus agréable en fait ce serait plus agréable de de pas avoir
des menaces d'attentat tout le temps

donc c'est pour ça qu'il faut s'attaquer à l'antisémitisme il faut pas
s'attaquer en disant regardez l'Etat protège les Juifs alors il
protège pas les autres minorités; le problème c'est qu'on protège pas les
autres minorités ; le problème c'est pas qu'on protège les Juifs

et là pour le coup, l'État qui met en place un appareil d'État pour protéger
les Juifs face à des menaces terroristes qui sont arrivés quand
même assez souvent en France pour qu'on puisse se poser la question c'est
juste normal ; **en fait c'est juste nécessaire**.

- https://youtu.be/tYWRnqNL_dk?t=3320

Léa
    c'est ça avant d'être un privilège ; en fait le privilège ça serait de
    pouvoir vivre son judaisme tranquillement


ouais et aussi par rapport à ça en fait pourquoi est-ce qu'il y a des
périodes d'hypervigilance quand il y a des hausses notamment d'actes
antisémites ?

c'est qu'on peut remarquer en réalité quand on regarde un peu
des petites courbes, des trucs et tout que l'antisémitisme et
l'explosion d'actes antisémites fonctionne généralement comme un effet de
boule de neige où un acte antisémite libère d'autres potentiels actes
antisémites

c'est à dire qu'il y a une forme comme on dit en fait c'est à partir
du moment où quelque chose est une porte est ouverte il y a des gens qui
s'engouffrent dedans et ça c'est la même mécanique un peu de
que de faire sauter les tabous ou c'est on se dit tout de suite ; il y a une
forme de ; l'antisémitisme nourrit l'antisémitisme

Léa : très important de rappeler cet effet boule de neige qu'on vit tout le temps dès qu'un acte arrive
------------------------------------------------------------------------------------------------------------

oui c'est assez dingue en fait c'est vrai en fait dans les statistiques
on voit que au lendemain des attentats antisémites les plus graves
qu'il y a eu en France c'est là qu'il a eu les plus grandes vagues
antisémites, **d'actes antisémites en tout cas**
donc **c'est ça très important de rappeler cet effet boule de neige qu'on
vit, tout le temps, dès qu'un acte arrive**.


Sender : l'antisémitisme commence dans les pensées dans l'idéologie
------------------------------------------------------------------------

- https://youtu.be/tYWRnqNL_dk?t=3402

Dès qu'il y a un acte qui arrive et aussi parce que l'antisémitisme comme
tu disais Martin au début, ça commence pas le 7 octobre, la situation des
Juifs en France il faut aussi à un moment donné considérer que il y a
pas que les attentats qui sont de l'antisémitisme.

en fait l'antisémitisme il commence avant les attentats, il
commence avant les tueries, et il commence avant les projets de meurtres
et d'assassinats

il commence dans les pensées, dans l'idéologie, dans ce qui
est normalisé, dans ce qui est conçu, dans ce qui est construit
en fait au sein de la société française et ça il y a des choses qui n'ont
absolument aucun rapport avec le conflit au Proche Orient, qu'on soit bien clair

le fait que les Juifs soient associés aux médias, qu'ils soient associés
enfin trop de pouvoir dans les médias, de l'argent ; il y a des gens qui
pensent clairement ouais même le truc de Jésus ; les meurtriers de Jésus
et compagnie mais ça c'est trucs qui ont rien à voir qui sont
quand même présents

le négationnisme de la Shoah c'est pas un truc qui émane du conflit israëlo-palestinien
c'est un truc qui émane de l'antisémitisme

Léa
    Donc c'est là c'est latent et puis quand il y a une vague de violence au
    proche Orient en fait c'est là que ça fait un ça fait boom mais c'est là
    tel un ??poison??


- https://youtu.be/tYWRnqNL_dk?t=3470

Martin Eden
    je voudrais dire aussi l'antisémitisme commmence si vous trouvez qu'il on a trop
    sur cette chaîne aussi ; je dis ça je dis rien

Sender utilisation du gaslighting
-------------------------------------

oui mais ça c'est pareil en fait c'est la la vieille idée de dire c'est
vraiment moi j'entends ça j'entends des gens qui disent maintenant que
j'avais discuté avec un gars bon c'est pas représentatif peut-être
mais ah mais non mais le marionnettiste c'est pas un Juif, les Juifs
sont pas marionnettistes c'est il y a quand même il y a quand même une
histoire en fait à cette image et enfin ça quand même été utilisé à un moment
donné c'est quand même "oui mais il est pas juif techniquement"

mais non le marionnettiste n'est techniquement pas Juif mais c'est ce
qu'on appelle du :term:`antisem:gaslighting` en fait c'est en fait
c'est vous qui parce que vous dénoncez des tropes antisémites qui
ne sont en fait pas des tropes antisémites **c'est vous qui êtes antisémites**
puisque c'est était là genre oh là le triple salto arrière


Martin Eden
    surtout sur cette imagerie du marionnettiste je voudrais renvoyer à la
    fresque qui a eu lieu du côté d'Avignon avec Attali

Léa
    oui c'est ça c'est ça


Sender
    oui et ça c'est pas ça c'est pas Israël Palestine ; ça c'est Attali ;
    les gilets jaunes c'était pas non plus Israël Palestine

    j'espère que tous les gilets jaunes grosse, partie le pass vaccinal l'étoile jaune
    et compagnie en fait voilà c'est ; le folklore est présent

Martin Eden
    c'est oui on pourrait se demander comment ça se fait que
    le gars qui a fait le le graf du côté d'Avignon d'où il a eu cette idée ?
    tu vois d'où il a eu cet imaginaire qui est antisémite voilà
    qu'est-ce qu'évoque Attali pour lui ?
    à mon avis c'est pas le pouvoir de l'amour quoi quelque part
    donc donc

Léa
    c'est le pouvoir dans l'ombre enfin c'est vraiment
    la base de l'antisémitisme quoi au début la vision du monde avec une élite
    complotiste


Martin Eden l'antisémitisme c'est un peu technique ... et les gens qui n'ont pas les codes face à des gens qui les ont effectivement **on passe pour des parano**
--------------------------------------------------------------------------------------------------------------------------------------------------------------------

je veux dire l'antisémitisme a tellement été théorisé aussi
quelque part c'est un peu le revers de la médaille c'est pour quelqu'un
qui sait un petit peu la mécanique et un peu les on va dire les grandes
lignes du machin c'est que pour lui c'est le coup du marionnettiste j'ai envie dire
c'est évident mais pour quelqu'un qui est pas initié c'est tu peux on peut
facilement passer pour quelqu'un de parano en fait

c'est ça aussi le comment dire un peu un peu le piège quoi c'est de
l'antisémitisme aussi c'est voilà c'est ça renvoie à toute une histoire
toute une histoire extrêmement longue

donc en 2000 ans d'histoire on a eu le temps de faire à peu près, on a eu on a
eu le temps d'expérimenter des trucs en terme de saloperie donc effectivement
pour des gens qui s'arrêtent sur genre l'antisémite c'est forcément
le gars en chaine en cuir avec une croix gammée bon voilà et qui passe
de temps en temps au Hellfest et c'est oui pour lui l'antisémite c'est ça

non l'antisémitisme c'est plus technique, c'est plus pervers, c'est plus
insidieux, et donc voilà c'est aussi quelque part on paye aussi
peut-être un peu, enfin on s'imagine que le, ouais que le nazi c'est forcément
le gars en treillis enfin avec un ??train Chan?? bien qui a un accent allemand qui
s'appelle Hans


non c'est l'idéologie nazie peut aussi avancer masquée et cetera
et cetera bon après ça renvoit toute une série;  de vraiment de stratégie
de ; le fascisme arrive masqué et cetera et cetera qui peut arriver peu sur la
pointe des pieds avec un comment dire avec une rhétorique vaguement sociale
comme dirait voilà Auguste Bebel "le socialisme des imbéciles", faire du Juif la
figure maléfique du capitalisme voilà et cetera et cetera

- https://youtu.be/tYWRnqNL_dk?t=3664

donc c'est là aussi que l'antisémitisme c'est un peu technique aussi comme
truc effectivement faut avoir un peu les codes et les gens qui n'ont pas les
codes face à des gens qui les ont, effectivement **on passe pour des parano** en
fait.

Sender : je pense qu'il y a un travail d'auto-formation, d'éducation populaire à faire
-----------------------------------------------------------------------------------------

- https://youtu.be/tYWRnqNL_dk?t=3675

je pense qu'il y a un travail de ; d'auto-formation d'éducation
populaire enfin d'essayer, de vulgarisation comme on préfère
dire de didactique autour de ça mais en réalité enfin je veux dire
l'antisémitisme il est spécifique autant que la négrophobie est spécifique,
autant que l'islamophobie est spécifique moi j'ai tendance à
dire je pense même pas que l'antisémitisme soit plus compliqué que les
autres racismes, je pense juste qu'il a sa  propre manière de fonctionner

c'est pas ; parce que en fait c'est effectivement quand quand on va
commencer à dire oui alors les :ref:`"qu'est-ce que tu penses des tests de Postone" <raar_2024:moiche_postone_2024_02_09>`

bon ça c'est peut-être c'est pas tellement, c'est peut-être un peu plus
poussé mais c'est surtout parce qu'il faut avoir des bases de marxisme et de
compagnie pour s'aider?? ; après l'antisémitisme en tant que tel il est
il a été beaucoup théorisé par le passé j'ai l'impression qu'il est un
peu moins dans le présent sauf par trop de gens qui sont trop de droite


Sender : il faut mieux comprendre en tout cas les termes de ce qu'on définit
--------------------------------------------------------------------------------

- https://youtu.be/tYWRnqNL_dk?t=3735

mais c'est ... **il faut mieux comprendre en tout cas les termes de ce qu'on
définit c'est pour ça que on voulait faire la partie en définition
au début** aussi sur dire mais en fait effectivement quand tu traites
quelqu'un d'antisémite ; enfin quand tu dis que quelque chose est antisémite
déjà de:

- 1) pas le prendre perso ; c'est pas grave tout le monde est antisémite ; on
  avance ensemble et on va vers le mieux
- mais deuxièmement: tu as vraiment un truc de "non je suis pas antisémite
  parce que je ne suis pas nazi" oui mais il y a des antisémites qui ne sont
  pas nazis

là par exemple dans ; ça me fait penser à un micro truc ; c'est David Guiraud
qui  répond à Libé interview croisé avec son père et tout et qui balance
"oui je me suis formé au au conflit israëlo-palestinien par le biais de
Soral et Dieudonné parce qu'il y avait rien d'autre à l'époque"

il y avait pas rien d'autre à l'époque et après "mais non mais après j'ai
vu que c'était craignosse et toute façon je ne suis pas Dieudonné"
et tu mais encore une fois mec tu as pas besoin d'être un nazi ou d'être
Dieudonné ou d'être quelqu'un pour être antisémite ;

l'antisémitisme c'est pas juste : il y en a un nombre défini d'antisémites
au monde et dès qu'on les aura tous éradiqués il y aura plus d'antisémitisme
c'est quoi c'est mais

Léa
    il y a pas un profil type c'est clair enfin voilà

c'est ça c'est vraiment une une idéologie qui va en fait qui a pré?? et
qui a succédé ?? les nazis aussi

et en fait Maurras n'est pas nazi par exemple mais il faut comprendre
qu'il est aussi antisémite

Martin Eden sur Guiraud (LFI)
--------------------------------

- https://youtu.be/tYWRnqNL_dk?t=3828

c'est vrai que Guiraud, sa défense avec les dragons célestes
j'ai encore mal à la tête

Sender
    ouais bon sa polémique de polémique

oui mais quand même quand tu es accusé d'antisémitisme enfin quand ça déc?? son
interview portrait croisé avec son père plus celle la conférence en Tunisie
plus ça et j'ai envie de dire à Moné?? je me je me fasse palme?? je dis

mais tu as jamais envisagé d'être, de réfléchir avant de parler c'est
c'est ..

Sender : comment est-ce que on met en application une vraie lutte contre l'antisémitisme
-----------------------------------------------------------------------------------------

mais regarde en fait au final avec tout ça il a le le quart du parcours
de :ref:`Mélenchon <antisem:melenchon>` en la matière pour l'instant
et mélenchon il se porte très bien ; donc c'est ça aussi le problème
c'est après après vraiment

mais peut-être pour faire la transition vers le  point d'orgue on
va dire mais tu vois qu'est-ce qu'il y a à faire là dedans ? où est-ce qu'on
en est ? bon là qu'on a un peu pris la température de où est-ce qu'on en
était mais parce qu'il va falloir penser à des enfin au-delà de
de ce qu'on a dit déjà là en fait

comment est-ce que on met en application une vraie lutte contre l'antisémitisme
parce que là on en parle aussi beaucoup de l'instrumentalisation de la lutte,
l'instrumentalisation de la lutte ok d'accord

Martin Eden
     mais en fait déjà c'est pas minimisé déjà


.. _lea_mini_2024_02_25:

Léa : la minimisation de l'antisémitisme c'est clair que c'est un combat
---------------------------------------------------------------------------

- https://youtu.be/tYWRnqNL_dk?t=3900

c'est ça c'est ça clairement la minimisation de l'antisémitisme
c'est clair que c'est un combat ; nous?? on pense en fait que dans dans
cette période là où l'extrême droite elle était diabolisée, aux portes du pouvoir et
où la parole raciste elle est libérée ; et c'est aussi une responsabilité
de notre gouvernement on l'a rappelé là mais voilà voit aussi avec la
loi immigration et tout ça

**on a vraiment d'une gauche forte et claire sur la lutte contre l'antisémitisme**
donc pour nous il faut travailler vraiment à une réponse globale en fait
qui va éviter deux aspects:

- 1) tu viens dele dire donc la minimisation de l'antisémitisme ou en tout
  cas l'isolement de la lutte contre l'antisémitisme
- 2) et son utilisation à des fins racistes donc ça c'est vraiment ce qu'on
  disait par rapport à l'extrême droite

en fait ces deux attitudes politiques elles ont pour effet de décentrer la
question de la lutte contre l'antisémitisme en elle-même et comme aspect de la
lutte antiraciste en général pour la subordonner à des agendas qui lui sont
extérieurs et en fait ça a pour effet encore une fois d'isoler la minorité
juive et de l'exposer encore plus violemment à l'antisémitisme et de faire
reculer au passage le front global contre le racisme.


.. _lea_vic_2024_02_25:

Léa : il faut que les victoires des uns elles puissent alimenter celles des autres
------------------------------------------------------------------------------------

- https://youtu.be/tYWRnqNL_dk?t=3975

En fait **il faut que les victoires des uns elles puissent alimenter celles
des autres** et on est venu à partir du début des années 2005 à peu près à
opposer en fait les avancées des luttes des uns et des autres donc avec
notamment **la notion de concurrence victimaire** en fait on en parlait tout
à l'heure et ça  évidemment ça fait le bonheur des racistes **donc on pense
vraiment qu'il faut un front commun**

Martin Eden
    c'est surtout aussi arrêter de voir l'antiracisme comme un gâteau où les
    Juifs auraient tout, les 3/4 de la part aussi ; l'antiracisme n'est pas un gâteau
    c'est enfin il y a toujours cette vision un petit peu "oui mais les juifs
    ils prennent quand même toute la place avec leur saloperie de Shoah"


ouais **c'est vraiment la concurrence victimaire** quoi on parle
toujours de la Shoah, on parle jamais de l'esclavage, on va comparer le nombre
de chapitres dans les programmes d'histoire, on va dire "ah voilà on parle tout
le temps des actes antisémites mais on parle jamais des actes islamophobes"


.. _lea_gen_2024_02_05:

Léa : il faut combattre l'antisémitisme pour lui-même mais en l'incluant dans un combat général contre tous les racismes
---------------------------------------------------------------------------------------------------------------------------

- https://youtu.be/tYWRnqNL_dk?t=4037

Enfin, en fait il faut réussir à sortir de ça pour au contraire **une convergence
de ces aspects** et en fait ça va permettre ; ça va s'alimenter
en fait c'est ça qui pourrait être intéressant **donc il faut
combattre l'antisémitisme pour lui-même mais en l'incluant dans un combat
général contre tous les racismes** en fait

**et en refusant la tendance à le minimiser ou à minimiser les autres
formes de racisme** c'est-à-dire il y a pas de hiérarchie entre les différentes
formes de racisme

et aussi évidemment refuser qu'il y ait des discours  racistes au nom de
la lutte contre l'antisémitisme type discours du  "nouvel antisémitisme"
qu'on a développé précédemment

et évidemment on cessant aussi de subordonner la lutte contre l'antisémitisme
à la question Israël et palestinienne

- https://youtu.be/tYWRnqNL_dk?t=4100

parce que la condition des Juives et des Juifs en France c'est une condition
minoritaire dans un pays ben on a cessé de le dire là pendant cette heure
et demie mais **dans un pays qui possède une longue tradition antisémite**
et qui date de bien avant le sionisme et la constitution de l'État d'Israël
donc quelle que soit la relation avec cetat, son gouvernement, sa politique,
**les Juifs de France ils subissent l'antisémitisme structurel de la
société française en fait**

Sender sur le travail de mémoire et travail d'éducation
-----------------------------------------------------------

- https://youtu.be/tYWRnqNL_dk?t=4121

ben oui ; et puis après j'aimerais bien rajouter juste pour le
petit moment un peu conclusion, solution ; le travail de mémoire en fait
**travail de mémoire et travail d'éducation** autour de ce sujet-là

tu vois ; enfin là je vais me permettre mais c'est vraiment je vise personne
j'ai même pas les noms j'avais vu à un moment donné un bouquin dans une librairie ;
c'est une anecdote hein qui s'appelait "entrer en pédagogie antiraciste"
super beau bouquin une belle couverture et tout hein je me disais oh cool et tout
et je l'ai feuilleté donc je l'ai pas lu et cetera j'ai juste feuilleté parce
que je fais ça généralement avec les livres qui parlent d'antiracisme parce
que je suis un petit curieux **je vois si le mot Juif apparaît ou si le mot
antisémitisme apparaît et si il y est en fait tout court**

**et généralement il y est pas** c'est pas pour dire les gens qui ont écrit
ce livre machin truc s'en foute machin ; c'est juste que c'est je pense
que **c'est illustratif d'un truc aujourd'hui où on considère que la lutte
contre l'antisémitisme elle fait pas vraiment partie de la lutte antiraciste**

Sender : il y a la lutte antiraciste et puis après il y a la droite qui instrumentalise la lutte contre l'antisémitisme
--------------------------------------------------------------------------------------------------------------------------

- https://youtu.be/tYWRnqNL_dk?t=4186

**Il y a la lutte antiraciste et puis après il y a la droite qui instrumentalise
la lutte contre l'antisémitisme** et en fait tout ce délire autour de
enfin pas c'est pas un délire parce que la droite instrumentalise réellement
l'antisémitisme mais de prendre le contrepied de ça et de se dire en fait
vu que la droite instrumentalise tout le temps nous on va partir du
principe qu'en fait tout ce qu'elle dit est faux et donc du coup quand
elle dit que l'antisémitisme existe en fait il existe pas

parce qu'après les gens de gauche ils vont pas ils vont dire oui l'antisémitisme
il existe et la genre oui mais vous le vous dites que ça vous dites juste
l'antisémitisme existe oui

mais en même temps il existe pas parce que truc ; et en fait il y a jamais
rien de fait autour de l'antisémitisme qui existe ; il y a pas vraiment de ;
surtout dans une période de confusion pareille où c'est aussi compliqué
de  discuter en fait

et de discuter autour de ces notions là antisémitisme/antisionisme on
va pas rentrer là-dedans ici parce que ça va être un peu long et que
ça et que que ce serait pas forcément le l'objet de là

l'antisémitisme en le travaillant de manière un peu analytique en regardant
l'histoire, en essayant de comprendre un petit peu justement cette espèce
d'essence de l'antisémitisme qui le rend spécifique ; qui le rend spécifique
c'est comme ça qu'on peut réussir à le détecter

.. _sander_whistle_2024_02_25:

Sender : il faut apprendre à détecter l'antisémitisme, sur les dog whistle
-------------------------------------------------------------------------------

Moi je pense que c'est pour ça qu'il faut faire un travail de mémoire
et d'éducation c'est ; faut apprendre à détecter l'antisémitisme il faut
apprendre à le voir là où il se cache parce que l'antisémitisme se cache ;
à une époque on était fier d'être antisémite c'était pas un problème il se
mettait des pins et ils buvaient des bières ensemble

aujourd'hui tu peux plus faire ça parce que c'est
répréhensible par la loi, parce que c'est beaucoup moins accepté qu'avant,
donc du coup il faut faire autrement principe des :term:`dog whistle <antisem:dog whistle>`
c'est le principe des messages codés mais ça il faut apprendre maintenant
à les décoder

et c'est pas en apprenant par coeur les :term:`dog whistle <antisem:dog whistle>`
qu'on apprend à les décoder c'est en apprenant à voir ce qu'il y a derrière

quand Marine Le Pen donne une interview je crois à "Valeurs Actuelles" si je dis pas de
bêtises il y a longtemps mais quelques années quoi et que la couverture c'est
sur où elle dit en fait c'est je sais plus comment elle s'appelle la société
de Soros du truc des droits humains là, la Fondation je sais pas quoi que
en fait il y a un plan de cette fondation pour financer les droits LGBT et
des minorités pour la subversion migratoire truc ; avec une photo en une d'un
globe à moitié dans l'ombre

et t'es là bon oui c'est ça l'Open Society et oui c'était un article qui
expliquait qu'open Society c'était le complot
personnel de George Soros pour éradiquer en gros, plus ou moins les lancs, en
les remplaçant par une affreuse communauté de dégénérés métis je sais
pas quoi mais avec le globe dans l'ombre et en fait effectivement ça si
tu as jamais de ta vie vu des caricatures antisémites et si tu ne sais pas du
tout que en fait **cette image vraiment d'un globe qui est pris dans l'ombre,
l'ombre de quelque chose qui est plus grand que le globe enfin tout pointe vers
l'antisémitisme** même si il y a pas écrit "les Juifs, les Juifs"
il y a pas de croix gammée, il y a même pas d'étoile de David ;

c'est juste caché mais c'est là et c'est pour ça qu'il faut travailler
dessus faut comprendre un petit peu pareil

Léa
    pour déconstruire quoi

ouais moi pour ajouter juste un dernier truc sur la mémoire de la Shoah
pourquoi c'est important de faire un travail de mémoire aussi c'est que
ok ce qui se passe en ce moment je veux dire c'est c'est terrible c'est
pas moi qui suis en train de dire le contraire que c'est acceptable
ce qui se passe en moment au proche Orient mais les comparaisons
avec la Shoah c'est juste ; c'est en fait c'est très ; faut pas comparer
la Shoah autre chose que la Shoah ; comme on peut pas comparer l'esclavage
à La Shoah ça aurait pas de sens

on peut pas comparer des crimes les uns aux autres et la comparaison
Juif/nazi qu'on peut entendre à des moments donnés alors bon pour discuter
de si c'est par rapport à la politique et cetera machin mais même à ce
niveau-là en fait ; c'est encore une fois c'est donné un pouvoir bien
trop important dans le retour de stigmate en fait c'est pas c'est pas ;

.. _sander_shoah_2024_02_25:

Sender : il faut qu'on commence à revoir des bases au niveau de l'histoire de la Shoah
---------------------------------------------------------------------------------------------

- https://youtu.be/tYWRnqNL_dk?t=4455

Moi je pense que vraiment il faut qu'on commence à revoir des bases au
niveau de l'histoire de la Shoah et la mémoire de la Shoah parce que ça
se dilue ;
pourtant les derniers survivants et survivantes sont malheureusement en
train de nous quittter ; il y a un gros travail sur le enfin sur la
mémoire qu'on va devoir faire là-dessus ; les descendants et descendantes

et puis les personnes qui auraient envie de parler de ça et ça va être un vrai
enjeu maintenant parce que la désinformation, parce que le complotisme, parce
que les négationnismes, n'a pas disparu l'antisémitisme de manière générale

ça veut pas rien dire "plus jamais ça" ; donc du coup il y a quand même
un travail à faire là-dessus de pas oublier ce qui s'est passé, de pourquoi
c'est arrivé et de comment est-ce que ça pourrait réarriver sous une
forme ou une autre quoi

enfin je me suis un peu perdu là-dessus ou


.. _lea_antisem_2024_02_02:

Léa il faut vraiment que la gauche se réapproprie la lutte contre l'antisémitisme
-------------------------------------------------------------------------------------

Bon ça y est moi c'était un peu le mot de la fin de dire que voilà il faut
que il faut vraiment que la gauche se réapproprie la lutte contre l'antisémitisme
parce que là et on l'a encore vu récemment **elle s'est égarée** et il est hors
de question que ce soit un autre enfin une autre tranche politique qui se
qui s'approprie cette lutte en fait

voilà donc à la gauche réveillez-vous ! écoutez les concernés voilà


.. note:: Voir :ref:`raar_2023:leftrenewal_fr_2023_12_10`

Liens sur leftrenewal (traduction française par Jonas Pardo)
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- :ref:`raar_2023:leftrenewal_fr_2023_12_10`

Sender
-----------

Et pour appuyer juste par rapport à ça non seulement laissez pas
l'extrême droite s'engouffrer dans le boulevard absent **enfin le
boulevard vide de la lutte contre l'antisémitisme** mais aussi parce
que l'extrême droite ne va jamais lutter contre l'antisémitisme
l'extrême droite va juste dire qu'elle lutte contre l'antisémitisme
mais elle ne fera évidemment jamais rien contre l'antisémitisme
puisqu'il faudrait qu'elle s'autodissolve donc c'est pas possible
donc la vraie ; en fait la véritable lutte contre l'antisémitisme, celle
qui est pas juste un terme creux où on met n'importe quoi dedans

mais la lutte contre le phénomène qu'on appelle l'antisémitisme, sous
toutes ses formes, d'où qu'il vienne, là il y a personne dedans, en vrai,
enfin à part des militants et militantes mais faut faut s'occuper de ce
terrain là aussi en fait ; c'est votre rôle.

- https://youtu.be/tYWRnqNL_dk?t=4605

Martin Eden
    je voudrais bien qu'il se dissolve tout seul c'est il ??faudra?? bien du
    travail et je pense que je finirai sur une blague juive

    **Qu'est-ce qu'un antisémite sinon quelqu'un qui déteste les Juifs
    plus que la normale** voilà

    donc je pense que c'est une bonne définition voilà je pense
    que

Léa
    c'est la définition d'un humoriste américain aussi qui dit ça


ah bon ok je sais plus je crois que je sais plus qui avait cette
blague enfin

c'était en tout cas Martin Eden et JJR en direct du wokistan
bisous à vous


Sender
    merci

Léa
    au revoir merci


