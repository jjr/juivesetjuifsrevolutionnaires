

.. _jjr_greve_2024_02_25:

=========================================================================
2024-02-25 **Il y a 83 ans : la grève de février 1941**
=========================================================================

- https://juivesetjuifsrevolutionnaires.wordpress.com/2024/02/25/il-y-a-83-ans-la-greve-de-fevrier/


Le 25 février 1941 avait lieu un évènement peu connu de l’histoire de
la Seconde Guerre mondiale, qu’il nous semble important de commémorer :
une grève générale de deux jours aux Pays-Bas pour protester contre la
déportation des Juifs et des Juives néerlandais·es.

Au début de l’année 1941, les nazis du NSB multiplient les provocations
et les attaques antisémites contre l’importante communauté juive
d’Amsterdam. Face à ces agressions, certain·es Juif·ves décident de
se défendre. Le 11 février, un nazi est grièvement blessé dans une rixe,
il meurt trois jours plus tard.

Les représailles ne se font pas attendre : les occupants isolent le quartier
juif, le 23 février ils raflent et déportent 425 Juifs.
L’immense majorité d’entre eux ne reviendront pas.

**Cas rare en Europe, les crimes nazis contre les Juifs entrainèrent une réaction
de masse dans la population**.

Le parti communiste néerlandais, alors dans la clandestinité, appelle à la grève.
La classe ouvrière se mobilise : les usines, le ramassage des déchets, le port,
le tramway, etc. s’arrêtent et des rassemblements sont organisés en solidarité
avec les déportés.
Selon certaines estimations, ce sont près de 300 000 travailleurs et travailleuses
qui auraient participé au mouvement de grève.

La répression nazie est féroce : neuf personnes sont assassinées dans
la répression des manifestation, dix-huit sont fusillées dans les jours
qui suivent.

A Amsterdam, une statue commémore ces évènements.

Souvenons nous de toutes les victimes des crimes nazis. Puisse la mémoire de
celles et ceux qui s’y sont opposé·es au péril de leur vie nous inspirer
dans nos combats.
