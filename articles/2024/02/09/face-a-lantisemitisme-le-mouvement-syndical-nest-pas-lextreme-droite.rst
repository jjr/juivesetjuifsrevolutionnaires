


.. _jjr_2024_02_09:

============================================================================================
2024-02-09 **Face à l’antisémitisme, le mouvement syndical n’est pas l’extrême-droite**
============================================================================================

- https://juivesetjuifsrevolutionnaires.wordpress.com/2024/02/09/face-a-lantisemitisme-le-mouvement-syndical-nest-pas-lextreme-droite/

**La lutte contre l’antisémitisme à l’Université est nécessaire**.

Elle n’est ni soluble dans l’islamophobie, ni dans des amalgames confus entre
mouvement syndical de lutte et extrême-droite.

L’explosion des propos et actes antisémites en France depuis le 7 octobre
n’a pas épargné les universités. De très nombreux et nombreuses étudiants
et étudiantes juif·ves ont été confronté·es à des propos antisémites,
mais aussi à du harcèlement voire parfois à des agressions physiques. Les
conséquences ont été pour beaucoup une détresse psychologique qui s’est
traduite par une vague de déscolarisation. Une tentative de suicide a également
eu lieu.

Face à cette situation, les réactions des autorités universitaires comme
des organisations syndicales étudiantes n’ont souvent pas été à la hauteur.

Le 6 février 2024, JJR devait intervenir dans une conférence à l’Université
Paris Cité, dans le cadre d’une conférence sur l’antisémitisme.

Deux autres intervenants étaient prévus, le collectif Golem, avec lequel JJR
entretient des liens, et l’UEJF, avec **lequel nous avons des désaccords de
fond quant à la stratégie de lutte contre l’antisémitisme**.

Nous souhaitions exprimer à cette conférence ce qui a toujours fait notre
ligne : la nécessité de prendre à bras le corps, sans faux fuyant, la lutte
contre l’antisémitisme, d’où qu’il vienne; le refus que la lutte contre
l’antisémitisme soit soluble dans l’islamophobie ou soit réduite à un
avatar du conflit israélo-palestinien **(en cela nous rejetons l’approche de
la direction de l’UEJF depuis la période Madar)**.

Notre intervenante ayant eu un empêchement de dernière minute, nous n’avons
finalement pas pu porter notre voix dans cette conférence et y développer
ces éléments.

Aujourd’hui, c’est un autre désaccord de fond avec la direction de l’UEJF
que nous devons exprimer : celui qui consiste, dans le cadre des élections au
CROUS, à renvoyer dos à dos l’organisation d’extrême droite La Cocarde
et les syndicats Solidaires Étudiant·es.

L’antisémitisme fait partie du cœur idéologique de l’extrême droite
et non de la gauche. Si cette dernière n’est pas immunisée par nature
contre l’antisémitisme qui influence toute la société, et qu’il
faut y mener un travail permanent de lutte antiraciste, renvoyer dos à dos
une organisation fasciste et une fédération syndicale étudiante est une
erreur politique majeure. Car, si le traitement de l’antisémitisme reste
insuffisant au sein de certains syndicats Solidaires Étudiant·es — et
si au moins l’un d’entre eux, à l’EHESS, a pu exprimer des positions
que nous considérons comme inacceptables au lendemain du 7 octobre —, une
majorité des syndicats Solidaires Étudiant·es sont un appui dans la lutte
contre l’antisémitisme. L’organisation la Cocarde est quant à elle une
organisation antisémite militante qui ne sera jamais un allié des Juifs et
des Juives.

Nous dénonçons donc cet amalgame qui, loin de participer à la lutte contre
l’antisémitisme, ne fait qu’entretenir la confusion. La lutte contre
l’antisémitisme à l’Université mérite mieux !


.. figure:: images/affiche.webp

   Un des visuels émis par l’UEJF assimilant Solidaires Etudiant.es et la Cocarde
