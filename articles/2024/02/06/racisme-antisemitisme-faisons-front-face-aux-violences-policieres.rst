.. index::
   ! Racisme, antisémitisme : faisons front face aux violences policières !

.. _jjr_2024_02_06_police:

===========================================================================================
2024-02-06 **Racisme, antisémitisme : faisons front face aux violences policières !**
===========================================================================================

- https://juivesetjuifsrevolutionnaires.wordpress.com/2024/02/06/racisme-antisemitisme-faisons-front-face-aux-violences-policieres/

Nous apprenons par les récentes révélations de Mediapart le traitement
dégradant, inhumain, à caractère sexiste et antisémite, infligé à une
femme juive orthodoxe de 67 ans, aux mains de la police nationale.

Le 8 juin 2023, cette femme est interpellée par la police pour avoir embouti
avec sa voiture la moto d’un policier pendant un contrôle, apeurée par
les armes pointées sur elle. Elle est conduite au commissariat de Créteil.

Sur les images des policiers, on la voit menottée au banc et étendue par
terre au milieu de huit policier.es, dont sept hommes et une femme, au mieux
indifférent.es, au pire moqueurs à son état de panique.

**Elle leur dit qu’elle est juive et les supplie de lui rendre sa perruque qui a glissé
pendant qu’un policier l’empoigne pour la lever**. Les femmes juives orthodoxes
mariées ont en effet l’obligation religieuse de se couvrir les cheveux.

Elle subit des moqueries face à ses cris de douleur et de choc, mais aussi
les entraves physiques de deux policiers qui lui agrippent les bras et les
jambes, ainsi qu’un coup de genou dans le dos dont elle témoigne. Elle
perd connaissance, est soulevée et secouée sans ménagement, tandis que
les policiers continuent leurs railleries. Ils finissent par appeler les
pompiers. Elle est emmenée une heure aux urgences, sans être examinée. Le
lendemain, son médecin traitant établit un certificat médical faisant état
« des contusions et hématomes aux poignets, à la face interne des bras,
sur les genoux, à la fosse lombaire droite, à la cuisse droite, au niveau
des fesses et un état de choc psychologique ».

Pour citer la victime « Que ce soit une femme juive, arabe ou toute femme qui
tient à un vêtement ou a une attitude liée à sa religion. Tout le monde
a droit au respect. » Nous la remercions de son témoignage courageux et
lui adressons tout notre soutien. Nous dénonçons tous les propos visant à
nier le caractère antisémite de ces violences, ou bien qui évoquent un «
privilège juif » et un « deux poids deux mesures » que l’on ne connait
que trop bien quand on parle d’antisémitisme. La mise en concurrence des
racismes ne profite qu’à l’extrême-droite.

Ces dernières années, plusieurs affaires ont mis en lumière la pénétration
profonde des idées d’extrême droite et de la violence raciste dans la
police. La liste des victimes de violences policières s’allonge chaque
année et cible principalement les minorités. Le traitement dégradant et
les insultes racistes visant les populations des quartiers populaires sont
monnaie courante et sont confirmées par les sondages d’opinion au sein de
la police. En mai 2021, une étude du Cevipof a montré que 60 % des policiers
étaient prêts à voter pour Marine Le Pen au prochain scrutin présidentiel.

En 2020, la découverte de plusieurs groupes de policiers, sur des réseaux
sociaux, avaient permis de mesurer à quel point le racisme pouvait s’exprimer
sans aucune gêne entre agents. Comme dans ce groupe Facebook regroupant huit
mille policiers. Les plus radicaux se retrouvant dans des groupes de discussion
privées, comme ces policiers de Rouen qui n’hésitent pas à parler de Noirs,
des Arabes et des Juifs ainsi : « Je n’attends qu’une chose, c’est que
tous ces gens crèvent. […] Ça régénérera l’espèce humaine et surtout
la race blanche. » Il faudra quatre ans pour que tous les mis en cause soient
finalement révoqués. Nous sommes en droit de nous demander combien de groupes
existent encore, dans lesquels les esprits se préparent à la violence raciste.

De même, la présence de néo-nazis, parfois décomplexés, dans l’armée,
la gendarmerie et la police s’est traduite dans plusieurs affaires, dans
lesquelles ils ont souvent été protégés par leur hiérarchie, et en tout
cas peu sanctionnés. Ce sont souvent les victimes mêmes ou les lanceurs et
lanceuses d’alerte qui ont dû démissionner ou qui ont été placardisé·es :

- à l’été 2014, un policier tente d’alerter ses supérieurs après
  avoir découvert des inscriptions nazies en page d’accueil d’un ordinateur
  de service : « Vive le IIIe Reich » ; « Heil Hitler hihi ».

- à Amiens en 2008, trois agents de la Bac (dont un est marié à une
  élue FN) effectuent des saluts nazis dans un bar. Un seul d’entre eux sera
  condamné et révoqué, les autres ont été mutés.

- en 2014, un policier, qui raconte à ses collègues que « Le monde du
  show-biz, des banquiers, tous des juifs » etqui arborait un symbole SS sur
  son casque est exclu seulement quinze jours. Quelques années après, il a
  été promu brigadier-chef.

- en février 2021, c’est un CRS juif qui retrouve son casier tagué :
  « Sale juif » et deux croix gammées.

- en juin 2021, c’est une croix gammée géante qui a été dessinée
  sur le toit de la maison d’un policier, par celui-ci et son père, fiers de
  leur idéologie.

Afin de dresser un tableau plus complet de la réalité d’un antisémitisme
qui se répand dans les institutions, il faut mentionner les nombreuses photos
de policiers, de gendarmes et de militaires avec des tatouages représentant
des symboles utilisés par l’extrême droite la plus violente, empruntant
même à l’imagerie néo-nazie, qui circulent en ligne. On pourrait aussi
mentionner les autres membres des forces de répression effectuant le geste de
la quenelle, avec le visage flouté qui ont largement tourné sur les réseaux
sociaux, parfois même fièrement.

L’antisémitisme policier est l’un des aspects de l’expression du
racisme dans la police, qui touche au quotidien les habitantes et habitants des
quartiers populaires et les minorités noires, arabes, turques, asiatiques,
rroms, qui subissent contrôles d’identités arbitraires, harcèlement
raciste, violences et meurtres impunis et couverts par l’Etat. Certains,
comme Gérard Darmanin, prétendent que la « haine du flic » serait liée
à la « haine du juif ». L’histoire antisémite et raciste de la Police
française montre bien au contraire que la « haine du juif » existe bel et
bien dans la police et n’appartient pas seulement au passé. Elle s’articule
avec la haine raciste que subissent les autres minorités. Plus que jamais,
face aux violences policières, à l’antisémitisme comme à toutes les
autres formes de racisme, faisons front !
