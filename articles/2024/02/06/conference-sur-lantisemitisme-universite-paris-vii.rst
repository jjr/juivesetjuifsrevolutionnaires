

.. _jjr_2024_02_06_conference:

=======================================================================================================
2024-02-06 **Conférence sur l’antisémitisme – Université Paris VII** ave Golem, UEJF et JJR (ANNULEE)
=======================================================================================================

- https://juivesetjuifsrevolutionnaires.wordpress.com/2024/01/31/cqfd-lantisemitisme-cest-du-racisme/


Conférence annulée
====================

.. warning::
   La conférence a été annulée

   Voir https://juivesetjuifsrevolutionnaires.wordpress.com/2024/02/09/face-a-lantisemitisme-le-mouvement-syndical-nest-pas-lextreme-droite/

.. figure:: images/affiche.webp

Conférence sur l’antisémitisme – Université Paris VII
=========================================================

L’université n’est pas épargnée par la montée de l’antisémitisme.

Depuis le 7 octobre 2023 des étudiant-es juifves sont harcelé-es, sont
obligé-es de cacher leur identité ou ont arrêté d’aller en cours.

Face aux voix qui s’élèvent pour minimiser ou relativiser l’antisémitisme,
nous organisons cette conférence pour apporter aux étudiant-es juifves
tout notre soutien.

**Nous vous croyons et vous n’êtes pas seul-es**.

**Nous n’accepterons jamais l’antisémitisme, nous ne nous habituerons
jamais et nous lutterons toujours contre les antisémites d’où qu’ils viennent !**

- Qu’est-ce que l’antisémitisme ?
- Quelle forme prend-il et comment y faire face ?
- Quelles sont les aides disponibles pour les étudiant-es Juifves ?

Nous en discuterons ensemble le mardi 6 février 2024 à 18h dans
l’amphi 10E avec l’UEJF Paris Cité, le collectif Golem et Juives et Juifs Révolutionnaires.

Inscription obligatoire : lorenzo.leschi@gmail.com

.. figure:: images/plan_campus.webp

.. raw:: html

   <iframe width="800" height="400" src="https://www.openstreetmap.org/export/embed.html?bbox=2.3772954940795903%2C48.8282688833962%2C2.3843765258789062%2C48.83113990556294&amp;layer=mapnik"
       style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=18/48.82970/2.38084">Afficher une carte plus grande</a></small>


Message mastodon
==================

- https://kolektiva.social/@jjr/111849748153452236
