.. index::
   ! Front populaire contre l’extrême-droite !

.. _jjr_2024_06_11:

=========================================================================
2024-06-11 **Front populaire contre l’extrême-droite !**
=========================================================================

- https://juivesetjuifsrevolutionnaires.wordpress.com/2024/06/11/front-populaire-contre-lextreme-droite/

**Se mobiliser, ne pas faire de chèque en blanc, construire un mouvement social
fort qui impose de véritables conquêtes sociales**.

Le macronisme est discrédité. L’extrême droite est aux portes du pouvoir.

Elle a bénéficié d’une exposition médiatique sans précédent, signe qu’une
fraction grandissante de la bourgeoisie lui apporte son soutien.
Emmanuel Macron a choisi de dissoudre l’assemblée dans un contexte où la gauche
est plus que jamais divisée et le mouvement social affaibli sur la base
d’un pari honteux :

- Pile, tenter de se présenter comme l’ultime recours face au RN
- Face, offrir sur un plateau le pouvoir au RN en pensant l’affaiblir par l’usure du pouvoir

L’arrivée de l’extrême droite serait une catastrophe pour les travailleuses,
les travailleurs, les femmes, les LGBTQI, les ultramarin⸱es, les migrant⸱es,
etc.

Elle aurait aussi de graves conséquences internationales, notamment en ce
qui concerne le soutien nécessaire à apporter aux peuples ukrainien, syrien
et palestinien.

Celles et ceux qui dans la minorité juive pensent que son antisémitisme appartient
au passé se trompent lourdement : même s’il est moins assumé publiquement,
l’antisémitisme fait partie de son héritage, reste central dans sa vision
du monde complotiste et fait partie intégrante de son projet politique qui
consiste à la répression de toutes les minorités.

Une fois au pouvoir, les masques ne pourront que tomber et il sera trop tard
pour faire marche arrière.

La seule alternative, c’est l’unité du mouvement social et l’union de la gauche,
sans exclusive, sur un programme rassembleur, autour d’une perspective de
progrès social.

Pour faire advenir cela, il faut cesser d’alimenter des politiques de division,
et notamment assumer un antiracisme conséquent qui prennent en charge réellement
tant la lutte contre l’antisémitisme que celle contre l’islamophobie, qui
cesse de mettre en opposition lutte contre l’antisémitisme et solidarité
avec la Palestine et revendication du cessez-le-feu.

Mais un tel front ne réalisera rien par lui-même.

S’il est une condition pour barrer la route à l’accession programmée de l’extrême
droite au pouvoir le 7 juillet 2024, il ne peut être une fin en soi ni nous
amener à nous bercer d’illusions sur des fausses perspectives électorales.

Plutôt qu’un chèque en blanc à de futurs élu⸱es qui, sans pression sociale,
pourraient vite oublier les aspirations des classes populaires, des femmes
et des minorités, il nous faut construire de puissants contre-pouvoirs, qui,
à la manière de la grève générale de 1936, mettent en échec les forces
réactionnaires et contraignent le patronat et le futur gouvernement à la
mise en œuvre de véritables conquêtes sociales.

Le renforcement de tels contre-pouvoirs est aussi une urgence vitale pour
faire face à toutes les éventualités à partir de juillet.

En cas de victoire du Rassemblement National, nous en aurons besoin pour nous
défendre et pour résister dans la rue, dans les entreprises, dans les universités,
et partout ailleurs.

**Contre l’extrême-droite, organisons nous !
Syndiquons-nous !
Construisons des collectifs de lutte !**
