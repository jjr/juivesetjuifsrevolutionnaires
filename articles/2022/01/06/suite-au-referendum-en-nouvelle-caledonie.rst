.. index::
   pair: Nouvelle Calédonie; Suite au référendum en Nouvelle Calédonie (2024-01-06)

.. _jjr_2022_01_06:

=========================================================================
2022-01-06 **Suite au référendum en Nouvelle Calédonie**
=========================================================================

- https://juivesetjuifsrevolutionnaires.wordpress.com/2022/01/06/suite-au-referendum-en-nouvelle-caledonie/

Suite au référendum en Nouvelle Calédonie
=============================================

Le troisième référendum concernant l’indépendance du Kanaky/Nouvelle-Calédonie
avait lieu le 12 décembre 2021.

Ce qui aurait dû être l’aboutissement des accords de Nouméa c’est déroulé
sans la participation des forces indépendantistes.

Ces dernières demandaient un report du scrutin en raison de la COVID et des
périodes de deuil de la communauté kanake, qui ne permettaient pas à une grande
partie de celle-ci de participer au scrutin.

Le gouvernement a refusé cette demande, pourtant raisonnable, elles ont donc
appelé la population à ne pas participer au scrutin.
Cet appel au boycott a été très suivi, ce qui montre à nouveau, s’il en était
besoin, la forte audience des organisations indépendantistes, notamment au
sein de la province Nord.
Les loyalistes seuls ayant donc participé, le non l’a emporté sans surprise et
avec une très forte abstention.

Les accords de Nouméa prévoyaient en 1998 une meilleure prise en compte de
l’identité kanake et reconnaissaient la légitimité des Kanaks comme des
nouvelles populations.


La tenue d’un référendum sans tenir compte de la population kanake est donc un grave retour en arrière
-------------------------------------------------------------------------------------------------------

La tenue d’un référendum sans tenir compte de la population kanake est donc
un grave retour en arrière qui met en danger le processus de paix à l’œuvre
depuis 1988.

Cette situation est également représentative de l’attitude coloniale de la
France vis-à-vis du Kanaky/Nouvelle Calédonie et des autres territoires encore
sous domination française.

Dans ces derniers, les spécificités linguistiques et culturelles des peuples
autochtones ne sont quasiment jamais reconnues, sinon de manière paternaliste
ou comme excuse devant justifier les carences de l’État.
On considère généralement que l’État doit s’adapter à la population, mais au
Kanaky/Nouvelle Calédonie, en Guadeloupe, ou en Guyane l’inverse devrait
avoir lieu, et dans le cas présent les Kanaks auraient dû abandonner leurs
rituels de deuil s’ils voulaient voter.

Ce mépris des populations autochtones au profit d’un agenda fixé en métropole est profondément colonialiste
--------------------------------------------------------------------------------------------------------------

Ce mépris des populations autochtones au profit d’un agenda fixé en métropole
est profondément colonialiste.

Il risque d’avoir pour conséquence un renforcement des fractures existantes
entre loyalistes et indépendantistes, entre Kanaks et nouvelles populations,
bien loin des ambitions affichées par les accords de Nouméa et dans la lignée
des colonialismes du 19e et du 20e siècle, qui jouaient sur les divisions
ethniques afin de maintenir leurs dominations.

Nous ne pouvons que condamner un tel mépris et **affirmer notre opposition à
tous les colonialismes**, à commencer par celui mis en œuvre par notre propre
pays.

Quelle que soit la forme qu’à l’avenir les populations du Kanaky/Nouvelle Calédonie
décideront de donner à leur relation avec la France, l’avenir de cette terre
ne pourra se faire en ignorant le peuple kanak et les larges parts de la
population qui souhaitent l’indépendance.

Non au colonialisme !
Oui au droit des peuples à l’autodétermination !
Soutien au peuple kanak !


Liens
========

- :ref:`jjr_2024_05_15`
