.. index::
   pair: Houria Bouteldja ; Contre l’antisémitisme, la politique du PIR(e) ? (2015-04-27)

.. _jjr_pir_2015_04_27:

=========================================================================
2015-04-27 **Contre l’antisémitisme, la politique du PIR(e) ?**
=========================================================================

- https://juivesetjuifsrevolutionnaires.wordpress.com/2015/04/27/contre-lantisemitisme-la-politique-du-pire/
- :ref:`antisem:houria_bouteldja`


Introduction
================

**Notre groupe s’est créé en raison d’un manque de prise en compte
de l’antisémitisme dans le milieu révolutionnaire**.

Celui-ci a été particulièrement flagrant lors des attaques meurtrières ayant visées
des Juifs et Juives de France ces dernières années, suite auxquelles une
majorité de nos organisations sont restées silencieuses, ne considérant
manifestement pas ces attaques comme ce qu’elles sont : des meurtres
racistes.

Ce constat nous a amené à nous interroger sur l’auto-défense des
minorités nationales et à nous organiser pour lutter par nous-même contre
le racisme. Particulièrement vigilants à l’égard des thèses qui peuvent
circuler dans notre milieu politique, nous ne pouvons rester sans réaction
face au texte de l’allocution d’Houria Bouteldja du 3 mars 2015 consacré
au racisme et au « philosémitisme d’État ».

Une fois l’introduction passée, affirmant que l’antiracisme en France
est structuré par la Shoah, ce qui est au moins incomplet (en ce qui concerne
l’Éducation Nationale cela peut être le cas, mais la récupération de la
Marche pour l’Égalité par SOS Racisme nous semble également un moment
majeur de la création d’un antiracisme « institutionnel »), Houria
Bouteldja en vient au cœur du sujet : la situation des Juifs et Juives de
France.

Le commencement du texte est extrêmement juste, évoquant trois
« lapsus» étatiques révélant une différenciation de la population
juive permettant de structurer en creux la création d’une identité
majoritaire blanche, européenne, et chrétienne.

Est affirmé ensuite, également avec justesse, que la réaction de l’État suite aux attaques de
janvier a été de mener l’offensive contre les Musulman-e-s (nous parlons
de « Musulman-e-s » avec une majuscule, car il s’agit de tous ceux qui,
sans être forcément religieusement musulman-e-s, sont renvoyés à cette
identité ou se définissent comme tels), ceux-ci faisant depuis des années
office de bouc émissaire.

Continuant sur sa lancée, Houria Bouteldja dénonce le manque de clarté de
la gauche face au racisme.
Celle-ci rencontre en effet des difficultés à le considérer comme structurel
à la société française, ce qui l’amène à une fausse contradiction entre
lutte contre l’islamophobie et lutte contre l’antisémitisme.


Jusque-là, nous ne pouvons qu’être plutôt en accord avec Houria Bouteldja, mais les choses se gâtent rapidement
=================================================================================================================

Jusque-là, nous ne pouvons qu’être plutôt en accord avec Houria Bouteldja,
mais les choses se gâtent rapidement.

D’abord, contredisant le constat fait plus haut que l’antisémitisme comme
l’islamophobie sont nécessaires à la définition d’une identité majoritaire,
elle refuse toute « fausse symétrie » qui consisterait à se positionner
à la fois contre l’un et contre l’autre.

Pourtant, si l’on part de ce constat, le moyen le plus
évident de combattre le racisme serait de briser cette fonction sociale, et
cela ne peut être fait du point d’une seule minorité (auquel cas on est
dans la demande d’intégration) mais dans la coordination des différents
fronts antiracistes.

Ayant démontré que l’antisémitisme était structurel, Houria Bouteldja
change brutalement son fusil d’épaule. En effet, l’antisémitisme ne
viendrait pas de l’État. Dans un autre registre, l’exemple du sexisme nous
prouve pourtant que la multiplication de discours étatiques, médiatiques,
ou politiques pour condamner une discrimination n’équivaut pas à une lutte
réelle et efficace contre celle-ci, et encore moins à sa disparation.

Pas plus que de l’État, l’antisémitisme selon Houria Bouteldja ne vient
que marginalement de la majorité nationale ou de l’extrême droite
«classique » (pour notre part, nous considérons que cet antisémitisme est
loin d’être marginal, surtout au regard des explications développées plus
haut), mais surtout des Musulman-e-s, considérés de manière homogène.

On est ici dans le calque d’un discours nationaliste classique
=================================================================

On est ici dans le calque d’un discours nationaliste classique, c’est-à-dire
niant toute contradiction de classe ou idéologique au profit d’une unité
nationale fantasmée (paradoxalement, Houria Bouteldja est ici bien plus proche
du nationalisme sioniste qu’elle ne veut bien l’admettre).
Mais justement, homogènes, ceux-ci ne le sont pas.

De même que la société dans son ensemble, les minorités ont une
histoire et sont traversées de contradictions sociales, de classe et
idéologiques.

L’antisémitisme qu’on peut trouver chez les Musulman-e-s est
à la fois la reproduction plus ou moins fidèle de celui (soi-disant marginal)
qu’on trouve dans la majorité nationale dont le côté pseudo anti-système
facilite l’implantation au sein du prolétariat ;

le produit d’une frange des Musulman-e-s qui le diffuse pour apporter du soutien extérieur à des
régimes et groupes réactionnaires d’Afrique du Nord et du Moyen-Orient
(dont elle est dépendante) ; et l’héritage d’une politique menée par la
France à l’époque coloniale visant à diviser les populations du Maghreb
entre Musulman-e-s et Juif-ve-s.

Face à elle, d’autres groupes sociaux existent et mènent un combat idéologique,
que ce soit la « beurgeoisie » en voie d’intégration institutionnelle,
les travailleurs Musulman-e-s qui s’organisent dans les syndicats, etc.

Il en est de même au sein de la minorité nationale juive
==========================================================

L’islamophobie et le racisme anti-arabe qu’on peut y trouver est à la fois
la reproduction plus ou moins fidèle de celui qu’on trouve dans la
majorité nationale ; le produit d’une frange des Juif-ve-s qui le diffuse
pour apporter un soutien extérieur au colonialisme israélien ;

et l’héritage d’une politique menée par la France à l’époque coloniale
visant à diviser les populations du Maghreb entre Musulman-e-s et Juif-ve-s.

Face à elle, d’autres groupes sociaux existent et mènent un combat idéologique,
que ce soit la bourgeoisie juive républicaine légitimiste, les travailleurs et
travailleuses Juif-ve-s qui s’organisent dans les syndicats, etc.

.. _bouteldja_2015_04_27:

La suite du discours d’Houria Bouteldja s’effondre d’elle-même
================================================================

- :ref:`antisem:houria_bouteldja`


Une fois posé ce point, **la suite du discours d’Houria Bouteldja s’effondre
d’elle-même**.

Refusant de voir cette histoire et ces contradictions, elle
en déduit que l’antisémitisme serait une conséquence du système raciste
à l’encontre des Musulman-e-s (seule explication restante si on nie les
contradictions internes à cette minorité sans tomber dans une explication
raciste-culturaliste).

Et explique que si cette réaction vise la minorité
juive spécifiquement, c’est en raison d’un traitement préférentiel que
l’État accorderait aux Juif-ve-s.

Suivant jusqu’au bout cette logique
faussée, la meilleure manière de lutter contre l’antisémitisme serait de
**lutter contre ce traitement (appelée ici « philosémitisme », on n’est
pas loin du « privilège juif »)**, notamment contre la « religion civile »
(ici aussi, on est n’est pas loin de la « pornographie mémorielle » ou
du « Shoah business ») que serait la Shoah.

Pour notre part, nous ne pensons pas qu’on lutte contre le racisme en
s’attaquant aux victimes de celui-ci.

Juifs et Juives révolutionnaires, nous entendons mener le combat sur
plusieurs fronts :

- **Contre l’antisémitisme, d’où qu’il vienne**
- **Contre tous les racismes**, aux côtés des autres minorités nationales,
  seul moyen de déconstruire e système raciste
-  **Contre les idéologies réactionnaires au sein de notre groupe national**
