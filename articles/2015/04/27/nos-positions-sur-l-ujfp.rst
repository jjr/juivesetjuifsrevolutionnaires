.. index::
   pair: JJR ; Nos positions sur l’UJFP (2023-04-27)

.. _jjr_ujfp_2015_04_27:

==================================================================================
2015-04-27 **Nos positions sur l’UJFP** (suite au texte sur Houria Bouteldja)
==================================================================================

- https://juivesetjuifsrevolutionnaires.wordpress.com/2015/04/27/nos-positions-sur-lujfp/
- :ref:`antisem:houria_bouteldja`
- :ref:`antisem:ujfp`

Introduction
================

Suite à notre texte concernant l’allocution d’Houria Bouteldja, il nous a
été demandé notre avis concernant l’UJFP. Cette organisation possédant une
certaine visibilité dans le milieu progressiste, il nous a semblé nécessaire
de clarifier notre position.

La création d’une organisation spécifique juive centrée sur la solidarité
avec les palestinienNEs telle que l’UJFP est historiquement une réaction
aux courants idéologiques sionistes qui présentent le sionisme comme
la seule expression politique légitime au sein de la minorité juive et
la seule identité juive possible.

Toute critique de l’Etat d’Israël ou du sionisme est ainsi disqualifiée
comme antisémite par essence.

Par ailleurs certains courants antisémites tentent d’identifier Juifs-ves,
sionistes, État et colonialisme israélien.

Des militants et militantes qui s’étaient jusque-là investiEs dans les
luttes anticoloniales sans nécessairement y mettre en avant leur identité
ou leur filiation juive (sans non plus nécessairement la nier ou la masquer
dans un cadre personnel), se sont sentiEs contraintEs de le faire dans
la logique du"pas en notre nom", en réponse aux discours des sionistes
d’une part et des antisémites d’autres part.

Cette démarche est louable et nous ne pouvons qu’être favorables à l’existence
d’organisations spécifiques juives soutenant le combat du peuple palestinien
contre l’oppression.

Elle représente cependant le constat d’un échec dans la lutte contre
l’antisémitisme.

En effet, elle entérine l’idée selon laquelle être Juive ou Juif c’est
nécessairement se positionner sur la question israélo-palestinienne, c’est
nécessairement être considéré à priori comme favorable au sionisme et donc
forcé de s’en dissocier explicitement.

Le sionisme, dans ses différentes variétés idéologiques, est une idéologie
très présente parmi les Juifs et les Juives de France.

Il a réussi à développer une influence idéologique majoritaire au sein
d’un grand nombre d’institutions communautaires.

Il ne mène pourtant ni à notre émancipation, ni à la coexistence pacifique des
populations vivant en Israël / Palestine, et ce quels que soient les courants
idéologiques qui s’en réclament (y compris les sionismes ouvriers).

Se revendiquant du peuple juif dans son ensemble, les différents courants du
sionisme, considérant l’antisémitisme comme inéluctable et présentant
Israël comme un refuge pour les Juifs et les Juives, cherchent à nous enrôler
dans un projet nationaliste, raciste et colonialiste.

Ces courants isolent ainsi la minorité juive, détournent l’aspiration à
l’autodéfense et empêchent une compréhension réelle de l’antisémitisme
et de son rôle dans la préservation du capitalisme, notamment en période
de crise.
En ce sens ils empêchent d’apporter une réponse efficace à l’antisémitisme.

Nous tenons à préciser qu’ici **nous parlons du sionisme selon ses conséquences
réelles, et non sur ses intentions**.
Pour cette raison il est nécessaire de le combattre au sein de notre minorité.

Comme révolutionnaires, nous pensons qu’il est nécessaire de soutenir
les oppriméEs face au colonialisme, en étant solidaires de celles et ceux
qui sont écraséEs par le colonialisme français comme des palestiniens et
des palestiniennes face au colonialisme israélien.


Se placer du côté des oppriméEs est la base d’une éthique juive et universaliste telle que nous la concevons
===============================================================================================================

**Se placer du côté des oppriméEs est la base d’une éthique juive et
universaliste telle que nous la concevons.**

Une démarche comme celle de l’UJFP, dans cette perspective, apparait
au premier abord intéréssante, puisqu’elle contredit la volonté
des organisations sionistes de faire taire toute critique de la politique
israélienne en l’assimilant à de l’antisémitisme.

Pour autant, pour nous, Juifs et Juives vivant en France, la nécessaire
solidarité avec les Palestiniens et les Palestiniennes ne doit pas devenir
un prétexte pour nier ou minimiser l’oppression antisémite.


**Juifs et juives vivant en France subissent quotidiennement la diffusion d’un antisémitisme de masse**
========================================================================================================

Juifs et juives vivant en France subissent quotidiennement **la diffusion d’un
antisémitisme de masse**.

Cette situation, loin d’être abstraite, correspond à notre vécu.

Cet antisémitisme se cache parfois sous le masque de la solidarité avec
la Palestine et peut transparaître même chez des gens dont nous partageons
d’autres combats.

C’est le cas lorsque ceux et celles qui dénoncent l’antisémitisme sont
immédiatement associéEs à des partisanEs du sionisme (le même mécanisme
est à l’oeuvre lorsque ceux et celles qui dénoncent le racisme contre les
MusulmanEs sont associéEs à des défenseurs ou à des défenseuses de
l’islam politique) ;

lorsque l’antisémitisme est condamné uniquement comme profitant au camp
sioniste, mais non en tant que racisme ; ou encore lorsqu’il est nié,
minimisé, ou considéré comme une invention de la propagande sioniste.


.. _ujfp_caution_2015_04_27:

L'UJFP prend le risque de servir de"caution juive" à tous les dérapages antisémites
==========================================================================================

Lorsqu’une organisation juive choisit de faire l’impasse sur cette réalité
(par exemple en minimisant l’antisémitisme pour faire pendant à
l’instrumentalisation de celui-ci par les courants sionistes), lorsqu’elle
nie l’antisémitisme de certains de ses alliés (le cas de René Balme est
ici criant), **elle prend le risque de servir de"caution juive" à tous
les dérapages antisémites**.

La mise en avant d’une personne ou d’un groupe issu du rang des raciséEs
pour légitimer un discours raciste est en effet une constante dans l’histoire
du racisme (pour la minorité juive, l’exemple le plus frappant est celui
de **la secte Neturei Karta, groupe juif fondamentaliste** qui soutient le régime
iranien et estime que la Shoah est une punition divine causée par le sionisme,
citée par tous les partisans de Dieudonné et de Soral).

La contradiction de l’UJFP découle notamment du fait qu’elle partage la
même erreur d’analyse concernant la nature de l’antisémitisme contemporain
que les courants sionistes et leurs alliés nationalistes français.

Elle y voit une"importation du conflit israélo-palestinien" alors qu’il
s’agit d’une production de l’idéologie nationale française et européenne,
mais aussi du pouvoir colonial français au Maghreb qui protége la bourgeoisie.

**Qu’il s’appuie opportunément sur ce conflit n’en est qu’un élément tactique,
certainement pas un élément essentiel**.


.. _ujfp_omega_2015_04_27:

2015-04-27 **L’UJFP fait du conflit israélo-palestinien l’alpha et l’oméga du  sort de la minorité juive en France**
===========================================================================================================================

L’UJFP s’est ainsi laissé entrainer sur le terrain même de ses adversaires
sionistes : **faire du conflit israélo-palestinien l’alpha et l’oméga du
sort de la minorité juive en France**.

**Une fois enfermé sur ce terrain, il ne lui reste que le déni ou la minimisation
de l’antisémitisme comme tactique pour faire face au discours sioniste**.

Nous ne pouvons espérer qu’une chose : c’est qu’elle rompe avec cet aveuglement
et qu’elle se reprenne.

Ceci dans l’intéret de la minorité juive, du combat antiraciste, et enfin
de la solidarité anticolonialiste, que les dérives antisémites comme la
complaisance vis à vis de l’antisémitisme ne font qu’affaiblir.

Quant à nous, notre démarche est différente
===============================================

Nous reconnaissons la réalité de l’antisémitisme, nous contestons aussi
bien l’analyse qu’en font les courants sionistes que celle de l’UJFP.

Nous combattons l’idée que le sionisme soit la réponse à l’antisémitisme.

D’un point de vue révolutionnaire, le sionisme est donc une idéologie à
combattre, mais ce n’est pas la cause de tous les maux de la société
(le supposer relève clairement d’un mode de pensée antisémite).

Ce n’est notamment pas lui qui crée le racisme à l’encontre des Juif-ve-s
et des Musulman-e-s, qui répondent à des logiques internes à la société
francaise et à son histoire.

L’oublier aggrave la situation de la minorité nationale juive en
légitimant un certain antisémitisme, tout en renforçant le sionisme par
le biais de l’amalgame entre antisémites et opposantEs à la politique
israélienne.

**Nous sommes et nous serons toujours du coté des oppriméEs partout dans le
monde, en France comme en Palestine**.

**Nous entendons marcher sur nos deux pieds : contre la réaction au sein de
notre minorité, dont le sionisme et l’islamophobie font partie, et contre
l’antisémitisme dont nous sommes victimes**.
