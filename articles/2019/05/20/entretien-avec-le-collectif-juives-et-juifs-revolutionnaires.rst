
.. _jjr_2019_05_20:

=============================================================================================
2019-05-20 **Entretien avec le collectif Juives et Juifs révolutionnaires** par ballast
=============================================================================================


- https://www.revue-ballast.fr/entretien-avec-le-collectif-juives-et-juifs-revolutionnaires/


revue-ballast.fr BALLAST • Entretien avec le collectif Juives et Juifs
révolutionnaires Ballast 35–45 minutes

En l’es­pace d’une décen­nie, Ilan Halimi était assas­si­né, des
enfants juifs étaient abat­tus dans une école tou­lou­saine et quatre
clients de l’Hyper Cacher tom­baient à Paris sous les balles de Daech. En
2018, on enre­gis­trait sur le ter­ri­toire fran­çais plus de 350 actes
anti­sé­mites : croix gam­mées, sym­boles fas­cistes et supré­ma­tistes
blancs, pro­fa­na­tions de cime­tières, etc. Le col­lec­tif Juives et Juifs
révo­lu­tion­naires a vu le jour trois ans plus tôt. Leurs objec­tifs ? Que
la lutte contre l’an­ti­sé­mi­tisme s’ins­crive au sein de la lutte
anti­ra­ciste, aux côtés de toutes les mino­ri­tés dis­cri­mi­nées
; que la soli­da­ri­té à l’en­droit de la cause pales­ti­nienne
s’é­ta­blisse en toute occa­sion sur des bases anti­co­lo­nia­listes
et jamais racistes ; que le com­bat contre l’an­ti­sé­mi­tisme s’ancre
au sein de la lutte des classes et désigne pour hori­zon la révo­lu­tion
sociale. Nous en dis­cu­tons ensemble.

Vous tenez, de l’in­té­rieur du mou­ve­ment social, à faire émer­ger
la lutte contre l’antisémitisme comme lutte anti­ra­ciste. La pre­mière
ne relève donc pas déjà de la seconde ?

Aujourd’hui — et depuis au moins une quin­zaine d’années —, nous pou­vons
consta­ter dans le champ poli­tique deux atti­tudes domi­nantes, en appa­rence
oppo­sées (mais qui se rejoignent sur le fond), en termes de trai­te­ment de
l’antisémitisme. L’une consiste à le voir comme quelque chose de rési­duel,
d’une moindre impor­tance, qui ne ferait pas par­tie — en tant que tel —
de l’agenda prio­ri­taire de la lutte anti­ra­ciste. L’autre consiste
au contraire à le trai­ter comme quelque chose de com­plè­te­ment à part,
sans lien avec le sys­tème raciste glo­bal. Pour nous, l’antisémitisme est
un des ava­tars du sys­tème raciste, une des formes que prend le racisme,
et il n’a rien de rési­duel. C’est une forme qui pos­sède bien sûr
des par­ti­cu­la­ri­tés spé­ci­fiques, qui expliquent notam­ment que
cer­tains et cer­taines peinent à l’identifier aujourd’hui : il se fonde sur
la racia­li­sa­tion d’une iden­ti­té reli­gieuse (comme l’islamophobie)
mais, à la dif­fé­rence des autres mino­ri­tés raci­sées, les Juifs et
Juives sont présenté·e·s comme déte­nant le pou­voir et l’argent, donc
comme pseu­do-classe domi­nante. Mais l’antisémitisme est lié au racisme
sys­té­mique : il en est un élé­ment, et pas un phé­no­mène dis­tinct. Il
nous semble donc néces­saire de réaf­fir­mer que la lutte anti­ra­ciste
ne peut élu­der la lutte contre l’antisémitisme, et que la lutte contre
l’antisémitisme implique de s’attaquer à l’ensemble du sys­tème raciste.

Vous enten­dez éga­le­ment tra­vailler à "la décons­truc­tion des
réflexes et des ten­dances anti­sé­mites pré­sentes dans les mou­ve­ments
révo­lu­tion­naires". Lorsque nous avions inter­ro­gé l’historien
Dominique Vidal à ce pro­pos, il sem­blait sur­pris. "La gauche a tiré les
leçons de la mon­tée du fas­cisme et du nazisme", nous a‑t-il dit, et aucun
mou­ve­ment ou par­ti de gauche ne "flirte" avec l’antisémitisme. «
Au contraire", ajou­tait-il, c’est à gauche, et plus encore à la gauche
radi­cale, que les Juifs ont trou­vé "leurs défen­seurs les plus héroïques
». Comment entendre ce déca­lage d’appréciation ?


Penser qu’une idéo­lo­gie qui imprègne l’ensemble de la socié­té s’arrête à la fron­tière de la gauche ou des mou­ve­ments révo­lu­tion­naires est absurde
=============================================================================================================================================================

L’antisémitisme est pro­fon­dé­ment ancré dans la réa­li­té
fran­çaise. Il pos­sède une dimen­sion struc­tu­relle lié au "roman
natio­nal" fran­çais. Penser qu’une idéo­lo­gie qui imprègne l’ensemble
de la socié­té s’arrête à la fron­tière de la gauche ou des mou­ve­ments
révo­lu­tion­naires est absurde. Si l’on étu­die sans com­plai­sance
l’histoire du mou­ve­ment ouvrier, de la gauche et de l’antisémitisme,
on s’aperçoit qu’il existe, à toutes les époques, une influence de
l’antisémitisme. Cela ne signi­fie pas que l’antisémitisme fasse par­tie du
cœur idéo­lo­gique de la gauche ou du mou­ve­ment révo­lu­tion­naire,
mais que les uns et les autres ne sont pas immu­ni­sés par nature contre
l’influence de cette idéo­lo­gie domi­nante. De nom­breux Juifs et Juives
ont plei­ne­ment trou­vé leur place au sein du mou­ve­ment ouvrier et de la
gauche radi­cale, et celle-ci a effec­ti­ve­ment su, à cer­tains moments,
jouer un rôle déci­sif dans le com­bat contre l’antisémitisme. Mais elle a
aus­si connu des erre­ments, par­fois, de la part de cou­rants loin d’être
mar­gi­naux. Les tra­vaux d’historiens comme Zeev Sternhell, Gérard Noiriel
ou Michel Dreyfus montrent cette réa­li­té. Aujourd’hui encore, à la faveur
d’un recul géné­ral d’une approche maté­ria­liste, lié notam­ment
à un gros défi­cit de for­ma­tion, on peut consta­ter un trai­te­ment
de l’antisémitisme par­ti­cu­liè­re­ment pauvre. Nous mili­tons dans
la gauche révo­lu­tion­naire, et avons pu consta­ter de pre­mière main ce
genre de problèmes.

À quoi songez-vous ?
========================

Par exemple, régu­liè­re­ment, des mili­tantes et des mili­tants juifs
sont som­més de se posi­tion­ner en préa­lable sur la situa­tion en
Palestine : cette injonc­tion géo­po­li­tique est anti­sé­mite. De la
même manière que som­mer les musul­mans ou musul­manes de se posi­tion­ner
en préa­lable sur Daech, les Frères musul­mans ou l’islam poli­tique est
pro­fon­dé­ment isla­mo­phobe. Aussi, un Juif ou une Juive qui sou­lève
des pro­pos anti­sé­mites sera régu­liè­re­ment accu­sé d’être un
mili­tant sio­niste infil­tré ; ou la reprise, par exemple, d’un des­sin
d’un anti­sé­mite notoire fera l’ob­jet d’une même accu­sa­tion. Soit
la ques­tion anti­sé­mite n’est pas prise au sérieux, soit elle est
soup­çon­née de cacher un agen­da de sou­tien à l’État israélien.

[Kazimir Malevitch］
======================

Vous refu­sez l’idée d’une cou­pure entre un anti­sé­mi­tisme «
tra­di­tion­nel" et un "nou­vel" anti­sé­mi­tisme. En clair, entre
l’antisémitisme de l’extrême droite et celui que nombre d’intellectuels
attri­buent aux Français de confes­sion musul­mane. Quel est ce conti­nuum ?

Tout d’abord, il convient de rap­pe­ler que l’antisémitisme «
tra­di­tion­nel" n’est pas l’apanage de l’extrême droite. Un
pré­sident (Macron) qui réha­bi­lite Pétain ou Maurras et un Premier ministre
(Raymond Barre) qui déplore que des "Français inno­cents" aient été
tués lors de l’attentat de la rue Copernic ne sont pas d’extrême droite ;
pour­tant, il s’agit bien d’antisémitisme, ou d’une réha­bi­li­ta­tion
des anti­sé­mites. Un par­ti poli­tique (LR) qui reprend une affiche sur
laquelle Macron est cari­ca­tu­ré avec un nez évo­quant les cari­ca­tures
anti­sé­mites n’appartient pas non plus à l’extrême droite. Lorsque des
jour­na­listes de la Ligue du Lol har­cèlent des jour­na­listes juifs,
il s’agit bien d’antisémitisme ; pour­tant ces jour­na­listes ne sont
pas d’extrême droite. L’antisémitisme est pro­fon­dé­ment ancré
dans la socié­té fran­çaise, qui est d’ailleurs, avec l’Espagne et
ses "Ley de Sangre", l’un des pays qui ont joué un rôle majeur dans la
for­ma­tion de l’antisémitisme. Nous dis­tin­guons donc l’antisémitisme,
oppres­sion raciste, de la judéo­pho­bie, oppres­sion reli­gieuse —
même si l’antisémitisme a pu reprendre des thèmes de la judéo­pho­bie
chré­tienne (les crimes rituels, la démo­no­lo­gie) et, beau­coup plus
mar­gi­na­le­ment, de la judéo­pho­bie musul­mane (l’ac­cu­sa­tion
de "traî­trise"). Ce sont des théo­ri­ciens fran­çais qui ont pro­mu
le mythe du "com­plot juif" et les thèmes anti­sé­mites modernes
comme l’association juif/argent/pouvoir/parasitisme : Gobineau, Vacher de
Lapouge, Drumont… Ils ont pui­sé dans une lit­té­ra­ture anti­juive
anté­rieure qui com­por­tait de tels thèmes, mais for­mu­lés de manière
pro­to-raciale. C’est la figure de Drumont qui incarne ce conti­nuum, car
c’est lui qui a expor­té ses théo­ries vio­lem­ment anti­sé­mites dans
l’Algérie colo­niale, et leur dif­fu­sion a été favo­ri­sée par le
par­ti colo­nial, qui y a vu un outil par­ti­cu­liè­re­ment effi­cace
de main­tien de l’ordre en cher­chant à dres­ser les musul­mans contre
les juifs, d’une part, et à soli­da­ri­ser les juifs — par peur — au
par­ti colo­nial, d’autre part. Cette stra­té­gie a été mise en avant
pour dévier la colère anti­co­lo­nia­liste dans un sens pogromiste.

Dieudonné et Soral jouent depuis les émeutes de 2005 le même rôle que jouait Drumont en Algérie : un moyen de dévier une révolte sociale vers une logique pogromiste
==============================================================================================================================================================================

Les mili­tants anti­co­lo­nia­listes algé­riens ont d’ailleurs per­çu
le rôle que jouait cette agi­ta­tion anti­sé­mite. Le fait que les auteurs
des assas­si­nats anti­sé­mites de ces der­nières années se reven­diquent
de l’islam (en en ayant par­fois une connais­sance plus que rudi­men­taire)
ne fait pas de leur anti­sé­mi­tisme un pro­duit d’importation. Les thèmes
qu’ils mobi­lisent dans leur dis­cours de haine ne sont pas une pro­duc­tion
musul­mane, mais bien une pro­duc­tion fran­çaise, qui a ensuite été
lar­ge­ment dif­fu­sée dans la sphère colo­niale. L’influence de ces
thèses a bien plus à voir avec le tra­vail poli­tique de Soral et de Dieudonné,
et leur dif­fu­sion d’une culture anti­sé­mite de masse, car les assas­sins
ont gran­di en France et se sont socia­li­sés au sein de cette réa­li­té
fran­çaise. Dieudonné et Soral jouent depuis les émeutes de 2005 le même
rôle que jouait Drumont en Algérie : un moyen de dévier une révolte sociale
vers une logique pogro­miste. Le fait que la grande majo­ri­té des émeutes
anti­juives en terre d’Islam coïn­cident avec la période colo­niale n’a
rien d’un hasard. Les théo­ri­ciens anti­sé­mites tak­fi­ris comme Sayyid
Qutb sont en réa­li­té pro­fon­dé­ment influen­cés par l’antisémitisme
euro­péen, et la relec­ture qu’ils font des textes reli­gieux s’ef­fec­tue
à tra­vers le prisme de cette influence idéo­lo­gique. Il n’y a donc pas
deux anti­sé­mi­tismes mais un anti­sé­mi­tisme, qui est une vision du
monde qui s’est dif­fu­sée à par­tir de la France et de l’Europe à
l’échelle mondiale.

Ces der­nières années, la lutte contre l’antisémitisme a sou­vent été
média­ti­sée par les enne­mis de l’é­man­ci­pa­tion. Face à quoi,
écri­vez-vous, la gauche a déser­té le ter­rain pour ne pas escor­ter
ces gens…

C’est parce que la gauche et la gauche radi­cale ont glo­ba­le­ment
déser­té le ter­rain de la lutte contre l’antisémitisme et le tra­vail
anti­ra­ciste spé­ci­fique en direc­tion de la mino­ri­té juive, que ce
hold-up idéo­lo­gique a pu se pro­duire. La droite, qui a his­to­ri­que­ment
joué le rôle de vec­teur de l’antisémitisme, a très bien per­çu
l’opportunité stra­té­gique. "La nature a hor­reur du vide", en
poli­tique comme ailleurs. Si la gauche radi­cale et la gauche avaient tenu le
ter­rain de la lutte contre l’antisémitisme, plu­tôt que de l’abandonner
en consi­dé­rant qu’il s’agissait au mieux de quelque chose de rési­duel,
au pire d’un pré­texte invo­qué par les réac­tion­naires, ces der­niers
n’auraient jamais pu impo­ser leur discours.

[Kazimir Malevitch］
==========================

"Négationnisme et anti­sé­mi­tisme sont consub­stan­tiels au FN", rap­pe­liez-vous en 2017.

Sur I24News, Marine Le Pen a pour­tant osé décla­rer, au mois de février 2019,
que son par­ti est le "meilleur bou­clier" des Français juifs !

Le FN a été créé par d’anciens SS et des anti­sé­mites mili­tants. Autour
de Marine Le Pen, dans son entou­rage immé­diat, on trouve un mili­tant
néo­na­zi comme Frédéric Chatillon. Et ce n’est que l’arbre qui cache
la forêt. Les réseaux FN sont tou­jours des vec­teurs d’antisémitisme,
et la théo­rie du grand rem­pla­ce­ment en est un exemple. Car dans cette
théo­rie com­plo­tiste, qui est à la manœuvre ? Le FN n’a pas besoin
de viser expli­ci­te­ment les Juifs : tout son dis­cours est struc­tu­ré
autour d’un ima­gi­naire anti­sé­mite — les "élites mon­dia­listes
» fai­sant par­tie des termes consa­crés pour dési­gner les Juives et
les Juifs. Marine Le Pen a sim­ple­ment com­pris qu’elle pou­vait plus
faci­le­ment dif­fu­ser ses thèmes racistes et isla­mo­phobes si elle le
fai­sait, comme une large part du champ poli­tique, der­rière l’étendard de
la "défense des Juifs". Il s’agit bien enten­du là d’une hypo­cri­sie
sans nom.

"Quand on attaque un Juif, on attaque la République", a récem­ment
décla­ré un porte-parole du gou­ver­ne­ment1. C’est deve­nu une
for­mu­la­tion récur­rente au sein du per­son­nel poli­tique. Vous
esti­mez pour­tant que "la République fait sem­blant de pro­té­ger les
Juifs et les Juives, tout en exi­geant d’elles et eux des preuves de loyau­té
per­ma­nente, et pointe du doigt, opprime et écrase d’autres mino­ri­tés" : quel est ce men­songe républicain ?


La République se pose en pro­tec­trice des Juifs et des Juives, mais il s’agit là, avant tout, d’une posi­tion opportuniste
====================================================================================================================================

Dans l’histoire fran­çaise, il y a tou­jours eu l’exigence d’une loyau­té
spé­ci­fique à l’égard de la mino­ri­té juive. Selon ce dis­cours,
il y a le "bon" Juif fran­çais et la figure de la cin­quième colonne,
les attaques contre les "judéo-bol­che­viks". Les Juifs et Juives ne
sont "inté­grés" qu’au prix de l’exigence d’une sur­en­chère de
loyau­té à l’égard du sys­tème poli­tique fran­çais. La République se
pose en pro­tec­trice des Juifs et des Juives, mais il s’agit là, avant tout,
d’une posi­tion oppor­tu­niste qui a un double objec­tif : d’une part,
effa­cer les traces de l’antisémitisme fran­çais en le rédui­sant dans la
conscience col­lec­tive à la seule période de l’Occupation (donc en élu­dant
le rôle his­to­rique de la France dans la for­mu­la­tion et l’exportation
de l’antisémitisme, et en en fai­sant, en quelque sorte, d’ores et déjà
un pro­duit prin­ci­pa­le­ment "étran­ger") et à une pré­ten­due
"impor­ta­tion exo­gène" liée au conflit israé­lo-pales­ti­nien ;
d’autre part, et en consé­quence, dif­fu­ser un dis­cours raciste en ciblant
la mino­ri­té musul­mane, tout en se pré­sen­tant comme "antiraciste"…

L’Union juive fran­çaise pour la paix est pro­ba­ble­ment l’organisation
juive la plus connue au sein de la gauche anti­ca­pi­ta­liste. Si vous
par­ta­gez avec elle la cri­tique du sio­nisme, vous esti­mez pour­tant
qu’elle prend le risque de ser­vir de "cau­tion juive" : ce n’est pas
rien !

Comme nous l’avons indi­qué dans plu­sieurs de nos textes, l’UJFP
a été créée par des mili­tantes et des mili­tants juifs qui, ayant
par­ti­ci­pé aux luttes anti­co­lo­niales, ont vou­lu déve­lop­per
une cri­tique du sio­nisme et du colo­nia­lisme israé­lien sur le thème
du "pas en notre nom". Cette démarche est louable, et elle n’est pas
l’objet de notre cri­tique. Par contre, au fil des années, l’UJFP a
déve­lop­pé un posi­tion­ne­ment vis-à-vis de l’antisémitisme qui
nous pose pro­blème à plu­sieurs égards. Tout d’abord, l’UJFP ana­lyse
pour l’essentiel la ques­tion de l’antisémitisme à tra­vers le prisme
du sio­nisme et de la situa­tion en Palestine : en ce sens, elle rejoint
les cou­rants sio­nistes en fai­sant de l’antisémitisme un ava­tar
de la ques­tion israé­lo-pales­ti­nienne, alors qu’il s’agit d’une
idéo­lo­gie fran­co-fran­çaise. La ques­tion israé­lo-pales­ti­nienne
ne fait éven­tuel­le­ment que se sur­im­po­ser à la ques­tion, en
en faus­sant la com­pré­hen­sion. L’UJFP fait, pour l’essentiel,
de l’antisémitisme actuel la consé­quence du sio­nisme et d’un
pré­ten­du "phi­lo­sé­mi­tisme d’État", enten­du comme
trai­te­ment pri­vi­lé­gié des Juifs et des Juives, qui ali­men­te­rait un
res­sen­ti­ment qui serait la source de l’antisémitisme. Or l’antisémitisme
pré­cède le sio­nisme. Le sio­nisme est une réponse — natio­na­liste, donc
pour nous réac­tion­naire — à l’antisémitisme. Faire de la consé­quence
la cause, et rendre res­pon­sable de la mon­tée du racisme une par­tie
— réac­tion­naire — de la mino­ri­té ciblée, est un pro­cé­dé
d’inversion très cou­rant en matière de racisme : c’est du même ton­neau
que les dis­cours qui rendent les Frères musul­mans res­pon­sables de la
mon­tée de l’islamophobie.

De même, l’UJFP sert très régu­liè­re­ment d’alibi à une par­tie de
la gauche radi­cale lorsqu’il est ques­tion de l’antisémitisme : on les
bran­dit comme les "bons Juifs", qui per­mettent de ne sur­tout pas se poser
trop de ques­tions, de ne pas se confron­ter aux pro­blèmes d’antisémitisme
qui existent y com­pris au sein de la gauche radi­cale. L’attitude poli­tique
de l’UJFP depuis quelques années sur la ques­tion de l’antisémitisme la
pré­dis­pose à jouer ce rôle avec un dis­cours du type : "On n’est pas
anti­sé­mites, la preuve, on fait des trucs avec l’UJFP." C’est aus­si
en ce sens qu’elle est très appré­ciée, parce qu’elle n’oblige pas la
gauche radi­cale à se remettre en cause. Bien sûr, nous ne pen­sons pas que
c’est l’intention de l’UJFP. Mais nous ne pou­vons que consta­ter la
réa­li­té de ce genre de discours.

[Kazimir Malevitch］
==========================

Vous évo­quez le "phi­lo­sé­mi­tisme d’État", l’i­dée de «
pri­vi­lège juif". Quand l’é­di­teur Éric Hazan écrit se sou­ve­nir
avec nos­tal­gie du "temps où les juifs n’é­taient pas du côté du
manche", ou quand le Parti des Indigènes de la Répulique (PIR) évoque un
"deux poids, deux mesures" en matière de lutte contre l’islamophobie et
l’antisémitisme, ce serait donc cette thèse qu’ils illustreraient ?

Cette thèse du "phi­lo­sé­mi­tisme d’État" est pour nous erro­née,
et par ailleurs dan­ge­reuse : même si ce n’est pas l’intention de l’UJFP,
elle réac­tive des sté­réo­types anti­sé­mites par­ti­cu­liè­re­ment
ancrés — comme ce pré­ten­du "pri­vi­lège juif" ou la pré­ten­due
"proxi­mi­té des Juifs et du pou­voir". Ce "phi­lo­sé­mi­tisme
d’État" ne repose pas sur une réa­li­té maté­rielle, et réac­tive
éga­le­ment le vieux thème anti­sé­mite de la période colo­niale pour
divi­ser juifs et musul­mans afin de pro­té­ger l’ordre colo­nial. La
phrase d’Éric Hazan est curieuse. Parce que main­te­nant les Juifs et les
Juives seraient "du côté du manche" ? Il faut dis­tin­guer le dis­cours
de l’État et de la classe poli­tique et la réa­li­té maté­rielle. Il y a
un déca­lage entre les paroles, la réa­li­té et les actes. Premièrement,
les avan­cées en matière de lutte contre l’antisémitisme sont le
fruit de dizaines d’années de luttes menées par la mino­ri­té juive :
elles ne sont pas le fait d’une prise de conscience sou­daine et morale
de la classe poli­tique. Deuxièmement, l’historiographie fran­çaise et
l’enseignement sco­laire conti­nuent lar­ge­ment d’éluder la ques­tion
de l’antisémitisme fran­çais pour se concen­trer sur la seule période
1939–1945. Et encore, ceci se fait sur la base de "Vichy, ce n’est pas
la France", avec toute une mytho­lo­gie recons­truite autour de "La
France résis­tante" — alors que seule une mino­ri­té (dont un nombre
consé­quent de Juifs et de Juives, d’Arménien·ne·s, d’Espagnol·e·s) a
résis­té. Ensuite, les anti­sé­mites blancs béné­fi­cient d’une large
impu­ni­té et d’une com­plai­sance qui contre­dit ce dis­cours : on pense
à Égalité et Réconciliation, à Rivarol, mais aus­si à des situa­tions de
har­cè­le­ment anti­sé­mite qui ont per­du­ré pen­dant de nom­breuses
années dans l’indifférence — comme l’illustre récem­ment, on l’a dit,
la Ligue du Lol.

Parler de "pri­vi­lège" pour par­ler d’une mino­ri­té ciblée par des actes racistes régu­liers est à peu près aus­si pro­duc­tif en matière d’antiracisme que de par­ler des "pri­vi­lèges des fonc­tion­naires" en matière de lutte des classes
======================================================================================================================================================================================================================================================

Bien sûr qu’il existe un dis­cours public qui en fait des tonnes sur
l’antisémitisme, et pas sur les autres formes de racisme ! Discours qui
en pro­fite d’ailleurs pour dif­fu­ser un poi­son raciste. Mais il ne
s’agit pas de "phi­lo­sé­mi­tisme d’État", pas plus que les grandes
décla­ra­tions sur l’égalité hommes-femmes du gou­ver­ne­ment ne doivent
mas­quer la réa­li­té du patriar­cat. Dénoncer un "phi­lo­sé­mi­tisme
d’État" est aus­si absurde que de dénon­cer un "fémi­nisme d’État
». Parler de "pri­vi­lège" pour par­ler d’une mino­ri­té ciblée par
des actes racistes régu­liers est à peu près aus­si pro­duc­tif en matière
d’antiracisme que de par­ler des "pri­vi­lèges des fonc­tion­naires"
en matière de lutte des classes. En réa­li­té, plu­tôt que d’introduire
ce genre de concepts, qui auront sur­tout pour seul effet d’obtenir un
nivel­le­ment par le bas et un recul des luttes anti­ra­cistes, il serait
plus pro­duc­tif de s’appuyer sur les quelques suc­cès rela­tifs — et
à rela­ti­vi­ser — en matière de lutte contre l’antisémitisme, pour
deman­der à les étendre et les géné­ra­li­ser à la lutte contre toutes
les formes de racisme, dont l’islamophobie.

Le PIR dit pro­po­ser, par la voix d’Houria Bouteldja, une "offre
géné­reuse" à l’endroit des Juifs de France : "Nous avons un des­tin
com­mun comme nous avons poten­tiel­le­ment un ave­nir poli­tique com­mun."
Pourtant, vous esti­mez que le PIR défend des "posi­tions réac­tion­naires
» et mini­mise l’antisémitisme.

On n’a sans doute pas la même vision de la géné­ro­si­té. On ne demande
d’ailleurs à per­sonne la géné­ro­si­té, juste l’égalité. Le
PIR s’est illus­tré régu­liè­re­ment par des prises de posi­tion
réac­tion­naires. Non seule­ment il mini­mise l’antisémitisme, mais il
s’en fait par­fois le vec­teur. À la lec­ture de ses com­mu­ni­qués
à chaque fait de vio­lence anti­sé­mite (on parle de meurtres, notam­ment
d’enfants), il y a régu­liè­re­ment une mise en doute du carac­tère
anti­sé­mite des actes, et on sent plus de sym­pa­thie pour les auteurs que
pour les vic­times. Ça signi­fie quoi de dire "Je suis Mohamed Merah", de
"dénon­cer toute cen­sure contre Dieudonné" ou de dire que "les Juifs
sont la batte de base­ball avec laquelle on frappe les Noirs et les Arabes",
en matière de "géné­ro­si­té" ? C’est une curieuse concep­tion de «
l’amour révo­lu­tion­naire". Le PIR s’en défend, y voit un mau­vais
pro­cès, mais quand ce genre de trucs revient de manière récur­rente, et
qu’il n’y a pas le début d’une auto­cri­tique, que le dis­cours se
borne à dire "Mais vous nous avez mal com­pris, vous êtes mal­veillants,
en fait on ne vous veut que du bien", on ne peut juste que consta­ter qu’il
y a un pro­fond problème.

[Kazimir Malevitch］
=====================

"Je suis, cela va sans dire, un adver­saire du sio­nisme", affirmait Trotsky
en 1934 dans le cadre d’une inter­view parue dans Class Struggle. 85 ans plus
tard, cette évi­dence n’en est plus une, et peut-être même expo­se­ra-t-elle
bien­tôt, à entendre Macron, à des repré­sailles judi­ciaires ! Qu’est-ce
qu’être anti­sio­niste, aujourd’hui, quand le pro­jet sio­niste a
triom­phé, que la com­mu­nau­té inter­na­tio­nale l’a rati­fié à la
majo­ri­té et qu’une armée des plus solides le "sécu­rise" durablement ?

Rappelons d’a­bord que le sio­nisme est un mou­ve­ment natio­na­liste
né dans des condi­tions par­ti­cu­lières : il s’ins­crit dans
le mou­ve­ment des natio­na­li­tés du XIXe siècle, mais avec
une spé­ci­fi­ci­té : il naît, à la dif­fé­rence des autres
idéo­lo­gies natio­na­listes, au sein d’une mino­ri­té natio­nale
dia­spo­rique qui n’est majo­ri­taire nulle part et qui est oppri­mée,
jus­te­ment, par les idéo­lo­gies natio­na­listes euro­péennes, qui
dési­gnent les Juives et les Juifs comme exté­rieurs au corps natio­nal
en déve­lop­pant l’an­ti­sé­mi­tisme. C’est donc ini­tia­le­ment
une idéo­lo­gie natio­na­liste qui se pré­sente comme une solu­tion à
l’an­ti­sé­mi­tisme. Mais du fait de cette situa­tion mino­ri­taire
des Juifs et des Juives, la consti­tu­tion d’une majo­ri­té natio­nale
juive sur un ter­ri­toire don­né, et donc d’un État-nation, n’a
pu se faire qu’a­vec une entre­prise colo­niale — elle-même d’un
genre par­ti­cu­lier puis­qu’elle s’est faite sans qu’existe
ini­tia­le­ment une métro­pole. Entreprise qui a eu pour consé­quence
l’ex­pul­sion mas­sive des Palestinien·ne·s. Ainsi que la mise en place
d’une domi­na­tion colo­niale sur ceux et celles-là en Palestine. Au sein de
la mino­ri­té juive, cette idéo­lo­gie a long­temps été mino­ri­taire,
même si elle a gagné pro­gres­si­ve­ment en influence avec la mon­tée
de l’an­ti­sé­mi­tisme. Mais elle a été com­bat­tue par d’autres
idéo­lo­gies, dont les idéo­lo­gies révo­lu­tion­naires qui la
cri­ti­quaient à la fois pour sa ten­dance à por­ter l’ef­fort sur
le pro­jet colo­nial au détri­ment de la lutte en dia­spo­ra contre
l’an­ti­sé­mi­tisme, pour sa volon­té de nier la culture juive
dia­spo­rique et pour la consé­quence du pro­jet sio­niste sur les
Palestinien·ne·s. Aujourd’hui, les cou­rants qui se reven­diquent du
sio­nisme en dia­spo­ra conti­nuent de mettre en avant l’ins­tal­la­tion
en Israël comme la solu­tion face à l’antisémitisme.


Aucune paix durable et juste ne peut décou­ler d’une telle vision, qui nie l’effet concret de ce pro­jet sur les Palestinien·ne·s : expro­pria­tion, expul­sion, vio­lence armée per­ma­nente, oppres­sion raciste
========================================================================================================================================================================================================================

Notre oppo­si­tion au sio­nisme découle d’une double ana­lyse.

Premièrement : d’une posi­tion anti­na­tio­na­liste qui, sans nier les
spé­ci­fi­ci­tés du sio­nisme par rap­port aux autres natio­na­lismes, en
iden­ti­fie éga­le­ment les traits com­muns. Cette posi­tion est notam­ment
fon­dée sur l’a­na­lyse de l’effet que cette idéo­lo­gie natio­na­liste
(dans ses cou­rants divers, de l’extrême droite à l’extrême gauche) a sur
notre mino­ri­té : iso­le­ment, mécom­pré­hen­sion des dyna­miques
de l’antisémitisme, renon­cia­tion à la lutte, ici rem­pla­cée
par le sou­tien à l’État-nation israé­lien. Deuxièmement : de nos
posi­tions anti­co­lo­nia­listes, en cohé­rence avec nos posi­tions
révo­lu­tion­naires. Aujourd’hui, l’idéologie sio­niste conti­nue
à jus­ti­fier le sta­tu quo colo­nial et l’ex­pro­pria­tion des
Palestinien·ne·s au nom de la néces­si­té vitale de pré­ser­ver un
État-nation refuge pour les Juifs et les Juives. Pour la gauche sio­niste,
c’est un mal néces­saire (qu’il fau­drait éven­tuel­le­ment limi­ter
dans l’es­pace afin de pré­ser­ver le fait natio­nal israé­lien) ;
pour la droite, c’est une pra­tique légi­ti­mée par l’Histoire. Aucune
paix durable et juste ne peut décou­ler d’une telle vision, qui nie l’effet
concret de ce pro­jet sur les Palestinien·ne·s : expro­pria­tion, expul­sion,
vio­lence armée per­ma­nente, oppres­sion raciste…

Ceci étant dit, nous vivons dans une métro­pole colo­niale et impé­ria­liste
res­pon­sable du sac­cage de l’Afrique et cores­pon­sable du géno­cide
au Rwanda. De façon per­ma­nente, elle mène des guerres néo­co­lo­niales
et sou­tient des dic­ta­tures san­gui­naires. C’est à par­tir de
cette posi­tion que nous dénon­çons le colo­nia­lisme israé­lien. Nous
sommes donc contre le sio­nisme, mais ne nous défi­nis­sons pas comme «
anti­sio­nistes" parce que notre oppo­si­tion au sio­nisme découle
d’une vision anti­co­lo­nia­liste, hos­tile à l’ethnonationalisme «
völ­kish". Cette vision n’ap­plique pas au sio­nisme un "deux poids, deux
mesures", qui en ferait une forme de colo­nia­lisme ou de natio­na­lisme
meilleure — point de vue sio­niste — ou pire qu’un autre — point de
vue de bon nombre d’"anti­sio­nistes". Nous insis­tons éga­le­ment
sur le fait que l’attachement d’une par­tie de la mino­ri­té juive en
dia­spo­ra au sio­nisme découle de la crainte d’un nou­veau géno­cide
et de l’idée du carac­tère indis­pen­sable d’un "État refuge"
pour les Juifs et les Juives. Il faut com­prendre cette crainte (après 2000
ans de per­sé­cu­tions et un géno­cide qui a fait dis­pa­raître deux
tiers des Juifs et Juives d’Europe), et pro­po­ser une autre voix de lutte
contre l’antisémitisme que celle qui consiste à sou­te­nir la fuite en
avant colo­niale en Palestine. C’est ce à quoi nous nous atta­chons, et nous
pen­sons que cette dimen­sion est glo­ba­le­ment sous-esti­mée. Pour ce qui
concerne la situa­tion en Palestine, il n’existe pas entre nous de posi­tions
uni­fiées sur la solu­tion à appor­ter : deux États, un État bina­tio­nal,
laïque et démo­cra­tique, une confé­dé­ra­tion… L’essentiel étant
de rompre avec ledit sta­tu quo colonial.

[Kazimir Malevitch］
===========================

Dans une post­face à Sur la Question juive de Marx, Daniel Bensaïd
déplore le fait que le judéo­cide ait été déshis­to­ri­ci­sé et
dépo­li­ti­sé : il serait deve­nu "un évè­ne­ment théo­lo­gique» fait d’indicible et d’impensable, laissant ain­si place à l’identité,
comme "outrage" enve­lop­pant, au lieu de pen­ser la poli­tique, et donc
la révo­lu­tion. Partagez-vous cette analyse ?

Nous consi­dé­rons bien évi­dem­ment que le géno­cide des Juifs et
Juives et des Rroms doit être pen­sé dans une pers­pec­tive his­to­rique
et poli­tique — ce qui n’empêche nul­le­ment d’en sou­li­gner les
spé­ci­fi­ci­tés. Nous ne déve­lop­pons pas une vision iden­ti­taire,
ni de l’Histoire, ni de la poli­tique, mais une vision maté­ria­liste. Il
est clair que le géno­cide des Juifs et des Rroms a long­temps été un sujet
du domaine de l’in­di­cible, puis­qu’il aura fal­lu attendre 1995 pour
que l’État fran­çais recon­naisse sa res­pon­sa­bi­li­té… Mais
il y a effec­ti­ve­ment un accent mis sur l’é­mo­tion­nel plu­tôt
que sur le maté­riel. En évo­quant le géno­cide, les poli­ti­ciens
pren­dront des tour­nures de phrase ampou­lées et des emphases sur «
les heures les plus sombres de notre Histoire", mais aucun accent n’est
mis sur l’en­sei­gne­ment de cet évè­ne­ment, les élé­ments qui
l’ont pré­cé­dé et les consé­quences qui l’ont sui­vi. Beaucoup
reste à faire pour évi­ter la mys­ti­fi­ca­tion de l’Histoire :
com­ment s’ad­mi­nistre un géno­cide ? quelle res­pon­sa­bi­li­té
doit être por­tée, par qui ? Tel que c’est ensei­gné actuel­le­ment, la
res­pon­sa­bi­li­té des nazis est la seule évo­quée, mais pas celles des
témoins silen­cieux, des col­la­bo­ra­teurs, ni celle de l’i­déo­lo­gie
natio­nale qui a per­mis à l’ap­pa­reil nazi d’ad­mi­nis­trer ce
géno­cide. Il faut un contexte d’an­ti­sé­mi­tisme à même de per­mettre
de dis­so­cier un peuple spé­ci­fique du reste de la popu­la­tion et de
rendre indif­fé­rent, ou jus­ti­fiable, son assas­si­nat mas­sif. Il y a
évi­dem­ment eu la volon­té de confis­quer ce trau­ma­tisme aux vic­times
juives et rroms. Lorsqu’on parle du géno­cide, on ne s’é­tend pas sur
le fait que les col­la­bo­ra­teurs et témoins pas­sifs n’é­taient
pas ravis du retour des dépor­tés, que les biens spo­liés n’aient
majo­ri­tai­re­ment pas été ren­dus. Ce sont des élé­ments fac­tuels,
pour­tant. Ils per­mettent d’an­crer des évè­ne­ments his­to­riques
dans la réa­li­té. Ce n’est pas pour rien que le seul docu­men­taire
qui a eu pour but de com­prendre com­ment ce géno­cide et pas pour­quoi
ce géno­cide, dure 10 heures… En essayant de com­prendre com­ment, on
apprend à iden­ti­fier les pré­mices d’une situa­tion géno­ci­daire et
l’im­pli­ca­tion de l’en­semble de la popu­la­tion dans celle-ci : c’est
une ques­tion qu’un pays comme la France, encore inca­pable de recon­naître
la vio­lence inouïe de la colo­ni­sa­tion, n’a pas envie de se poser.

La défense de la liber­té d’expression s’est sou­vent cris­tal­li­sée,
ces der­nières années, autour de la ques­tion juive et/ou israé­lienne. Avec
l’idée qu’il y a des choses qu’on ne peut pas dire, plus dire — Soral et
Dieudonné, vous les citiez, en ont très sou­vent joué. Faut-il s’en remettre
au pou­voir d’État pour faire face aux fas­cistes, ou peut-on ima­gi­ner
d’autres voies ?

"La voix des opprimé·e·s est en per­ma­nence silen­ciée dans    le sys­tème raciste ; faire taire les racistes, c’est per­mettre l’expression des opprimé·e·s."
=================================================================================================================================================================

"La défense de la liber­té d’expression" (de tout le monde, donc des
fas­cistes) comme posi­tion abs­traite est un angle d’attaque poli­tique
carac­té­ris­tique du libé­ra­lisme : on pense la ques­tion en dehors
des rap­ports sociaux et des rap­ports de forces réels. Le fait de se
posi­tion­ner comme oppri­més est une constante pour les anti­sé­mites :
c’est une inver­sion du réel. Cette stra­té­gie de vic­ti­mi­sa­tion est
un moyen pour eux de ren­for­cer le sté­réo­type du "pou­voir juif" ;
c’est une stra­té­gie plei­ne­ment poli­tique. En réa­li­té, ce qui
est défen­du par celles et ceux qui la bran­dissent dans ces cir­cons­tances,
c’est la "liber­té d’oppression". La voix des opprimé·e·s est en
per­ma­nence silen­ciée dans le sys­tème raciste ; faire taire les racistes,
c’est per­mettre l’expression des opprimé·e·s qui, sinon, est étouf­fée
par la vio­lence du dis­cours raciste qui se répand en per­ma­nence. La lutte
contre le racisme se fait, selon nous, "par tous les moyens néces­saires",
pour reprendre une expres­sion des Black Panthers. Nous ne nous en remet­tons
pas au pou­voir d’État, mais nous ne voyons pas pour­quoi il fau­drait
par exemple se pri­ver d’utiliser les moyens légaux exis­tants — ce qui
n’empêche ni la lutte idéo­lo­gique, ni la lutte phy­sique pour étouf­fer
le dis­cours raciste et assu­rer l’autodéfense antiraciste.

On a récem­ment pu lire dans Le Monde, sous la plume de la rab­bin Delphine
Horvilleur, que "la contes­ta­tion du pou­voir", si elle s’énonce sur
le registre de la "dénon­cia­tion d’"élites", de [la] culpa­bi­li­té
des "riches" ou [du] "com­plot" des puis­sants", ren­voie au "lan­gage
ances­tral qui fut dans l’histoire celui de l’antisémitisme". On a
éga­le­ment vu Le Point faire part de son trouble à la lec­ture du der­nier
ouvrage de François Ruffin, face à l’évocation des mots "Rothschild" et «
Attali". Comment rendre lim­pide l’idée que la lutte contre les déten­teurs
du capi­tal n’a rien — ou ne devrait rien — à voir avec l’antisémitisme ?

La foca­li­sa­tion sur les ban­quiers juifs ou sur un "capi­ta­lisme
financier/cosmopolite" (oppo­sé arti­fi­ciel­le­ment au ver­tueux «
capi­ta­lisme industriel/national") asso­cié aux Juifs et aux Juives est
effec­ti­ve­ment une constante his­to­rique du dis­cours anti­sé­mite «
social". Il s’agit là d’un "anti­ca­pi­ta­lisme roman­tique" qui
ne s’attaque ni à la pro­prié­té pri­vée des moyens de pro­duc­tion
et de dis­tri­bu­tion, ni aux rap­ports sociaux de pro­duc­tion, et
qui refuse d’envisager la mise en com­mun des moyens de pro­duc­tion
et de dis­tri­bu­tion. La seule manière de rendre lim­pide l’idée
anti­ca­pi­ta­liste est donc de la for­mu­ler de manière maté­ria­liste,
en s’attaquant à la racine des rap­ports sociaux de pro­duc­tion :
pro­prié­té com­mune des moyens de pro­duc­tion et de dis­tri­bu­tion,
mar­chan­di­sa­tion, socié­té de classe. Et d’adopter un voca­bu­laire
sans ambi­guï­té : par­ler de "bour­geoi­sie" plu­tôt que de "finance
», iden­ti­fier les classes en pré­sence plu­tôt que les indi­vi­dus,
refu­ser toute foca­li­sa­tion sur les bour­geois juifs en particulier.

[Kazimir Malevitch］
===========================

L’essayiste Michel Warschawski nous disait un jour, à Jérusalem : "Je ne
crois pas à l’existence de valeurs juives." Entendre que, pour lui, "tout
est juif". Vous louez quant à vous une "éthique juive et uni­ver­sa­liste" :
quels en sont les contours singuliers ?

Nous avons, en tant que révo­lu­tion­naires, des valeurs éthiques qui
sont uni­ver­selles, en ce sens qu’elles sont com­munes à l’humanité
et existent dans toutes les socié­tés, à toutes les époques : entraide,
soli­da­ri­té, refus de la domi­na­tion… L’éthique juive dont nous
nous reven­di­quons est sim­ple­ment l’une des formes cultu­relles qu’a
prise cette éthique uni­ver­selle, liée à la condi­tion de mino­ri­té
oppri­mée. Ce n’est pas tout à fait un hasard si de très nom­breux Juifs et
Juives ont par­ti­ci­pé au mou­ve­ment ouvrier révo­lu­tion­naire, dans
toutes ses ten­dances, en jouant par­fois un rôle moteur dans la créa­tion
d’organisations de soli­da­ri­té qui étaient loin de ne se consa­crer
qu’à la mino­ri­té juive et aux pro­blèmes aux­quels elle était
confron­tée. C’est aus­si parce qu’ils y trou­vaient l’expression de
valeurs pré­sentes dans la culture dia­spo­rique. Nous nous situons dans la
conti­nui­té de ce par­cours, et il y a nombre de figures his­to­riques qui
nous ins­pirent : d’Emma Goldman en pas­sant par Rosa Luxemburg, de Marek
Edelman en pas­sant par Denis Théodore Goldberg, de Lucien Sportisse à Ilan
Halevi, de Joseph Rosenthal à Adolfo Kaminsky. Parmi bien d’autres.

Le Bund et les FTP-MOI sont des réfé­rences régu­liè­re­ment mobi­li­sées
au sein du camp de l’émancipation. Mais cet inté­rêt, que vous par­ta­gez,
occulte à vos yeux la lutte contre l’antisémitisme contem­po­rain. Évoquant
la mémoire des com­bat­tants du ghet­to de Varsovie, vous en appe­lez à ne
plus attendre et à agir, ici et main­te­nant, contre l’antisémitisme. Vous
avez d’ailleurs par­lé d’"autodéfense"…

Nous avons sou­le­vé une atti­tude qui nous parais­sait pro­blé­ma­tique
: celle de ne par­ler des Juifs et Juives qu’au pas­sé, d’invoquer ces
expé­riences moins pour en tirer des ensei­gne­ments aujourd’hui que pour
sur­tout évi­ter de par­ler de la situa­tion des Juifs et des Juives au
pré­sent. Or "le pas­sé ne s’est envo­lé nulle part". Parler de
ces réfé­rences doit être un moyen de pour­suivre le com­bat, et pas de
l’éluder ou le remi­ser dans le rayon des nos­tal­gies com­modes. Ces
réfé­rences ne sont pas exclu­sives : il existe d’autres réfé­rences
juives dans le mou­ve­ment ouvrier inter­na­tio­na­liste. L’autodéfense
est une longue tra­di­tion dans la dia­spo­ra en Europe, qui a sus­ci­té
face aux pogroms la for­ma­tion de groupes d’autodéfense appe­lés «
Zelbshuts". L’idée est de renon­cer à subir, de ne pas comp­ter sur
l’État pour se défendre, ici et main­te­nant. C’est une pra­tique née
de la néces­si­té. Mais cette notion ne doit pas être com­prise seule­ment
dans cette pers­pec­tive d’autodéfense phy­sique (visant à pré­ser­ver
nos inté­gri­tés face aux anti­sé­mites), mais sur le plan de la lutte
idéo­lo­gique éga­le­ment : prendre en charge notre propre lutte et ne pas
comp­ter sur des "pro­tec­teurs", d’où qu’ils viennent.

Illustrations et ban­nière : extraits d’œuvres de Kazimir Malevitch

