.. index::
   pair: Golem ; 2019-11-06
   pair: UJFP ; 2019-11-06
   pair: Philosémitisme d’Etat; 2019-11-06

.. _jjr_antiracisme_2019_11_06:

=========================================================================
2019-11-06 **Quel antiracisme politique ?** Critiques de l'UJFP/PIR
=========================================================================

- https://juivesetjuifsrevolutionnaires.wordpress.com/2019/11/06/quel-antiracisme-politique/
- :ref:`antisem:houria_bouteldja`


.. figure:: images/golem.jpg

Introduction
=============

La récurrence des attaques racistes, de tout bord, et contre l’ensemble des
minorités, rappellent l’importance d’une réponse antiraciste forte et
unifiée, sur la base d’une analyse fiable du contexte social et historique
français.

Mais la volonté d’unification autour d’une lutte ne doit pas
pousser à pousser la poussière sous le tapis, minimiser l’oppression subie
et nous taire sur les questions qui fâchent.

.. _ujfp_pir_2019_11_06:

2019-11-06 **Sur l'appel du PIR et de l'UJFP**
=================================================

Ainsi il nous a été reproché par certains de "mettre les pieds dans le plat",
de faire oeuvre de division, en critiquant l’appel à rassemblement de
Ménilmontant, à l’appel notamment de l’UJFP et du PIR.

Un rassemblement où l’on a pu entendre des propos antisémites tels que
"les juifs sont privilégiés", voir une quenelle, et la tentative de
silenciation de prises de paroles juives (1) dénonçant le traitement
qui était fait de l’antisémitisme sans grande réaction des
organisateurs, et où il n’a quasiment pas été question d’antisémitisme
en tant que tel, mais uniquement de son instrumentalisation.

Une telle situation ne peut nous conduire à mettre les problèmes sous
le tapis. Mais au contraire, elle doit nous pousser à nous poser la
question suivante : quel antiracisme politique voulons-nous ?"

Antiracisme moral ou le pir(e) de l’antiracisme politique : un faux dilemne
======================================================================================

Selon certains, **critiquer les positions de l’UJFP et du PIR en matière
d’antisémitisme**, ce serait faire le choix de "l’antiracisme moral" et
renoncer à une analyse politique du racisme (et de l’antisémitisme comme
forme de racisme).

Il n’y aurait donc aucune autre alternative.

Cette manière de penser est sans doute très confortable, puisqu’elle pose le combat en
termes chimériques et permet d’éviter de se remettre en cause, **comme si
l’auto-affirmation antiraciste immunisait contre l’antisémitisme**.

Mais elle est justement d’une grande pauvreté politique. Et elle est démentie
pas un simple regard sur la réalité..

L’antiracisme politique ne se réduit ni au PIR, ni à l’UJFP.

Analyser le caractère systémique du racisme, promouvoir l’auto-organisation
des personnes raciséEs, identifier les mécanismes coloniaux et néo-coloniaux
dans la production du racisme, tout cela n’oblige fort heureusement ni
à utiliser des concepts du type "Philosémitisme d’Etat", ni à se
retrouver régulièrement à mettre en doute le caractères antisémite de
crimes qui en ont pourtant les caractère, ni à minimiser l’antisémitisme
en France.

Plutôt que le pir(e) de l’antiracisme politique, il est donc possible de
choisir le meilleur !

2019-11-06 **Sur le concept de "philosémitisme d’Etat" : l’enfer antisémite peut même être pavé de bonnes intentions**
==============================================================================================================================

Les défenseurs "antiracistes" de son usage nous expliquent que ce concept
n’a rien d’antisémite.

Qu’il s’agirait simplement de souligner une forme particulière d’antisémitisme
au niveau de l’État, qui se traduirait essentiellement par l’instrumentalisation
de la lutte contre l’antisémitisme contre d’autres minorités raciséEs.

Qu’il ne s’agirait nullement de recycler ici, dans une forme progressiste,
l’idée antisémite d’une collusion entre les juifs et le pouvoir.

Le premier problème dans cette approche est que le concept de "philosémitisme d’Etat"
ne renvoit pas seulement à l’idée antisémite d’une collusion entre les juifs
et le pouvoir, mais à un autre thème antisémite récurrent, celui du privilège
juif.

Ainsi l’étude de l’histoire de l’antisémitisme nous montre que l’un, mais
évidemment pas le seul, des moteurs des révoltes pogromistes en Pologne,
ou plus largement dans la zone de résidence (:ref:`2 <2_2019_11_06>`)
c’est l’idée que les juifs et juives seraient privilégiés par le pouvoir.

L’un des moteurs des premières émeutes anti-juives au Maghreb à l’époque
coloniale, c’est également cette idée du privilège juif, savamment diffusée
auprès de la population musulmane tant par les administrateurs coloniaux
que par la ligue antisémitique de Drumont.

L’un des moteurs de l’agitation antisémite en France à la même époque,
c’est également cette idée de la "république enjuivée" et du "juif roi de l’époque",
parce que privilégié.


Faire abstraction de cette histoire .. relève **soit d’une singulière naiveté et méconnaissance, soit d’un choix politique assumé**
========================================================================================================================================

Faire abstraction de cette histoire, penser qu’elle relève uniquement du
passé et penser qu’elle n’a aucune incidence sur l’idéologie dominante
relève **soit d’une singulière naiveté et méconnaissance, soit d’un
choix politique assumé**.

Le second problème de cette approche est qu’elle ignore ensuite une réalité :
le sens que prend un discours ne dépend pas de l’intention de celui qui
l’émet, mais **de l’idéologie dominante qui influence la compréhension qui
en est faite, quelles que soient les intentions de l’auteur**.

A supposer donc que l’intention ne soit pas de faire preuve de complaisance
avec la thèse du "privilège juif", il est sérieusement permis de s’interroger
sur le résultat d’une telle stratégie, d’un point de vue antiraciste,
à l’heure où les violences antisémites explosent.

Car ce concept est également largement utilisé précisément comme synonyme
de "privilège juif" de la part de l’État.
En populariser l’usage et la diffusion revient donc en réalité à se faire,
même inconsciemment, le vecteur de cette théorie.


Rappeller qu’une telle attitude .. s’inscrit en fait dans la continuité d’une longue tradition, notamment coloniale
=======================================================================================================================

Il existe pourtant une manière plus simple, plus directe et bien moins
ambiguë de démasquer l’hypocrisie de l’État quand il est question
d’antisémitisme, et l’instrumentalisation raciste qui est faite de la lutte
contre l’antisémitisme par les politiciens : celle qui consiste à rappeller
qu’une telle attitude, loin de relever d’une prétendue forme nouvelle ou
d’un "changement de nature" de l’antisémitisme, s’inscrit en fait
dans la continuité d’une longue tradition, notamment coloniale.

Celle qui consiste à rappeller qu’il a fallu des dizaines d’années de lutte
pour que la reconnaissance du génocide juif et des responsabilités de l’État
français.

Que malgré cette reconnaissance, la question de la responsabilité de
la France dans la formation de l’idéologie antisémite et sa diffusion reste
largement éludée.

Que l’histoire de l’antisémitisme en France avant la période 39-45 reste
encore largement ignorée dans les programmes et manuels scolaires, comme
si celui ci était apparu avec le nazisme.

Et que donc loin de relever d’un "traitement privilégié", cette reconnaissance
est le fruit de luttes qui continuent encore aujourd’hui.

Celle qui consiste à souligner que lorsque les auteurs des discours, des
actes et des violences antisémites sont blancs, **ils bénéficient largement
d’une impunité et d’une tolérance, particulièrement lorsqu’ils occupent
des positions de pouvoir**.

**L’offensive récurrente pour réhabiliter des intellectuels ou figures
politiques antisémites en est d’ailleurs la démonstration, de Maurras à
Céline en passant par Pétain**.

**L’antisémitisme est un outil de maintien de l’ordre colonial, hier comme aujourd’hui**
===========================================================================================

Enfin, rappeler que l’antisémitisme est un outil de maintien de l’ordre
colonial, hier comme aujourd’hui : en opposant les minorité raciséEs
entre elles autour de la dénonciation d’un prétendu "privilège juif",
cet antisémitisme a un double effet :

- enjoindre à la minorité juive de se solidariser à l’ordre colonial
  qui se présente comme protecteur des Juives et Juifs d’une part,
  alors qu’il joue le rôle de pompier pyromane.
- protéger l’ordre colonial, d’autre part, en dirigeant la colère des
  autres minorités racisées contre les Juifs et les Juives en lieu et
  place de la bourgeoisie coloniale blanche et de culture chrétienne.


.. _ujfp_antisem_2019_11_06:

2019-11-06 **Lutter contre l’antisémitisme en minimisant l’antisémitisme, vraiment ?** (UJFP/PIR)
=======================================================================================================

Reste enfin un constat : la constance, associée à l’utilisation de ce
concept, de la minimisation des actes antisémites, voire de la négation de
leur caractère.

**Car qu’a t’on pu constater de la part de l’UJFP comme du PIR ces 15 dernières années ?**

Une mise en doute du caractère antisémite de l’assassinat d’Ilan Halimi
-----------------------------------------------------------------------------------------------------------

Une mise en doute du caractère antisémite de l’assassinat d’Ilan Halimi (:ref:`3 <3_2019_11_06>`)

La parole des victimes ou de leurs familles n’a visiblement pas de valeur
lorsqu’il s’agit d’antisémitisme, où tout d’un coup il est fait une curieuses
confiance dans les sources policières, et **une curieuse retenue en matière
de qualification politique**.

Mais qu’il s’agisse, des policiers lyonnais qui affichent une carte de
la milice dans leur bureau, ou des CRS perpignanais qui affichent une photo
d’Hitler dans une caserne de Perpignan, on a pu pourtant constater qu’il
existe encore des antisémites dans la police, en quantité non négligeable.

Plus occupés à défendre "l’indépendance de la justice face aux
pressions communautaires" (sic) que l’intérêt des victimes et de leur
famille, **les deux organisations ont, régulièrement publié des communiqués
qui témoignent de plus de compassion pour les antisémites que pour leur
victimes**. (:ref:`4 <4_2019_11_06>`)

Lorsqu’est relevé cette complaisance, la ligne de défense est
régulièrement la même : il s’agirait de passages "isolés de leur contexte",
et le lecteur de ces communiqués qui les comprendrait comme une forme de
complaisance avec l’antisémitisme ambiant serait de mauvaise foi ou ferait
une lecture malvaillante.

Le problème d’une telle explication, c’est :

- 1/ qu’en matière de communication politique, elle part d’un curieux postulat
  **ce ne serait pas à celui qui communique de faire l’effort de rendre sa
  pensée claire et lisible, sans ambiguité, mais à celui ou celle qui reçoit
  le message**.
  Ainsi, **plutôt que de clarifier le discours**, puis de renoncer à toute
  formulation ambiguë, **ces organisations ont choisi en toute connaissance
  de cause de continuer à utiliser les mêmes formulations ambiguës**.

- 2/ que le côté récurrent de tels passages définit une attitude politique.
  Ainsi, PIR comme UJFP ne se privent pas, et à raison, lorsqu’il s’agit
  de critiquer leurs adversaires politiques, de relever des éléments de
  discours pour les qualifier politiquement.

  Même enrobés de protestations antiracistes, des éléments de discours
  racistes restent racistes.
  Houria Bouteldja, porte parole du PIR, ne se prive d’ailleurs pas de
  préciser que pour elle "l’idée ici n’est pas de combattre l’islamophobie
  et l’antisémitisme. **L’idée est de combattre l’islamophobie et le philosémitisme**." (:ref:`5 <5_2019_11_06>`)

Les deux avatars, en apparence opposés, d’un pretendu "nouvel antisemitisme"
================================================================================

Nous avons régulièrement combattu la thèse d’un "nouvel antisémitisme"
puisque l’antisémitisme actuel n’a rien de nouveau, trouvant ses
sources dans la France coloniale notamment.

Les tenants de ce concept entendent ainsi imposer l’idée d’un antisémitisme
qui serait importé, exogène pour mieux passer un discours raciste.

Ainsi entend t-on régulièrement parler "d’importation du conflit israelo-palestinien"
comme clé d’explication de la montée de l’antisémitisme en France.

L’UJFP et le PIR partagent avec les tenants de cette thèse l’idée d’un
changement de nature de l’antisémitisme.

Ils y voient la conséquence du sionisme, et voient dans l’antisémitisme
une "réaction au sionisme", et à un prétendu "philosémisme d’Etat"..

Cette thèse nie tout simplement la dynamique propre de l’antisémitisme
et son ancrage dans le roman national français et la culture française.

Elle est de même nature que de prétendre que l’islamophobie serait une
réaction à l’islam politique/Les frères musulmans/Daesh…, **thèse qu’on
entend régulièrement et qui doit être combattue avec la même fermeté** .

Or l’existence de courants réactionnaires dans nos minorités, si elle
est à combattre parce qu’elle s’oppose à toute perspective émancipatrice,
n’est jamais une explication pertinente du racisme, car une telle explication
revient à exonérer le système raciste de sa responsabilité.

Les seuls responsables du racisme sont les racistes.

.. _ujfp_sit_israel_2019_11_06:

Qu’il s’agisse des sionistes, d’une part, ou de l’UJFP et du PIR, d’autre part, toutes et tous voient la question de l’antisémitisme par le prisme de la situation en Israël/Palestine
==============================================================================================================================================================================================

Ainsi, qu’il s’agisse des sionistes, d’une part, ou de l’UJFP et du PIR,
d’autre part, toutes et tous voient la question de l’antisémitisme par
le prisme de la situation en Israël/Palestine.

**Or nous sommes en France, et l’antisémitisme actuel est le produit de
l’histoire française**.

Identifier et combattre ses mécanismes suppose justement de revenir à
cette réalité.

**Pour les masses juives, 74 ans après le génocide, la lutte contre
l’antisémitisme n’est pas secondaire, c’est une nécessité vitale**.

Pour les antiracistes en France, pays qui possède une responsabilité particulière
dans la formation et la diffusion des théories antisémites, mais aussi dans
le génocide, c’est aussi une nécessité.

Pour les révolutionnaires enfin, combattre l’antisémitisme est une nécessité
**pour éviter qu’une dynamique pogromiste se substitue à une dynamique
révolutionnaire authentique fondée sur une réelle conscience de classe.**


.. _ujfp_pal_2023_11_06:

2019-11-06 Impasse pour la minorité juive opprimée par l’antisemitisme, mais aussi impasse du point de vue de la solidarité anticolonialiste avec les palestiniennes et palestiniens (UJFP)
==============================================================================================================================================================================================

L’UJFP met en avant pour justifier ses positions la nécessité de faire preuve
d’une solidarité sans faille avec les palestiniens et palestiniennes qui
subissent au quotidien la violence d’un système colonial.

Cette solidarité suppose de combattre l’idéologie sioniste, qui justifie
cette oppression autour de l’idée d’un Etat refuge pour les juifs et les
juives face aux persécutions antisémites.

Or ce que l’UJFP refuse de prendre en compte c’est que la raison pour
laquelle cette idée a acquis et a conservé une telle influence dans les
masses juives en diaspora, même les plus éloignées à priori du sionisme,
**c’est justement la persistance et la violence récurrente des persécutions
antisémites**.


.. _ujfp_minor_2023_11_06:

2019-11-06  **L’UJFP, loin de produire un affaiblissement de ces courants qu’elle entend combattre, participe en fait à leur renforcement au sein de la minorité juive**
==========================================================================================================================================================================

Faute de comprendre cette base matérielle sur laquelle repose l’influence
idéologique des courants sionistes, **l’UJFP, loin de produire un affaiblissement
de ces courants qu’elle entend combattre, participe en fait à leur renforcement
au sein de la minorité juive**.

Ainsi, plutôt que de nier ou minimiser ces persécutions et donc le vécu
d’une majorité des juifs et juives au quotidien, la meilleure attitude
à avoir pour combattre l’influence de cette idée est précisément de
démontrer qu’il existe une autre voie face aux violences antisémites, qui
ne soit ni le déni, ni le soutien à un Etat et à sa pratique coloniale :
cette voie c’est celle de la lutte antiraciste, c’est à dire la lutte en
diaspora pour la liquidation de l’antisémitisme, à travers plusieurs moyens.

L’un, immédiat
------------------

L’un, immédiat, qui consiste à **organiser l’autodéfense idéologique
et physique au sein de la minorité**, mais aussi en cherchant des convergences
avec d’autres minorités victimes de racisme, sur des bases de compréhension
mutuelle de la violence raciste subie.

En cherchant enfin les convergence avec le mouvement ouvrier organisé,
porteur d’une dimension anticapitaliste qui met à nu les mécanismes de
classe et les mécanismes coloniaux de l’antisémitisme.

L’autre, sur la durée
-------------------------

L’autre, sur la durée, qui consiste à s’attaquer aux racines idéologiques
et matérielles de l’antisémitisme : l’idéologie dominante, à travers
notamment le roman national français et européen, **le capitalisme qui lors de
ses périodes de crises secrète et réactive les thèmes antisémites comme
moyen de défense de la bourgeoisie**.

Le pouvoir (néo)colonial, qui utilise et sécrète l’antisémitisme comme
moyen de défense pour dévier la révolte anticoloniale dans un sens
pogromiste et ainsi préserver le pouvoir blanc et le colonialisme français.


.. _place_extreme_droite_2023_11_06:

2019-11-06 **L’attitude, à l’inverse qui consiste à nier et minimiser le vécu juif de l’antisémitisme revient à laisser le terrain du discours et de l’action à la droite, voire à l’extrême droite française**
==================================================================================================================================================================================================================

**L’attitude, à l’inverse qui consiste à nier et minimiser le vécu juif de
l’antisémitisme revient à laisser le terrain du discours et de l’action
à la droite, voire à l’extrême droite française**, et plus largement
aux chauvins qui peuvent ainsi déployer leur discours raciste et éluder
leur responsabilité historique dans l’antisémitisme.

Cela revient aussi, au sein de notre minorité, **à laisser le terrain aux
différents courants sionistes, qui ont ainsi une grande facilité à pointer
le décalage entre le vécu d’une majorité des Juifs et des Juives en diaspora
et le discours d’une organisation comme l’UJFP lorsqu’il est question
d’antisémitisme**.

Pour sortir de cette impasse, il faut s’attaquer précisément à la base
matérielle de cette influence.

Loin d’être secondaire dans la solidarité anticolonialiste avec les palestiniens
et palestiniennes, le combat contre l’antisémitisme est incontournable : parce
qu’il montre qu’il existe une autre voix pour les Juives et les Juifs
en diaspora face à l’oppression antisémite que le soutien au statut quo
colonial en Palestine : la lutte, ici et maintenant, contre l’antisémitisme.

Entendre la voix des juives et des juifs en diaspora pour faire vivre le diasporisme
=======================================================================================

L’un des points communs entre les différents courants sionistes et les
assimilationistes républicains, est la négation du diasporisme.

C’est à dire d’une vie juive en diaspora qui ne soit pas assimilée et dont la
spécificité culturelle ne soit pas niée, mais aussi d’une vie juive qui
ne soit pas conçue comme une anomalie historique, qui doit être résolue
par l’alya (:ref:`6 <6_2019_11_06>`) et la renonciation à la galout (:ref:`7 <7_2019_11_06>`).


**Partout où nous résidons, là est notre patrie**
---------------------------------------------------

Réaffirmer que la vie juive en diapora n’est ni un exil ni la survivance
d’un passé qui doit être absorbé par le nationalisme français ou le
nationalisme juif qu’est le sionisme, c’est réaffirmer cette position
diasporiste tenue historiquement par le mouvement ouvrier juif non
sioniste : **"Partout où nous résidons, là est notre patrie"**.

Cela suppose donc de combattre l’idéologie nationaliste en ce qu’elle
prétend nier toute spécificité culturelle pour construire un roman national
fondée en France sur une culture nationale "blanche et chrétienne", ou
en Israël une "culture nationale juive".

Cette vision nationaliste volkiste (:ref:`8 <8_2019_11_06>`) est l’un des fondements de
l’oppression des minorités nationales par la majorité nationale, partout
où elle a été mise en œuvre.

Elle s’est forgée, à la même époque que se développait l’impérialisme
européen, d’abord en Espagne par les "leys de Sangre" introduisant une
vision raciale de la nationalité pour construire un Etat centralisé et
conquérant au moment de la reconquista puis de l’empire colonial espagnol.

Puis en France lors de la construction de la monarchie absolue et du
premier empire colonial français, et plus tard au Maghreb comme outil
de maintien de l’ordre colonial.

Entendre la voix des juives et des juifs qui subissent l’antisémitisme
est donc une nécessité à la fois du point de vue antiraciste, et du point
de vue anticolonialiste, qu’il s’agisse de s’opposer au colonialisme
français ou israélien.

.. _ujfp_pir_neg_2019_11_06:

2019-11-06 **Briser les idées recues plutôt que de les entretenir** (UJFP et PIR)
====================================================================================

L’UJFP et le PIR justifient leur discours en affirmant qu’il s’agit
pour eux précisément de combattre l’influence du discours soralien et
dieudonniste au sein des minorités racisées.

Pour se faire il conviendrait de "combattre la sacralisation de la shoah
pour combattre le négationisme»,(sic)
et "combattre le philosémitisme d’État pour combattre l’antisémitisme"(sic).

Si on prend au sérieux cette affirmation selon laquelle ce discours est
en réalité une subtile "stratégie de combat contre l’antisémitisme", **le
constat des résultats obtenus ne peut que laisser songeur**.


Loin d’entamer la progression du racisme, une telle attitude a contribué à légitimer des thèmes antisémites
===============================================================================================================

Loin d’entamer la progression du racisme, une telle attitude a contribué
à légitimer des thèmes antisémites.

**Plus globalement, elle affaiblit le combat antiraciste**.

Car plutôt que de s’appuyer sur le succès relatif des luttes pour la mémoire
du génocide pour les généraliser à la mémoire de l’ensemble des crimes
racistes, notamment les crimes coloniaux, une telle approche ouvre tout
simplement la perspective du nivellement par le bas : car si l’on focalise
sur l’importance supposée -en réalité à relativiser- qu’aurait acquis le
génocide juif dans l’historiographie, plutôt que sur la nécessité d’étendre
la reconnaissance aux autres crimes racistes, on s’expose à une réponse
du pouvoir raciste qui consiste en réalité à éluder l’ensemble des crimes
racistes, y compris le génocide, au nom du rejet de "l’idéologie de la repentance".

La réhabilitation de Maurras et Pétain par le champs politique en est
l’un des exemples les plus récents

Aussi sûrement que de dénoncer un prétendu traitement plus
favorable de certains secteurs du prolétariat (public, grande industrie)
n’ouvre certainement pas la voie à une progression générale de la
condition du prolétariat dans la lutte des classes, mais plutôt à un recul
généréralisé qui en réalité renforce la bourgeoisie, une telle attitude
ne peut en réalité que contribuer au renforcement du pouvoir raciste.

Briser ce cercle c’est en réalité montrer que les quelques conquêtes des
unEs doivent être étendues et bénéficier à toutes et tous, et non considérer
que celles-ci relèveraient d’un privilège qu’il faudrait combattre.

L’arrivée au pouvoir de Trump aux Etats Unis a ainsi montré la nécessité
pour les minorités racisées, mais aussi le mouvement ouvrier, de faire front
commun autour d’une stratégie antiraciste qui construise les convergences
plutôt que les oppositions.

La question se pose de manière particulièrement urgente en France.

De même, il faut savoir s’extraire du discours officiel qui prétend apporter
une réponse "ferme" face à l’antisémitisme.

Dans la pratique, il existe de nombreuses situations où les juifs et les juives
peuvent observer le décalage entre le discours et la pratique.

**Ainsi, le torrent de discours racistes et antisémites sur les réseaux sociaux reste
largement toléré**.

Des organes antisémites comme Rivarol ou le site «démocratie participative"
continuent en toute impunité de diffuser leur venin.

Globalement, lorsqu’il est le fait de blancs, l’antisémitisme est minimisé
et toléré.
Il n’est qu’à voir la complaisance dont les journalistes impliqués dans
la "Ligue du lol" ont pu bénéficier pour leur acharnement antisémite,
pendant des années.
Ou le traitement de harcèlement antisémite en milieu de travail par la
justice, qui a tendance, comme pour les discriminations racistes, à
classer sans suite ou à rejeter le caractère raciste de la preuve.

L’absence de sanction réelle pour les policiers et militaires nostalgiques
de Vichy ou du nazisme en est une autre illustration.


.. _ujfp_pourvoir_2019_11_06:

2019-11-06 En réalité, l’UJFP valide la construction médiatique du pouvoir
==================================================================================

En réalité, **l’UJFP valide la construction médiatique du pouvoir lorsqu’elle
prend au mot la posture de prétendue "protection des Juifs et des Juives"
que prend le gouvernement**.

De même que les grands discours sur l’égalité homme-femme des institutions
n’ont pas, et de loin, fait disparaître le patriarcat, **les grands discours
en matière de "lutte contre l’antisémitisme" n’ont pas fait disparaître
un antisémitisme profondément ancré dans la réalité française**.

Il existe fort heureusement des organisations qui, dans le champs de
l’antiracisme politique, ne tombent pas dans ces travers, et qui combattent
l’antisémitisme pour ce qu’il est, un des avatars du système raciste dans
sa globalité.
Nous les saluons !
Pour celles qui à l’inverse, continuent dans ces impasses, elle ne pourront
que constater, si elles ne changent pas radicalement de politique en
assumant une réelle autocritique plutôt qu’une fuite en avant, les effets
délétères d’une telle approche pour l’ensemble des minorités racisées,
pour la lutte anticoloniale et pour la lutte anticapitaliste.

Notes
==========

.. _1_2019_11_06:

(1) Précisions concernant le rassemblement contre l’antisémitisme et son instrumentalisation du 19 février 2019 à Ménilmontant
------------------------------------------------------------------------------------------------------------------------------------

(1) Pour un compte rendu de l’une de ces interventions :
https://paris-luttes.info/precisions-concernant-le-11745 (Précisions concernant
le rassemblement contre l’antisémitisme et son instrumentalisation
du 19 février 2019 à Ménilmontant)

Précisons que si nous partageons les critiques qu’ils et elles ont émises,
les camarades qui sont intervenus ne font pas partie de JJR, contrairement
à ce que certains et certaines ont pu avancer.

Si bien sur beaucoup de personnes se sont rendues à ce rassemblement
avec l’intention sincère de protester contre l’antisémitisme, que de
telles réactions soient possibles dans un rassemblement dont l’objet
est censée être la lutte contre l’antisémitisme témoignent d’un
sérieux problème.


.. _2_2019_11_06:

(2) Zone de Résidence
----------------------------

(2) La Zone de Résidence était la région ouest de l’Empire russe
frontalière avec les puissances d’Europe centrale, où les Juifs enregistrés
comme tels étaient cantonnés par le pouvoir impérial jusqu’en février 1917.


.. _3_2019_11_06:

(3) Ilan Halimi
---------------------

(3)"Les informations sur les circonstances de ce crime, rendues
publiques à ce jour, très partielles et de sources souvent
contestables, ne permettent donc pas d’affirmer de manière
tranchée l’existence d’une dimension effectivement raciste."
MIR (http://indigenes-republique.fr/meurtre-dilan-halimi/),

"Pour autant, le caractère antisémite de cet acte n’est pas avéré"/"
Elle déplore que certains accréditent d’office la thèse du crime
antisémite.",
UJFP, 25 février 2006
(https://oumma.com/ilan-halimi-contre-tous-les-racismes-contre-toute-recuperation-de-la-douleur-et-de-lemotion/)


.. _4_2019_11_06:

(4)  Plus de compassion pour les antisémites que pour leur victimes
-----------------------------------------------------------------------

(4) "On pourrait discuter de savoir si l’analyse, diffusée sur le site
de la Tribu ka, mérite ou non, d’un point de vue strictement pénal, une
sanction pour antisémitisme.

Mais, quand bien même ce serait le cas, la condamnation de Kemi Seba est
scandaleuse parce qu’elle est n’est pas un acte de justice mais un acte
politique qui consacre le traitement inégal avec lequel sont jugés les
propos et comportements racistes."
(PIR, 3 décembre 2007),
Nous condamnons toutes les censures passées et futures à l’encontre
de Dieudonné. (PIR, 12 janvier 2014)

.. _5_2019_11_06:

(5) Islamophobia Conference 2018: Houria Bouteldja
---------------------------------------------------------

(5) https://www.youtube.com/watch?v=zYOsX4bgsls (Islamophobia Conference 2018: Houria Bouteldja)
Houria Bouteldja speaks at the Islamic Human Rights Commission's annual
Islamophobia Conference on 8 December 2018.
This year's theme was: 'Islamophobia and Silencing Criticism of Israel'


.. _6_2019_11_06:

(6) Alya
-----------

(6) l’alya, terme hébreux qui signifie "ascension" est le terme utilisé
par les courants sionistes pour désigner l’installation en Israël d’un
Juif ou d’une Juive de la diaspora


.. _7_2019_11_06:

(7) galout
-------------

(7) la galout, terme hébreu signifiant "exil", est le terme utilisé par
les courants sionistes, mais aussi par un certain nombre de courants religieux
pour désigner la situation des Juifs et des Juives en diaspora.


.. _8_2019_11_06:

(8) Le nationalisme "volkiste"
-----------------------------------


(8) Le nationalisme "volkiste" est un type de nationalisme qui fonde
la communauté nationale sur le liens du sang, de la filiation biologique,
et qui donc exclue de la "communauté nationale" les personnes qui sont
considérées comme "exogènes" de par leur filiation
