.. _sphx_tag_immigration:

My tags: Immigration
####################

.. toctree::
    :maxdepth: 1
    :caption: With this tag

    ../articles/2024/01/05/sur-la-loi-immigration.rst
