

.. raw:: html

   <a rel="me" href="https://kolektiva.social/@raar"></a>
   <a rel="me" href="https://piaille.fr/@raar"></a>


.. un·e
.. https://framapiaf.org/web/tags/raar.rss
.. https://framapiaf.org/web/tags/golem.rss
.. https://framapiaf.org/web/tags/racisme.rss
.. https://framapiaf.org/web/tags/racism.rss
.. https://framapiaf.org/web/tags/jjr.rss
.. https://framapiaf.org/web/tags/tsedek.rss
.. https://framapiaf.org/web/tags/ujfp.rss

.. ⚖️
.. 🔥
.. ☠️
.. ☣️
.. 🪧
.. 🇺🇳 unicef
.. 🌍 ♀️
.. 📣
.. 💃
.. 🎻
.. ✍🏼 ✍🏻✍🏿
.. ♀️✊ ⚖️ 📣
.. ✊🏻✊🏼✊🏽✊🏾✊🏿
.. 🤥
.. 🤪
.. ⚖️ 👨‍🎓
.. 🔥
.. 💧
.. ☠️
.. ☣️
.. 🪧
.. 🌍 ♀️✊🏽
.. 🎥 🎦
.. 🎇 🎉

|FluxWeb| `RSS <http://jjr.frama.io/juivesetjuifsrevolutionnaires/rss.xml>`_

.. _jjr:

=========================================================================
**Juives et Juifs Révolutionnaires** |jjr|
=========================================================================

- https://juivesetjuifsrevolutionnaires.wordpress.com/
- https://kolektiva.social/@jjr (Mastodon)
- https://blogs.mediapart.fr/juives-et-juifs-revolutionnaires
- https://www.instagram.com/Juifvesrevolutionnaires/


Juives et juifs révolutionnaires (JJR) est un collectif réunissant des
personnes juives de gauche radicale luttant contre l'antisémitisme,
notamment dans les espaces au sein desquels elles militent.

.. toctree::
   :maxdepth: 6

   articles/articles
